<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="DB/spDB.asp" -->
<!--#include virtual="inc/cart.asp" -->
<!--#include virtual="inc/date.asp" -->
<%
MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%>
<html>
<head><%
MetaTitle = "Sequins Plus Online - About Us Page"
MetaDesc = "Sequins Plus is a well-established dancewear company that has been supplying dancers and studios with dancewear, shoes and costumes for 17 years. Everything from ballet, tap and jazz shoes to bodywear and tiaras."
MetaKey = "sequins, plus, sequinsplus, about, supplies, wilson road south, oshawa, ontario, sales@sequinsplus.com, ballet, help, business, mothers, young, dancers, appropriate, shoes, leotards, dance, environment, dancewear, accessories, major dancewear manufacturers, Bloch, Capezio, Leos, Mondor, Body Wrappers, Gilda Marx, Bunheads, Sansha, company, Eileen Apsega, Edward Apsega, Studios, workshops, seminars, exhibitions, developments, children, danceworld, Parents, fitting, pointe, largest dancewear selection, scrunchies, bun covers, tiaras, bodysuits, tights"
%>
<TITLE><%=MetaTitle%></TITLE>
<!--#include virtual="inc/meta.asp" -->
<!--#include virtual="inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY bgcolor=#DFDFDF leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="MM_preloadImages('/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif','/images/about_but_over.jpg','/images/products_but_over.jpg','/images/services_but_over.jpg','/images/contact_but_over.jpg')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="3"></td>
    <td width="281" height="28" valign="top"><IMG src="/images/logo_top.gif" width=281 height=28 alt=""></td>
    <td width="336" height="28" colspan="2"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="2" valign="top"><IMG src="/images/logo_lft_top_line.gif" width=155 height=9 alt=""></td>
    <td width="281" height="9" valign="top"><IMG src="/images/logo_topline.gif" width=281 height=9 alt=""></td>
    <td width="326" height="9" valign="top"><IMG src="/images/logo_rt_top_line.gif" width=326 height=9 alt=""></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
    <td width="155" height="26" colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" valign="top"><IMG src="/images/logo_title.gif" width=281 height=26 alt="Sequins Plus"></td>
    <td width="326" height="26" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td width="155" height="11" colspan="2" valign="top"><IMG src="/images/logo_lft_bot_line.gif" width=155 height=11 alt=""></td>
    <td width="281" height="11" valign="top"><IMG src="/images/logo_bot-line.gif" width=281 height=11 alt=""></td>
    <td width="326" height="11" valign="top"><IMG src="/images/logo_rt_bot_line.gif" width=326 height=11 alt=""></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/nav_top.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="281" height="14" valign="top"><IMG src="/images/logo_beige.gif" width=281 height=14 alt=""></td>
    <td width="326" height="14" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>

  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" valign="top"><IMG src="/images/logo_spacer.gif" width=281 height=2 alt=""></td>
    <td width="326" height="2" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="174"></td>
    <td width="153" height="174" bgcolor="#CC1C0F"><a href="/en/about/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('aboutus','','/images/about_but_over.jpg',1)"><img src="/images/about_but.jpg" alt="About Us" name="aboutus" width="153" height="28" vspace="5" border="0"></a><a href="/en/products/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('products','','/images/products_but_over.jpg',1)"><img src="/images/products_but.jpg" alt="Products" name="products" width="153" height="28" vspace="5" border="0"></a><a href="/en/services/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('services','','/images/services_but_over.jpg',1)"><img src="/images/services_but.jpg" alt="Services" name="services" width="153" height="28" vspace="5" border="0"></a><a href="/en/contact/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','/images/contact_but_over.jpg',1)"><img src="/images/contact_but.jpg" alt="Contact Us" name="contact" width="153" height="29" vspace="5" border="0"></a></td>
    <td width="2" height="174" bgcolor="#FFFFFF"></td>
    <td width="607" height="174" colspan="2" valign="top"><IMG src="/images/sequinsmain.jpg" width=607 height=174 alt=""></td>
    <td width="10" height="174"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="762" height="2" colspan="4" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/list_bottom.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="607" height="14" colspan="2" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="4" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="681"></td>
    <td width="762" height="681" colspan="4" valign="top">
  <TABLE WIDTH=762 BORDER=0 CELLPADDING=0 CELLSPACING=0 bgcolor="#FFFFFF" mm_noconvert="TRUE">
        <TR>
		  <TD COLSPAN=2> <IMG SRC="/images/nav_top.gif" WIDTH=153 HEIGHT=14 ALT=""></TD>
		  <TD width="259" height="13" bgcolor="#CC1C0F"></TD>
		  <TD COLSPAN=2> <IMG SRC="/images/bar_gradient.gif" WIDTH=350 HEIGHT=14 ALT=""></TD>
	</TR>
	<TR>
		  <TD height="10" COLSPAN=5 bgcolor="#FFFFFF"></TD>
	</TR>
	<TR>
		  <TD COLSPAN=4> <IMG SRC="/images/about_us_title.gif" WIDTH=504 HEIGHT=24 ALT=""></TD>
		  <TD width="258" height="24" bgcolor="#FFFFFF"></TD>
	</TR>
	<TR>
		  <TD height="12" COLSPAN=5 bgcolor="#FFFFFF"></TD>
	</TR>
	<TR>
		  <TD width="22" height="607" bgcolor="#FFFFFF"><IMG SRC="/images/transparent.gif" WIDTH=22 HEIGHT=607></TD>
		  <TD height="607" COLSPAN=4 valign="top" background="/images/ballet-drawing_bg.gif" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Sequins 
            plus began as a home based small business 15 years ago. It began when 
            two mothers with young dancers found it difficult to find the appropriate 
            shoes and leotards in their local area. As the young dancers progressed 
            in the dance environment, their needs grew and grew. So began Sequins 
            Plus.<br>
			<br>
            From that small beginning, the Sequins Plus of today now carries thousands 
            of items of dancewear and accessories. All the major dancewear manufacturers 
            are represented; Bloch, Capezio, Leos, Mondor,<br>
			Body Wrappers, Gilda Marx, Bunheads, Sansha, and many more.<br>
			<br>
			The company is now owned by one of the original partners, Eileen Apsega and her<br> 
            husband Edward. Their knowledge and expertise in the dance world was 
            gained in part by<br>
			the fact they raised two beautiful dancers themselves, 
            but mostly by the fact that they<br>
            work closely with the various Studios, listening to what their needs 
            are and fulfilling<br>
			these needs. They attend workshops, seminars and exhibitions put together<br>
            by the various manufacturers and in this way ensure that they are 
            aware<br>
			of the new and exciting developments that take place.<br>
			<br>
			Most of the staff at Sequins Plus have 
            been involved with dancers<br>
			in some way, albeit by the fact that their 
            children danced or were<br>
			connected to the danceworld in some other 
            way. Parents of<br>
			young dancers (very important people) know that they 
            can<br>
            seek advice from Sequins Plus on all aspect of their children's<br>
			dance needs...<br>
            Sequins Plus staff specializes in the fitting of pointe shoes�. that 
            most important<br>
            first fitting is carried out professionally and with great care to 
            ensure the appropriate<br>
			pointe shoe is chosen.<br>
			<br>
            Today, Sequins Plus carries possibily the largest dancewear selection 
            in Ontario<br>
            and are constantly adding to our list as new items become available.<br>
			<br> 
            So from the top of their heads (scrunchies, bun covers, tiaras) to 
            the tip<br>
            of their toes (bodysuits, tights, shoes) Sequins Plus carries every<br>
            item a young dancer could possibly need.<br>
			<br>
			With this website, all 
            this wonderful selection and knowledge<br>
			is available to all, so please 
            email us, or call for any more<br>
			information you may require.</font></TD>
	</TR>
	<TR>
		  <TD COLSPAN=2> <IMG SRC="/images/list_bottom.gif" WIDTH=153 HEIGHT=14 ALT=""></TD>
		  <TD width="259" height="14" bgcolor="#CC1C0F"></TD>
		  <TD height="14" COLSPAN=2><IMG SRC="/images/bar_gradient.gif" WIDTH=350 HEIGHT=14 ALT=""></TD>
	</TR>
	<TR>
		  <TD bgcolor="#FFFFFF"> <IMG SRC="/images/spacer.gif" WIDTH=22 HEIGHT=1 ALT=""></TD>
		  <TD bgcolor="#FFFFFF"> <IMG SRC="/images/spacer.gif" WIDTH=131 HEIGHT=1 ALT=""></TD>
		  <TD bgcolor="#FFFFFF"> <IMG SRC="/images/spacer.gif" WIDTH=259 HEIGHT=1 ALT=""></TD>
		  <TD bgcolor="#FFFFFF"> <IMG SRC="/images/spacer.gif" WIDTH=92 HEIGHT=1 ALT=""></TD>
		  <TD bgcolor="#FFFFFF"> <IMG SRC="/images/spacer.gif" WIDTH=258 HEIGHT=1 ALT=""></TD>
	</TR>
</TABLE>
</td>
    <td width="10" height="681"></td>
  </tr>
  <tr>
    <td width="8" height="1"></td>
    <td width="762" height="1" colspan="4" bgcolor="#FFFFFF"></td>
    <td width="10" height="1"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="4" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td width="762" height="49" colspan="4" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="4" valign="top"><IMG src="/images/page-bottom.gif" width=762 height=7 alt=""></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="6" valign="top"></td>
  </tr>
</table></body>
</html>
