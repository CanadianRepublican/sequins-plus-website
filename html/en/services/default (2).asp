<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" --><%
MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%><HTML>
<HEAD><%
MetaTitle = "Sequins Plus Online - Services"
MetaDesc = "Sequins Plus is a full service company for dancers and studios. Everything from custom, competitive costumes to recital wear.  We also carry a full range of stretch fabrics and trim.  Dancer's jewellery and gifts are also available."
MetaKey = "sequins, plus, sequinsplus, dance, supplies, sequins plus online, ontario,"
%><TITLE><%=MetaTitle%></TITLE>
<!--#include virtual="sequinsplus/inc/meta.asp" -->
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY bgcolor=#DFDFDF leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="MM_preloadImages('/images/top_home_over.gif','/images/top_help_over.gif','/images/about_but_over.jpg','/images/products_but_over.jpg','/images/services_but_over.jpg','/images/contact_but_over.jpg')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="3"></td>
    <td width="281" height="28" valign="top"><IMG src="/images/logo_top.gif" width=281 height=28 alt=""></td>
    <td width="336" height="28" colspan="3"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="2" valign="top"><IMG src="/images/logo_lft_top_line.gif" width=155 height=9 alt=""></td>
    <td width="281" height="9" valign="top"><IMG src="/images/logo_topline.gif" width=281 height=9 alt=""></td>
    <td width="326" height="9" colspan="2" valign="top"><IMG src="/images/logo_rt_top_line.gif" width=326 height=9 alt=""></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
    <td width="155" height="26" colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" valign="top"><IMG src="/images/logo_title.gif" width=281 height=26 alt=""></td>
    <td width="326" height="26" colspan="2" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td width="155" height="11" colspan="2" valign="top"><IMG src="/images/logo_lft_bot_line.gif" width=155 height=11 alt=""></td>
    <td width="281" height="11" valign="top"><IMG src="/images/logo_bot-line.gif" width=281 height=11 alt=""></td>
    <td width="326" height="11" colspan="2" valign="top"><IMG src="/images/logo_rt_bot_line.gif" width=326 height=11 alt=""></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/nav_top.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="281" height="14" valign="top"><IMG src="/images/logo_beige.gif" width=281 height=14 alt=""></td>
    <td width="326" height="14" colspan="2" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" valign="top"><IMG src="/images/logo_spacer.gif" width=281 height=2 alt=""></td>
    <td width="326" height="2" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="174"></td>
    <td width="153" height="174" bgcolor="#CC1C0F"><a href="/en/about/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('aboutus','','/images/about_but_over.jpg',1)"><img src="/images/about_but.jpg" alt="About Us" name="aboutus" width="153" height="28" vspace="5" border="0"></a><a href="/en/products/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('products','','/images/products_but_over.jpg',1)"><img src="/images/products_but.jpg" alt="Products" name="products" width="153" height="28" vspace="5" border="0"></a><a href="/en/services/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('services','','/images/services_but_over.jpg',1)"><img src="/images/services_but.jpg" alt="Services" name="services" width="153" height="28" vspace="5" border="0"></a><a href="/en/contact/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','/images/contact_but_over.jpg',1)"><img src="/images/contact_but.jpg" alt="Contact Us" name="contact" width="153" height="29" vspace="5" border="0"></a></td>
    <td width="2" height="174" bgcolor="#FFFFFF"></td>
    <td width="607" height="174" colspan="3" valign="top"><IMG src="/images/sequinsmain.jpg" width=607 height=174 alt=""></td>
    <td width="10" height="174"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="762" height="2" colspan="5" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/list_bottom.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="607" height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="5" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="852" rowspan="3" valign="middle"> 
  <table width="153" border="0" cellpadding="0" cellspacing="0" background="/images/list_border.gif" mm_noconvert="TRUE">
    <tr>
      <td><img src="/images/services_01.gif" width="153" height="14"></td>
    </tr>
	<tr> 
      <td align="center"><img src="/images/services_pic1.jpg" width="149" height="100"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/images/services_pic2.jpg" width="149" height="199"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/images/services_pic3.jpg" width="149" height="224"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/images/services_pic4.jpg" width="149" height="207"></td>
    </tr>
    <tr>
      <td align="center"><img src="/images/services_pic5.jpg" width="149" height="108"></td>
    </tr>
  </table>
</td>
    <td width="456" height="14" colspan="3" bgcolor="#CC1C0F"></td>
    <td width="153" height="852" rowspan="3" valign="middle"> 
  <table width="153" border="0" cellpadding="0" cellspacing="0" background="/images/list_border.gif" mm_noconvert="TRUE">
    <tr>
      <td><img src="/images/services_03.gif" width="153" height="14"></td>
    </tr>
	<tr> 
      <td align="center"><img src="/images/services_pic_R1.jpg" width="149" height="143"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/images/services_pic_R2.jpg" width="149" height="243"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/images/services_pic_R3.jpg" width="149" height="103"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/images/services_pic_R4.jpg" width="149" height="242"></td>
    </tr>
    <tr>
      <td align="center"><img src="/images/services_pic_R5.jpg" width="149" height="107"></td>
    </tr>
  </table>
</td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="10"></td>
    <td width="456" height="10" colspan="3" valign="top" bgcolor="#FFFFFF"><IMG SRC="/images/transparent.gif" WIDTH=456 HEIGHT=10 ALT=""></td>
    <td width="10" height="10"></td>
  </tr>
  <tr>
    <td width="8" height="828"></td>
    <td width="456" height="828" colspan="3" valign="top">
  <table width="456" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" mm_noconvert="TRUE">
    <tr> 
      <td colspan="3"><img src="/images/pointe_fitting.gif" width="456" height="24"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td width="15" align="left"><img src="/images/transparent.gif" width="15" height="1"></td>
      <td width="424"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Of 
        course you must bring your feet! So pointe shoe fitting is only available 
        to customers' who are able to access our store.<br>
        Our staff have been trained by The National Ballet on the correct method 
        of fitting pointe shoes. This is especially important for that first pair 
        of pointe shoes. Correct fitting ensures comfort and ease, avoids stressing 
        parts of the foot, that may lead to later problems.<br>
        That all important first fitting takes at least 1 hour to achieve a proper 
        fitting. An appointment is necessary to ensure that you get our fitters 
        undivided attention at that time.<br>
        Already on pointe and need a new pair? Please make an appointment and 
        bring your old pair with you. Email <u><font color="#0000FF">service@sequinsplus.com</font></u> 
        or call ahead for appointments: <font color="#000000">(905) 433 2319</font>.</font></td>
      <td width="17" align="right"><img src="/images/transparent.gif" width="15" height="1"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="3"><img src="/images/costumes.gif" width="456" height="24"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Using 
        our international suppliers who produce the latest and most innovative 
        ready-made dancewear, we can supply you with stunning costumes whether 
        it is for solo performances or group productions. Take the stress out 
        of competition and recital!<br>
        If you don&#8217;t see what you want on our website, email <u><font color="#0000FF">service@sequinsplus.com</font></u> 
        or call us: <font color="#000000">(905) 433 2319</font>.</font></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="3"><img src="/images/custom_costumes.gif" width="456" height="24"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Need 
        a really different look? For many years we have created custom-made costumes 
        for both competition and recital.<br>
        Our years of experience have resulted in many Award Winning costumes!<br>
        Our staff is highly skilled and dedicated to making the dancers look the 
        best they possibly can. We use the lastest in fabrics and designs. We 
        can use our experience to help achieve the look you want.<br>
        Let our expertise work for you!</font></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="3"><img src="/images/fabric.gif" width="456" height="24"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 

      <td>&nbsp;</td>
      <td><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Want 
        to do it yourself? We carry large selections of the latest in stretch 
        fabrics and trim. From basic solid colour stretch Lycra, to neon colours 
        and patterns. Velvets both solid and crushed, holographic and mesh in 
        both plain and patterned. Georgette, chiffon, slinky, crepe. Pastel coloured 
        broadcloth.<br>
        Our stock is extensive, with new patterns being added constantly. Of course, 
        we carry Sequins, non-stretch and stretch. Rhinestones, boas, feathers, 
        buckles, rhinestone zippers and fringe.<br>
        If you need it for costume, chances are we have it, so look no further.</font></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="3"><img src="/images/gifts.gif" width="456" height="24"></td>

    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Need 
        something special for your young dancer?<br>
        We carry a wide range of gifts from beautiful frames for those special 
        photographs. China figurines and dolls. Gifts for birthdays and even for 
        under the tree.</font></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
  </table>
</td>
    <td width="10" height="828"></td>
  </tr>
  <tr>
    <td width="8" height="13"></td>
    <td width="762" height="13" colspan="5" valign="top">
  <table width="762" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
        <tr>
      <td><img src="/images/services_08.gif" width="153" height="13"></td>
          <td width="456" height="13" bgcolor="#CC1C0F"></td>
      <td><img src="/images/services_10.gif" width="153" height="13"></td>
    </tr>
  </table>
</td>
    <td width="10" height="13"></td>
  </tr>
  <tr>
    <td width="8" height="1"></td>
    <td width="762" height="1" colspan="5" bgcolor="#FFFFFF"></td>
    <td width="10" height="1"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="5" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td width="762" height="49" colspan="5" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="5" valign="top"><IMG src="/images/page-bottom.gif" width=762 height=7 alt=""></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="7"></td>
  </tr>
  <tr>
    <td width="8" height="1"></td>
    <td width="153" height="1"></td>
    <td width="2" height="1"></td>
    <td width="281" height="1"></td>
    <td width="173" height="1"></td>
    <td width="153" height="1"></td>
    <td width="10" height="1"></td>
  </tr>
</table>
</BODY>
</HTML>