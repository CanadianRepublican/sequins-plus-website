<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<html>
<head>
<title>Sequins Plus Online - Tell a Friend - Email Sent</title>
<link href="/main.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="400" height="195" border="0" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
      
    <td colspan="4" align="center"><img src="/images/form_logo.gif" width="281" height="109" align="top"></td>
    </tr>
    <tr> 
      <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="4"><div align="center"><font color="#CC1C0F" size="4" face="Arial, Helvetica, sans-serif"><strong><em>Your 
          Email Has Been Sent</em></strong></font></div></td>
    </tr>
    <tr> 
      <td height="5" colspan="4"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
    
    <tr> 
      
    <td height="30" colspan="4" align="center" valign="middle" class="Head2ub"><br>
      Thank You For Choosing Sequins Plus Online<br>
    </td>
    </tr>
    <tr> 
      <td width="262" align="left"><img src="/images/transparent.gif" width="15" height="1"></td>
      
    <td colspan="2" align="center" valign="middle">
    <a href="javascript:window.close()"><img src="/images/close.gif" alt="Continue Shopping" width="124" height="18" border="0"></a></td>
      <td width="15" align="right"><img src="/images/transparent.gif" width="15" height="1"></td>
    </tr>
    <tr> 
      <td height="15" colspan="4"><img src="/images/transparent.gif" width="1" height="15"></td>
    </tr>
  </table>
</body>
</html>
