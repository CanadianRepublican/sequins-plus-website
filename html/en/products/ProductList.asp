<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="DB/spDB.asp" -->
<!--#include virtual="inc/DESQLSearch.asp" -->
<%
'variable declaration
Dim oDB, ct, results, noCriteria
'create instance of the class
Set oDB = New DESQLSearch
With oDB
	'db connection string
	.DBConnectionString = "DSN=sp_sequinsplus_com;"
	'db account name
	.DBConnectionAccount = ""

	'db account's password
	.DBConnectionAcctPassword = ""

	'search type
	.SearchType = "OR"

	'determine what to search
	select case "Quick"
	case "TD"
	.SearchName
	.SearchDescriptions
	case "Title"
	.SearchName
	case "Desc"
	.SearchDescriptions
	case "Code"
	.SearchProductID
	case "Quick"
	.QuickSearch
	end select
	if Request("manu") <> "" then
	.SearchManu
	.ManuValue = Request("manu")
	end if
	if Request("cat") <> "" then
	.SearchCatagory
	.CatagoryValue = Request("cat")
	end if
	if Request("subcat") <> "" then
	.SearchSubCat
	.SubCatValue = Request("subcat")
	end if
	if Request("size") <> "" then
	.SearchSize
	.SizeValue = Request("size")
	end if
	if Request("xact") <> "" then
	.SearchXact
	end if
	if Request("MaxPrice") <> "" then
	.SearchMaxPrice
	.MaxPriceValue = Request("MaxPrice")
	end if
	if Request("MinPrice") <> "" then
	.SearchMinPrice
	.MinPriceValue = Request("MinPrice")
	end if
	'gather results into a string
	results = .Search("NNothingNN")

	'get the count of records returned
	ct = .Count

	'find out whether or not the minimum search criteria 
	'was entered...
	noCriteria = .bNoFieldsToSearch
End With

'free class instance (failure to explicitly free objects in 
'ASP is generally considered a bad idea)
Set oDB = Nothing
%><%
dim SortSQL

if request("SortOn") <> "" then SortSQL = ", spItem.Name ASC;"
if request("SortOn") = "AZ" then SortSQL = ", spItem.Name ASC;"
if request("SortOn") = "ZA" then SortSQL = ", spItem.Name DESC;"
if request("SortOn") = "Feat" then SortSQL = ", spItem.Feat ASC;"
if request("SortOn") = "HighLow" then SortSQL = ", spItem.Price DESC;"
if request("SortOn") = "LowHigh" then SortSQL = ", spItem.Price ASC;"
if request("SortOn") = "Manu" then SortSQL = ", spItemManufacturer.Name ASC;"
SQLQUEREY = results & SortSQL 
%><%
set RSResults = Server.CreateObject("ADODB.Recordset")
RSResults.ActiveConnection = SPdbString 
RSResults.Source = SQLQUEREY
RSResults.CursorType = 0
RSResults.CursorLocation = 2
RSResults.LockType = 3
RSResults.Open()
RSResults_numRows = 0
%><html>
<head>
<title>Sequins Plus Product List</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/main.css" rel="stylesheet" type="text/css">
</head>
<%
Function Highlight(strText, strFind, strBefore, strAfter)
Dim nPos
Dim nLen
Dim nLenAll
Highlight = strText
if Request("xact") <> "" then
Xact = true 
else
Xact = false
end if
sDelim = ","
if Xact then
if TRIM(strFind) = "NNothingNN" then 
redim kwa(0)
kwa(0) = ""
else
redim kwa(0)
kwa(0) = strFind
end if	
end if		
if NOT (Xact) then
		if not instr(strFind, sDelim) then sDelim = " "
		kwa = Split(strFind, sDelim)
		end if
for i = 0 to UBound(kwa)
nLen = Len(kwa(i))
nLenAll = nLen + Len(strBefore) + Len(strAfter) + 1
if nLen > 0 AND Len(Highlight) > 0 THEN
nPos = InStr(1, Highlight, kwa(i), 1)
Do While nPos > 0 
Highlight = Left(Highlight, nPos - 1) &  strBefore & Mid(Highlight, nPos, nLen) & strAfter & Mid(Highlight, nPos + nLen)
nPos = InStr(nPos +nLenAll, Highlight, kwa(i), 1)
Loop
End If
next
End Function
%>
<body>
<table width="400" height="195" border="0" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
    <td colspan="4" align="center"><img src="/images/form_logo.gif" width="281" height="109" align="top"></td>
  </tr>
  <tr> 
    <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="30" colspan="4" align="center" valign="middle" class="Head2ub"><table width="605" border="0" cellpadding="5" cellspacing="1" bgcolor="#616161" mm_noconvert="TRUE">
        <tr valign="top"> 
          <td colspan="2" bgcolor="#CC1C0F"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>PRODUCT 
            DESCRIPTION</strong></font></td>
          <td bgcolor="#CC1C0F"><div align="center"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>BRAND</strong></font></div></td>
          <td bgcolor="#CC1C0F"><div align="center"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>PRICE</strong></font></div></td>
        </tr>
        <%
		curcat = "none"
		precat1 = "none1"
		precat2 = "none2"
		startrw = 0
		endrw = HLooper1__index
		numberColumns = 1
		numrows = 5
		while (NOT RSResults.EOF or RSResults.BOF)
			startrw = endrw + 1
			endrw = endrw + numberColumns
 %>
        <%
		precat2 = precat1
		precat1 = curcat
		curcat = RSResults.Fields.Item("Cat").Value
		if precat1 = "none" AND precat2 = "none" OR precat1 = curcat AND NOT precat1 = precat2 then resulttask = "1"
		if curcat = precat1 and curcat = precat2 then resulttask = "2"
		if curcat <> precat1 AND precat1 = precat2 then resulttask = "3"
		if resulttask = "1" then%>
        <tr> 
          <td colspan="4" bgcolor="#EED39E"><font color="#333333" size="2" face="Arial, Helvetica, sans-serif"><strong><%=RSResults.Fields.Item("CatName").Value%></strong></font></td>
        </tr>
        <%startrw = startrw + 1
		 end if %>
        <%if resulttask = "2" then%>
        <tr> 
          <td rowspan="2" align="center" bgcolor="#FFFFFF"><a href="/en/products/details.asp?ItemID=<%=RSResults.Fields.Item("id").Value%>"><img src="<%="/images/products/small/" & RSResults.Fields.Item("SmallPic").Value%>" alt="<%=RSResults.Fields.Item("Name").Value%>" width="60" height="70" border="0"></a></td>
          <td bgcolor="#FFFFFF"><a href="/en/products/details.asp?ItemID=<%=RSResults.Fields.Item("id").Value%>"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong> 
            <%
spinfo = RSResults.Fields.Item("Name").Value
a = "<b>"
b = "</b>"
%>
            <%=Highlight(spinfo, key, a, b)%></strong></font></a></td>
          <td bgcolor="#FFFFFF"><div align="center"><a href="/en/products/browse.asp?Manu=<%=RSResults.Fields.Item("Manu").Value
		  %>"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"> 
              <%
spinfo = RSResults.Fields.Item("ManuName").Value
a = "<b>"
b = "</b>"
%>
              <%=Highlight(spinfo, key, a, b)%></font></a></div></td>
          <td bgcolor="#FFFFFF"><div align="center"><a href="/en/products/details.asp?ItemID=<%=RSResults.Fields.Item("id").Value%>"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong><%=FormatCurrency((RSResults.Fields.Item("Price").Value), 2, -2, -2, -2)%></strong></font></a></div></td>
        </tr>
        <tr> 
          <td colspan="3" bgcolor="#FFFFFF"><a href="/en/products/details.asp?ItemID=<%=RSResults.Fields.Item("id").Value%>"> 
            <%
spinfo = RSResults.Fields.Item("Desc").Value
a = "<b>"
b = "</b>"
%>
            <%=Highlight(spinfo, key, a, b)%></a></td>
        </tr>
        <%
		startrw = startrw + 1
		oldcat = RSResults.Fields.Item("CatName").Value
		oldcid = RSResults.Fields.Item("cat").Value
		Descr = ""
		RSResults.MoveNext()
		numrows=numrows-1
		end if
		wend
		%>
      </table> </td>
  </tr>
  <tr> 
    <td width="262" align="left"><img src="/images/transparent.gif" width="15" height="1"></td>
    <td colspan="2" align="center" valign="middle">&nbsp; </td>
    <td width="15" align="right"><img src="/images/transparent.gif" width="15" height="1"></td>
  </tr>
  <tr> 
    <td height="15" colspan="4"><img src="/images/transparent.gif" width="1" height="15"></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
