<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="DB/spDB.asp" -->
<!--#include virtual="inc/cart.asp" --><%Function ValidateEmail(email)
   dim atCnt
   ValidateEmail = false

   if len(cstr(email)) < 7 then 
      ValidateEmail = true

   elseif instr(email,"@") = 0 then
      ValidateEmail = true

   elseif instr(email,".") = 0 then
      ValidateEmail = true

   elseif len(email) - instrrev(email,".") > 3 then
      ValidateEmail = true

   elseif instr(email,"_") <> 0 and _
      instrrev(email,"_") > instrrev(email,"@") then
      ValidateEmail = true

   else
   atCnt = 0
      for i = 1 to len(email)
         if mid(email,i,1) = "@" then
            atCnt = atCnt + 1
         end if
      next

   if atCnt > 1 then
      ValidateEmail = true
   end if

   for i = 1 to len(email)
      if not isnumeric(mid(email,i,1)) and _
         (lcase(mid(email,i,1)) < "a" or _
         lcase(mid(email,i,1)) > "z") and _
         mid(email,i,1) <> "_" and _
         mid(email,i,1) <> "." and _
         mid(email,i,1) <> "@" and _
         mid(email,i,1) <> "-" then
         ValidateEmail = true
      end if
   next
   end if
End Function%>
<%
' ------------ validate form ------------
bFormSubmitted = false
bFormBad = false
	bBad01 = false : If Len(Request.Form("FirstName")) = 0 Then bBad01 = true
	bBad02 = false : If Len(Request.Form("LastName")) = 0 Then bBad02 = true
	bBad03 = false : If Len(Request.Form("Company")) = 0 Then bBad03 = true
	bBad04 = false : If Len(Request.Form("Address1")) = 0 Then bBad04 = true
	bBad05 = false : If Len(Request.Form("City")) = 0 Then bBad05 = true
	bBad06 = false : If Len(Request.Form("Province")) = 0 Then bBad06 = true
	bBad07 = false : If Len(Request.Form("email")) = 0 Then bBad07 = true
	bBad08 = false ': if instr(Request.Form("email"), "@") = TRUE AND instr(Request.Form("email"), ".") = TRUE then bBad08 = true
	If  ( bBad01 OR bBad02 OR bBad03 OR bBad04 OR bBad05 OR bBad06 OR bBad07 OR bBad08)  AND _
		( Request.Form("MM_insert") <> "" ) Then bFormBad = true
%>
<%
' *** Edit Operations: declare variables
MM_editAction = CStr(Request("URL"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Request.QueryString
End If
' boolean to abort record edit
MM_abortEdit = bFormBad
' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: set variables
If (CStr(Request("MM_insert")) <> "") Then
  MM_editConnection = SPdbString
  MM_editTable = "spContacts"
  MM_editRedirectUrl = "/en/contact/finish.asp"
  MM_fieldsStr  = "FirstName|value|LastName|value|Company|value|Address1|value|Appartment|value|Address2|value|City|value|Province|value|Postal|value|Country|value|PhoneAC1|value|Phone31|value|Phone41|value|Ext1|value|PhoneAC2|value|Phone32|value|Phone42|value|Ext2|value|email|value|Subject|value|OrderNumber|value|PurchaceDate|value|Message|value|resEmail|value|resTelephone|value|NoResponse|value|ContactType|value"
  MM_columnsStr = "FirstName|',none,''|LastName|',none,''|Company|',none,''|Address1|',none,''|Appartment|',none,''|Address2|',none,''|City|',none,''|Province|',none,''|Postal|',none,''|Country|',none,''|PhoneAC1|none,none,NULL|Phone31|none,none,NULL|Phone41|none,none,NULL|Ext1|none,none,NULL|PhoneAC2|none,none,NULL|Phone32|none,none,NULL|Phone42|none,none,NULL|Ext2|none,none,NULL|email|',none,''|Subject|',none,''|OrderNum|',none,''|PurchaseDate|',none,''|Message|',none,''|ReplyByEmail|none,1,0|ReplyByPhone|none,1,0|NoReply|none,1,0|ContactType|',none,''"
  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  ' set the form values
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(i+1) = CStr(Request.Form(MM_fields(i)))
  Next
  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If
End If
%>
<%
' *** Insert Record: construct a sql insert statement and execute it
If (CStr(Request("MM_insert")) <> "") Then
  ' create the sql insert statement
  MM_tableValues = ""
  MM_dbValues = ""
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    If (Delim = "none") Then Delim = ""
    AltVal = MM_typeArray(1)
    If (AltVal = "none") Then AltVal = ""
    EmptyVal = MM_typeArray(2)
    If (EmptyVal = "none") Then EmptyVal = ""
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End if
    MM_tableValues = MM_tableValues & MM_columns(i)
    MM_dbValues = MM_dbValues & FormVal
  Next
  MM_editQuery = "insert into " & MM_editTable & " (" & MM_tableValues & ") values (" & MM_dbValues & ")"
  If (Not MM_abortEdit) Then
    ' execute the insert
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_editConnection
    MM_editCmd.CommandText = MM_editQuery
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close
       bFormSubmitted = true
   
' --------------- EMAIL CONTACT ---------------
	mail = mail & "<html>"
	mail = mail & "<head>"
	mail = mail & "<title>Sequins Plus Online - Service Inquery Email Conformation</title>"
	mail = mail & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">"
	mail = mail & "</head>"
	mail = mail & "<body leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">"
	mail = mail & "<table width=""600"" border=""0"" cellpadding=""0"" cellspacing=""0"" background=""http://www.sequinsplus.com/images/form_bg.gif"">"
	mail = mail & "<tr>"
	mail = mail & "<td colspan=""2"" align=""center""><img src=""http://www.sequinsplus.com/images/form_logo.gif"" width=""281"" height=""109""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>" 
	mail = mail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td><div align=""center""><font color=""#CC1C0F"" size=""3"" face=""Arial, Helvetica, sans-serif""><strong><em>Your e-mail has been successfully recieved.</em></strong></font></div></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td colspan=""2""><table width=""400"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
	mail = mail & "<tr>"
	mail = mail & "<td><blockquote>" 
	mail = mail & "<p><font color=""#616161"" size=""2"" face=""Arial, Helvetica, sans-serif"">This" 
	mail = mail & "is an automatic email response from Sequins Plus Online. This "
	mail = mail & "page is a confirmation receipt that your email has been successfully" 
	mail = mail & "received by our server.<br>"
	mail = mail & "<br>"
	mail = mail & "One of the Sequins Plus representatives will reply to your inquiry or comment shortly</font></p>"
	mail = mail & "</blockquote></td>"
	mail = mail & "</tr>"
	mail = mail & "</table>"
	mail = mail & "</td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""30""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td colspan=""2"" align=""center"">&nbsp;</td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td height=""10"" colspan=""2"">&nbsp;</td>"
	mail = mail & "</tr>"
	mail = mail & "</table>"
	mail = mail & "</body>"
	mail = mail & "</html>"
	Dim ObjMail
	
	etype       = Request.Form("ContactType")       
	ename       = Request.Form("Name")    
	eemail      = Request.Form("email")     
	esubject    = Request.Form("Subject")  
	emessage    = Request.Form("Message")   
	Set ObjMail = Server.CreateObject("CDONTS.NewMail")
	ObjMail.From = "Sequins Plus Online - Automated Response<service@sequinsplus.com>"
	ObjMail.To = ename &"<"&eemail&">"
	ObjMail.CC = "NightCrawler<rahim@nasupplies.com>"
	ObjMail.Subject = "Sequins Plus Online - We Recived Your eMail!!"
	ObjMail.Importance = 1 '0-low,1-normal,2-high
	ObjMail.BodyFormat = 0 '0-html,1-text
	ObjMail.Mailformat = 0 '0-mime,1-plain text
	ObjMail.Body = mail
	ObjMail.Send
	Set ObjMail = Nothing
servermail = servermail & "<html>"
servermail = servermail & "<head>"
servermail = servermail & "<title>Sequins Plus Online - Service Inquery Email Details</title>"
servermail = servermail & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">"
servermail = servermail & "</head>"
servermail = servermail & "<body leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">"
servermail = servermail & "<table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"" background=""http://www.sequinsplus.com/images/form_bg.gif"">"
servermail = servermail & "<tr>"
servermail = servermail & "<td colspan=""2"" align=""center""><img src=""http://www.sequinsplus.com/images/form_logo.gif"" width=""281"" height=""109""></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td><div align=""center""><font color=""#CC1C0F"" size=""3"" face=""Arial, Helvetica, sans-serif""><strong>General Email Details</strong></font></div></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td colspan=""2""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
servermail = servermail & "<tr>" 
servermail = servermail & "<td>"
servermail = servermail & "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
servermail = servermail & "<tr>"
servermail = servermail & "<td align=""right""><font color=""#616161"" size=""2"" face=""Arial, Helvetica, sans-serif"">Name:</font></td>"
servermail = servermail & "<td ><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&ename&"</strong></font></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td align=""right""><font color=""#616161"" size=""2"" face=""Arial, Helvetica, sans-serif"">Email Address:</font></td>"
servermail = servermail & "<td><a href=""mailto:"&eemail&"""><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eemail&"</strong></font></a></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td align=""right""><font color=""#616161"" size=""2"" face=""Arial, Helvetica, sans-serif"">Subject:</font></td>"
servermail = servermail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&esubject&"</strong></font></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td align=""right""><font color=""#616161"" size=""2"" face=""Arial, Helvetica, sans-serif"">Message:</font></td>"
servermail = servermail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&emessage&"</strong></font></td>"
servermail = servermail & "</tr>"
servermail = servermail & "</table>"
servermail = servermail & "</td>"
servermail = servermail & "</tr>"
servermail = servermail & "</table>"
servermail = servermail & "</td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr> "
servermail = servermail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""30""></td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td colspan=""2"" align=""center"">&nbsp;</td>"
servermail = servermail & "</tr>"
servermail = servermail & "<tr>"
servermail = servermail & "<td height=""10"" colspan=""2"">&nbsp;</td>"
servermail = servermail & "</tr>"
servermail = servermail & "</table>"
servermail = servermail & "</body>"
servermail = servermail & "</html>"
Set ObjMail = Server.CreateObject("CDONTS.NewMail")
	ObjMail.From = ename &"<"&eemail&">"
	ObjMail.To = "Sequins Plus Online<service@sequinsplus.com>"
	ObjMail.CC = "NightCrawler<rahim@nasupplies.com>"
	ObjMail.Subject = "Sequins Plus Online - " & esubject
	ObjMail.Importance = 1 '0-low,1-normal,2-high
	ObjMail.BodyFormat = 0 '0-html,1-text
	ObjMail.Mailformat = 0 '0-mime,1-plain text
	ObjMail.Body = servermail
	ObjMail.Send
	Set ObjMail = Nothing
' ----------------- END EMAIL -----------------
 
   end if
   end if
   if (MM_editRedirectUrl <> "") Then
    Response.Redirect(MM_editRedirectUrl)
    End If
   %><%
set spCountries = Server.CreateObject("ADODB.Recordset")
spCountries.ActiveConnection = SPdbString
spCountries.Source = "SELECT * FROM Countries"
spCountries.CursorType = 0
spCountries.CursorLocation = 2
spCountries.LockType = 3
spCountries.Open()
spCountries_numRows = 0
%>
<html>
<head>
<title>Sequins Plus Online - Service Email</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/main.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form method="post" action="" name="contactForm"><table width="600" height="1088" border="0" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
    <td height="109" colspan="2" align="center"><img src="/images/form_logo.gif" width="281" height="109"></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="144" colspan="2">
<blockquote> 
        <p><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
          Need assistance? Our customer service people are here to help. To inquire 
          about the products and services found on our website or in our store, 
          contact us by phone, mail or email. We're happy to assist you any way 
          we can.<br>
          <br>
          If you have a concern about a product which you have already ordered, 
          contact us and one of our customer service representatives will contact 
          you shortly. We are committed to ensuring our customers utmost satisfaction 
          when dealing with Sequins Plus.<br>
		  <br>
          The information you provide will be handled according to our <font color="#0033FF"><u>privacy 
          policy</u></font>.</font></p>
      </blockquote></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="16" colspan="2"><img src="/images/transparent.gif" width="30" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#FF0000">*</font><font color="#000000" size="2"> 
      Indicates required feilds</font></font></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td width="201" height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
      First Name : <font color="#FF0000">*</font></strong></font></td>
      <td width="399" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="FirstName" type="text" id="FirstName" value="<%=request("FirstName")%>" size="30"> 
        <%If bFormBad = TRUE  AND bBad01 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
      Last Name : <font color="#FF0000">*</font></strong></font></td>
      <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="LastName" type="text" id="LastName" value="<%=request("LastName")%>" size="30"> 
        <%If bFormBad = TRUE  AND bBad02 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Studio 
      / Organization : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td colspan="2" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Company" type="text" id="Company" value="<%=request("Company")%>" size="40"> 
        <%If bFormBad = TRUE  AND bBad03 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="18"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong></strong></font> 
    </td>
    <td colspan="2" valign="bottom"><font size="2" face="Arial, Helvetica, sans-serif"><font color="#000000" size="1">(Street 
      Address - In this order please: house or building number and street name)</font></font></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Street 
      Address : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> <input name="Address1" type="text" id="Address1" value="<%=request("Address1")%>" size="40"> 
        <%If bFormBad = TRUE  AND bBad04 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Apartment 
      / Suite : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Appartment" type="text" id="Appartment" value="<%=request("Appartment")%>" size="5"> 
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
  </tr>
  <tr> 
    <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Street 
      Address 2 : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td height="20" colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Address2" type="text" id="Address2" value="<%=request("Address2")%>" size="40"> 
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
  </tr>
  <tr> 
    <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>City 
      : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td height="20" colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="City" type="text" id="City" value="<%=request("City")%>" size="40"> 
        <%If bFormBad = TRUE  AND bBad05 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Province 
      / State : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> <input name="Province" type="text" id="Province" value="<%=request("Province")%>"> 
        <%If bFormBad = TRUE  AND bBad06 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Postal 
      Code : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> <input name="Postal" type="text" id="Postal" value="<%=request("Postal")%>"></td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
  </tr>
  <tr> 
    <td height="22" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Country 
      : <img src="/images/transparent.gif" width="5" height="8"></strong></font></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> <select name="Country" class="input_textbox">
          <option value="">Please Choose A Country</option>
          <%
While (NOT spCountries.EOF)
%>
          <option value="<%=(spCountries.Fields.Item("Country").Value)%>" <%if (CStr(spCountries.Fields.Item("Country").Value) = CStr(Request.Form("Country"))) then Response.Write("SELECTED") : Response.Write("")%> ><%=(spCountries.Fields.Item("Country").Value)%></option>
          <%
  spCountries.MoveNext()
Wend

  spCountries.close
%>
        </select></td>
  </tr>
  <tr> 
    <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="27" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Telephone 
      : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <font size="5" face="Arial, Helvetica, sans-serif">(</font> <input name="PhoneAC1" type="text" id="PhoneAC1" value="<%=request("PhoneAC1")%>" size="5"> 
        <font size="5" face="Arial, Helvetica, sans-serif">)</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone31" type="text" id="Phone31" value="<%=request("Phone31")%>" size="5"> 
        <img src="/images/transparent.gif" width="5" height="1"><font size="5" face="Arial, Helvetica, sans-serif">-</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone41" type="text" id="Phone41" value="<%=request("Phone41")%>" size="7"> 
        <img src="/images/transparent.gif" width="15" height="0"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Ext 
        : <img src="/images/transparent.gif" width="3" height="8"> 
        <input name="Ext1" type="text" id="Ext1" value="<%=request("Ext1")%>" size="5">
        </strong></font></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="27" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Secondary 
      Phone # : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <font size="5" face="Arial, Helvetica, sans-serif">(</font> <input name="PhoneAC2" type="text" id="PhoneAC2" value="<%=request("PhoneAC2")%>" size="5"> 
        <font size="5" face="Arial, Helvetica, sans-serif">)</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone32" type="text" id="Phone32" value="<%=request("Phone32")%>" size="5"> 
        <img src="/images/transparent.gif" width="5" height="1"><font size="5" face="Arial, Helvetica, sans-serif">-</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone42" type="text" id="Phone42" value="<%=request("Phone42")%>" size="7"> 
        <img src="/images/transparent.gif" width="15" height="0"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Ext 
        : <img src="/images/transparent.gif" width="3" height="1"> 
        <input name="Ext2" type="text" id="Ext2" value="<%=request("Ext2")%>" size="5">
        </strong></font></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Email 
      Address : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td align="left" valign="bottom"><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="email" type="text" id="email" value="<%=request("email")%>" size="40"> 
        <% If bFormBad = TRUE  AND bBad07 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% end if%>
        <% if bFormBad = TRUE AND bBad08 = TRUE AND bBad07 = FALSE Then %>
        <font color="#CC0000">Invalid Email Address</font>
        <%end if%>
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Subject 
      : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Subject" type="text" id="Subject" value="<%=request("Subject")%>" size="40"> 
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Order 
      Number : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
    <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
      <input name="OrderNumber" type="text" id="OrderNumber" value="<%=request("OrderNo")%>" size="20">
      <img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong>(If 
      Applicable)</strong></font></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="18" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Purchase 
      Date : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
    <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
      <input name="PurchaceDate" type="text" id="PurchaceDate" value="<%=request("PDate")%>" size="20">
      <img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong>(In 
      this format: MM/DD/YYYY)</strong></font></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="109" align="right" valign="top"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Message 
      : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td height="109" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <textarea name="Message" cols="40" rows="7" id="Message"><%=request("Message")%></textarea> 
      </td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="16" colspan="2"><img src="/images/transparent.gif" width="20" height="1"><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif"><strong>*</strong></font><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"> 
      How would you like us to contact you?</font></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="16" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Email</strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
    <td><input name="resEmail" type="checkbox" id="resEmail" value="true" checked></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="16" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Telephone</strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
    <td><input name="resTelephone" type="checkbox" id="resTelephone" value="true"></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="16" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>No 
      Response</strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
    <td><input name="NoResponse" type="checkbox" id="NoResponse" value="true"></td>
  </tr>
  <tr> 
    <td height="30" colspan="2"><img src="/images/transparent.gif" width="1" height="30"></td>
  </tr>
  <tr align="center"> 
      <td height="18" colspan="2"><a href="/en/contact/service.asp"><img src="/images/clear_form.gif" alt="Blank Form" width="107" height="18" border="0"></a><img src="/images/transparent.gif" width="100" height="1"><a href="#"><input type="image" border="0" name="iF" src="/images/send_email.gif" width="101" height="18" alt="Submit Email">
	<input type="hidden" name="MM_insert" value="true">
	<input type="hidden" name="ContactType" value="Service"></a></td>
  </tr>
  <tr> 
    <td height="18" colspan="2">&nbsp;</td>
  </tr>
  <tr> 
    <td height="18" colspan="2">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
