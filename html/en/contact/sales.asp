<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="DB/spDB.asp" -->
<!--#include virtual="inc/cart.asp" --><%
Function ValidateEmail(email)
   dim atCnt
   ValidateEmail = false

   if len(cstr(email)) < 7 then 
      ValidateEmail = true

   elseif instr(email,"@") = 0 then
      ValidateEmail = true

   elseif instr(email,".") = 0 then
      ValidateEmail = true

   elseif Len(email) - instrrev(email,".") > 3 then
      ValidateEmail = true

   elseif instr(email,"_") <> 0 and _
      instrrev(email,"_") > instrrev(email,"@") then
      ValidateEmail = true

   else
   atCnt = 0
      for i = 1 to len(email)
         if mid(email,i,1) = "@" then
            atCnt = atCnt + 1
         end if
      next

   if atCnt > 1 then
      ValidateEmail = true
   end if

   for i = 1 to len(email)
      if not isnumeric(mid(email,i,1)) and _
         (lcase(mid(email,i,1)) < "a" or _
         lcase(mid(email,i,1)) > "z") and _
         mid(email,i,1) <> "_" and _
         mid(email,i,1) <> "." and _
         mid(email,i,1) <> "@" and _
         mid(email,i,1) <> "-" then
         ValidateEmail = true
      end if
   next
   end if
End Function
%><%
' ------------ validate form ------------
bFormSubmitted = false
bFormBad = false
	bBad01 = false : If Len(Request.Form("FirstName")) = 0 Then bBad01 = true
	bBad02 = false : If Len(Request.Form("LastName")) = 0 Then bBad02 = true
	bBad03 = false : If Len(Request.Form("Company")) = 0 Then bBad03 = true
	bBad04 = false : If Len(Request.Form("Address1")) = 0 Then bBad04 = true
	bBad05 = false : If Len(Request.Form("City")) = 0 Then bBad05 = true
	bBad06 = false : If Len(Request.Form("Province")) = 0 Then bBad06 = true
	bBad07 = false : If Len(Request.Form("email")) = 0 Then bBad07 = true
	bBad08 = false : if ValidateEmail(request.form("email")) then bBad08 = true
	bBad09 = false : if not (Request.Form("resEmail") = "true" OR Request.Form("resTelephone") = "true" OR Request.Form("NoResponse") = "true") then bBad09 = true
	bBad10 = false : if Request.Form("resTelephone") = "true" and Len(Request.Form("PhoneAC1")) = 0 AND Len(Request.Form("Phone31")) = 0 AND Len(Request.Form("Phone41")) = 0 then bBad10 = true
	If  ( bBad01 OR bBad02 OR bBad03 OR bBad04 OR bBad05 OR bBad06 OR bBad07 OR bBad08 OR bBad09 OR bBad10)  AND _
		( Request.Form("MM_insert") <> "" ) Then bFormBad = true
%>
<%
' *** Edit Operations: declare variables
MM_editAction = CStr(Request("URL"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Request.QueryString
End If
' boolean to abort record edit
MM_abortEdit = bFormBad
' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: set variables
If (CStr(Request("MM_insert")) <> "") Then
  MM_editConnection = SPdbString
  MM_editTable = "spContacts"
  MM_editRedirectUrl = "/en/contact/finish.asp"
  MM_fieldsStr  = "FirstName|value|LastName|value|Company|value|Address1|value|Appartment|value|Address2|value|City|value|Province|value|Postal|value|Country|value|PhoneAC1|value|Phone31|value|Phone41|value|Ext1|value|PhoneAC2|value|Phone32|value|Phone42|value|Ext2|value|email|value|Subject|value|Message|value|repEmail|value|repTelephone|value|NoResponse|value|ContactType|value"
  MM_columnsStr = "FirstName|',none,''|LastName|',none,''|Company|',none,''|Address1|',none,''|Appartment|',none,''|Address2|',none,''|City|',none,''|Province|',none,''|Postal|',none,''|Country|',none,''|PhoneAC1|none,none,NULL|Phone31|none,none,NULL|Phone41|none,none,NULL|Ext1|none,none,NULL|PhoneAC2|none,none,NULL|Phone32|none,none,NULL|Phone42|none,none,NULL|Ext2|none,none,NULL|email|',none,''|Subject|',none,''|Message|',none,''|ReplyByEmail|none,1,0|ReplyByPhone|none,1,0|NoReply|none,1,0|ContactType|',none,''"
  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  ' set the form values
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(i+1) = CStr(Request.Form(MM_fields(i)))
  Next
  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If
End If
%>
<%
' *** Insert Record: construct a sql insert statement and execute it
If (CStr(Request("MM_insert")) <> "") Then
  ' create the sql insert statement
  MM_tableValues = ""
  MM_dbValues = ""
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    If (Delim = "none") Then Delim = ""
    AltVal = MM_typeArray(1)
    If (AltVal = "none") Then AltVal = ""
    EmptyVal = MM_typeArray(2)
    If (EmptyVal = "none") Then EmptyVal = ""
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End if
    MM_tableValues = MM_tableValues & MM_columns(i)
    MM_dbValues = MM_dbValues & FormVal
  Next
  MM_editQuery = "insert into " & MM_editTable & " (" & MM_tableValues & ") values (" & MM_dbValues & ")"
  If (Not MM_abortEdit) Then
    ' execute the insert
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_editConnection
    MM_editCmd.CommandText = MM_editQuery
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close
       bFormSubmitted = true
   
' --------------- EMAIL CONTACT ---------------
	mail = mail & "<html>"
	mail = mail & "<head>"
	mail = mail & "<title>Sequins Plus Online - General Email Conformation</title>"
	mail = mail & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">"
	mail = mail & "</head>"
	mail = mail & "<body leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">"
	mail = mail & "<table width=""600"" border=""0"" cellpadding=""0"" cellspacing=""0"" background=""http://www.sequinsplus.com/images/form_bg.gif"">"
	mail = mail & "<tr>"
	mail = mail & "<td colspan=""2"" align=""center""><img src=""http://www.sequinsplus.com/images/form_logo.gif"" width=""281"" height=""109""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>" 
	mail = mail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td><div align=""center""><font color=""#CC1C0F"" size=""3"" face=""Arial, Helvetica, sans-serif""><strong><em>Your e-mail has been successfully recieved.</em></strong></font></div></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td colspan=""2""><table width=""400"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
	mail = mail & "<tr>"
	mail = mail & "<td><blockquote>" 
	mail = mail & "<p><font color=""#616161"" size=""2"" face=""Arial, Helvetica, sans-serif"">This" 
	mail = mail & "is an automatic email response from Sequins Plus Online. This "
	mail = mail & "page is a confirmation receipt that your email has been successfully" 
	mail = mail & "received by our server.<br>"
	mail = mail & "<br>"
	mail = mail & "One of the Sequins Plus representatives will reply to your inquiry or comment shortly</font></p>"
	mail = mail & "</blockquote></td>"
	mail = mail & "</tr>"
	mail = mail & "</table>"
	mail = mail & "</td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""30""></td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td colspan=""2"" align=""center"">&nbsp;</td>"
	mail = mail & "</tr>"
	mail = mail & "<tr>"
	mail = mail & "<td height=""10"" colspan=""2"">&nbsp;</td>"
	mail = mail & "</tr>"
	mail = mail & "</table>"
	mail = mail & "</body>"
	mail = mail & "</html>"
	Dim ObjMail
	etype = Request.Form("ContactType")       
	eFname = Request.Form("FirstName")
	eLname = Request.Form("LastName")
	eEmail = Request.Form("email")     
	eSubject = Request.Form("Subject")  
	eMessage = Request.Form("Message")
	eAddress1 = Request.Form("Address1")
	eAddress2 = Request.Form("Address2")
	eAppartment = Request.Form("Appartment")
	eCompany = Request.Form("Company")
	ePhone2 = "("&Request.Form("PhoneAC2")&") "&Request.Form("Phone32")&"-"& Request.Form("Phone42")
	ePhone1 = "("&Request.Form("PhoneAC1")&") "&Request.Form("Phone31")&"-"& Request.Form("Phone41")
	eCity = Request.Form("City")
	eProvnice = Request.Form("Province")
	eCountry = Request.Form("Country")
	ePostal = Request.Form("Postal")
	eContactMeBy = "ttt"  
	Set ObjMail = Server.CreateObject("CDONTS.NewMail")
	ObjMail.From = "Sequins Plus Online - Automated Response<info@sequinsplus.com>"
	ObjMail.To = eFname &"<"&eEmail&">"
	ObjMail.CC = "NightCrawler<rahim@nasupplies.com>"
	ObjMail.Subject = "Sequins Plus Online - We Recived Your eMail!!"
	ObjMail.Importance = 1 '0-low,1-normal,2-high
	ObjMail.BodyFormat = 0 '0-html,1-text
	ObjMail.Mailformat = 0 '0-mime,1-plain text
	ObjMail.Body = mail
	ObjMail.Send
	Set ObjMail = Nothing
ContactDetailsEmail = ContactDetailsEmail & "<html>"
ContactDetailsEmail = ContactDetailsEmail & "<head>"
ContactDetailsEmail = ContactDetailsEmail & "<title>Sequins Plus Online - Sales Inquirey Email Details</title>"
ContactDetailsEmail = ContactDetailsEmail & "<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">"
ContactDetailsEmail = ContactDetailsEmail & "</head>"
ContactDetailsEmail = ContactDetailsEmail & "<body leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">"
ContactDetailsEmail = ContactDetailsEmail & "<table width=""600"" border=""0"" align=""left"" cellpadding=""0"" cellspacing=""0"" background=""http://www.sequinsplus.com/images/form_bg.gif"">"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td colspan=""2"" align=""center""><img src=""http://www.sequinsplus.com/images/form_logo.gif"" width=""281"" height=""109""></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>" 
ContactDetailsEmail = ContactDetailsEmail & "<td><div align=""center""><font color=""#CC1C0F"" size=""3"" face=""Arial, Helvetica, sans-serif""><strong>Sales" 
ContactDetailsEmail = ContactDetailsEmail & " Inquerey Email Details</strong></font></div></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>" 
ContactDetailsEmail = ContactDetailsEmail & "<td height=""10"" colspan=""2""><img src=""http://www.sequinsplus.com/images/transparent.gif"" width=""1"" height=""10""></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td colspan=""2""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td> <table width=""600"" border=""0"" cellspacing=""2"" cellpadding=""2"">"
if Not Len(eFname) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td width=""143"" align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">First Name:</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td width=""457""><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eFname&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(eLname) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Last Name:</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eLname&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(eCompany) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Company:</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eCompany&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(eAddress1) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Address Line One</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eAddress1&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(eAddress2) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Address Line Two</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eAddress2&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(eAppartment) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Appartment No.</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eAppartment&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(eCity) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">City</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eCity&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(eProvince) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Province</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eProvince&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
if Not Len(ePostal) = 0 then
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Postal Code</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&ePostal&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
end if
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td height=""20"" align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Country</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eCountry&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Telephone No. 1</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&ePhone1&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Telephone No. 2</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&ePhone2&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">eMail Address</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eEmail&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Subject</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eSubject&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Contact Me By Email</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>Yes!</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Contact Me By Phone</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>No</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Don't Contact Me</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>No</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td align=""right""><font color=""#575757"" size=""2"" face=""Courier New, Courier, mono"">Message</font></td>"
ContactDetailsEmail = ContactDetailsEmail & "<td><font color=""#000000"" size=""2"" face=""Arial, Helvetica, sans-serif""><strong>"&eMessage&"</strong></font></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "</table></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td>&nbsp;</td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "</table></td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "<tr>"
ContactDetailsEmail = ContactDetailsEmail & "<td height=""10"" colspan=""2"">&nbsp;</td>"
ContactDetailsEmail = ContactDetailsEmail & "</tr>"
ContactDetailsEmail = ContactDetailsEmail & "</table>"
ContactDetailsEmail = ContactDetailsEmail & "</body>"
ContactDetailsEmail = ContactDetailsEmail & "</html>"
Set ObjMail = Server.CreateObject("CDONTS.NewMail")
	ObjMail.From = "Sequins Plus Online - " & eFname &" "&eLname&"<"&eEmail&">"
	ObjMail.To = "Sequins Plus Online<sales@sequinsplus.com>"
	' ObjMail.CC = "NightCrawler<rahim@nasupplies.com>"
	ObjMail.Subject = "Sequins Plus Online - " & eSubject
	ObjMail.Importance = 1 '0-low,1-normal,2-high
	ObjMail.BodyFormat = 0 '0-html,1-text
	ObjMail.Mailformat = 0 '0-mime,1-plain text
	ObjMail.Body = ContactDetailsEmail
	ObjMail.Send
	Set ObjMail = Nothing
' ----------------- END EMAIL -----------------
If (MM_editRedirectUrl <> "") Then
      Response.Redirect(MM_editRedirectUrl)
    End If
 
   end if
   
   end if
   
   %><%
set spCountries = Server.CreateObject("ADODB.Recordset")
spCountries.ActiveConnection = SPdbString
spCountries.Source = "SELECT * FROM Countries"
spCountries.CursorType = 0
spCountries.CursorLocation = 2
spCountries.LockType = 3
spCountries.Open()
spCountries_numRows = 0
%>
<html>
<head>
<title>Sequins Plus Online - Sales Email</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/main.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<form method="post" action="<%=MM_editAction%>" name="contactForm">
  <table width="600" border="0" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
    <tr> 
      <td colspan="2" align="center"><img src="/images/form_logo.gif" width="281" height="109"></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="2"><blockquote> 
          <p><font color="CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong>Attention 
            Dance Teachers!</strong></font><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><br>
            <br>
            Sequins Plus offers a special discount on all orders you place with 
            us. For more information on this special offer made exclusively to 
            you, contact us and ask about our <font color="#0033FF"><u>Dance Teacher 
            Discount Program</u></font>.<br>
            <br>
            Is there a particular product you would like us to carry?<br>
            We would love to hear about new products that you, our customers, 
            would like us to make available to you.<br>
            <br>
            The information you provide will be handled according to our <font color="#0033FF"><u>privacy 
            policy</u></font>.</font></p>
        </blockquote></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="2"><img src="/images/transparent.gif" width="30" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#FF0000">*</font><font color="#000000" size="2"> 
        Indicates required feilds</font></font></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td width="201" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
        First Name : <font color="#FF0000">*</font></strong></font></td>
      <td width="399" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="FirstName" type="text" id="FirstName" value="<%=request("FirstName")%>" size="30"> 
        <%If bFormBad = TRUE  AND bBad01 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
        Last Name : <font color="#FF0000">*</font></strong></font></td>
      <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="LastName" type="text" id="LastName" value="<%=request("LastName")%>" size="30"> 
        <%If bFormBad = TRUE  AND bBad02 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Studio 
        / Organization : <font color="#FF0000">*</font></strong></font></td>
      <td colspan="2" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Company" type="text" id="Company" value="<%=request("Company")%>" size="40"> 
        <%If bFormBad = TRUE  AND bBad03 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong></strong></font> 
      </td>
      <td colspan="2" valign="bottom"><font size="2" face="Arial, Helvetica, sans-serif"><font color="#000000" size="1">(Street 
        Address - In this order please: house or building number and street name)</font></font></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Studio 
        Address : <font color="#FF0000">*</font></strong></font></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Address1" type="text" id="Address1" value="<%=request("Address1")%>" size="40"> 
        <%If bFormBad = TRUE  AND bBad04 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Apartment 
        / Suite : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Appartment" type="text" id="Appartment" value="<%=request("Appartment")%>" size="5"> 
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Street 
        Address 2 : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
      <td height="20" colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Address2" type="text" id="Address2" value="<%=request("Address2")%>" size="40"> 
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>City 
        : <font color="#FF0000">*</font></strong></font></td>
      <td height="20" colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="City" type="text" id="City" value="<%=request("City")%>" size="40"> 
        <%If bFormBad = TRUE  AND bBad05 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Province 
        / State : <font color="#FF0000">*</font></strong></font></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Province" type="text" id="Province" value="<%=request("Province")%>"> 
        <%If bFormBad = TRUE  AND bBad06 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Postal 
        Code : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Postal" type="text" id="Postal" value="<%=request("Postal")%>"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Country 
        : <img src="/images/transparent.gif" width="5" height="8"></strong></font></td>
      <td colspan="2"><img src="/images/transparent.gif" width="5" height="1"> 
        <select name="Country" class="input_textbox">
          <option value="">Please Choose A Country</option>
          <%
While (NOT spCountries.EOF)
%>
          <option value="<%=(spCountries.Fields.Item("Country").Value)%>" <%if (CStr(spCountries.Fields.Item("Country").Value) = CStr(Request.Form("Country"))) then Response.Write("SELECTED") : Response.Write("")%> ><%=(spCountries.Fields.Item("Country").Value)%></option>
          <%
  spCountries.MoveNext()
Wend

  spCountries.close
%>
        </select></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Telephone 
        : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <font size="5" face="Arial, Helvetica, sans-serif">(</font> <input name="PhoneAC1" type="text" id="PhoneAC1" value="<%=request("PhoneAC1")%>" size="5"> 
        <font size="5" face="Arial, Helvetica, sans-serif">)</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone31" type="text" id="Phone31" value="<%=request("Phone31")%>" size="5"> 
        <img src="/images/transparent.gif" width="5" height="1"><font size="5" face="Arial, Helvetica, sans-serif">-</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone41" type="text" id="Phone41" value="<%=request("Phone42")%>" size="7"> 
        <img src="/images/transparent.gif" width="15" height="0"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Ext 
        : <img src="/images/transparent.gif" width="3" height="8"> 
        <input name="Ext1" type="text" id="Ext1" value="<%=request("Ext1")%>" size="5"></strong></font> <% If bFormBad = TRUE  AND bBad10 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% end if%></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Secondary 
        Phone # : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <font size="5" face="Arial, Helvetica, sans-serif">(</font> <input name="PhoneAC2" type="text" id="PhoneAC2" value="<%=request("PhoneAC2")%>" size="5"> 
        <font size="5" face="Arial, Helvetica, sans-serif">)</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone32" type="text" id="Phone32" value="<%=request("Phone32")%>" size="5"> 
        <img src="/images/transparent.gif" width="5" height="1"><font size="5" face="Arial, Helvetica, sans-serif">-</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone42" type="text" id="Phone42" value="<%=request("Phone41")%>" size="7"> 
        <img src="/images/transparent.gif" width="15" height="0"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Ext 
        : <img src="/images/transparent.gif" width="3" height="1"> 
        <input name="Ext2" type="text" id="Ext2" value="<%=request("Ext2")%>" size="5">
        </strong></font></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Email 
        Address : <font color="#FF0000">*</font></strong></font></td>
      <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="email" type="text" id="email" value="<%=request("email")%>" size="40"> 
        <% If bFormBad = TRUE  AND bBad07 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% end if%>
        <% if bFormBad = TRUE AND bBad08 = TRUE AND bBad07 = FALSE Then %>
        <font color="#CC0000">Invalid Email Address</font>
        <%end if%>
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Subject 
        : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <input name="Subject" type="text" id="Subject" value="<%=request("Subject")%>" size="40"> 
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td height="10" align="right" valign="top"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Message 
        : </strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td height="10" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
        <textarea name="Message" cols="40" rows="7" id="Message"><%=request("Message")%></textarea> 
      </td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="2"><img src="/images/transparent.gif" width="20" height="1"><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif"><strong>*</strong></font><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"> 
        How would you like us to contact you? <% If bFormBad = TRUE  AND bBad09 = TRUE Then %>
        <font color="#CC0000">Can't be blank</font> 
        <% end if%></font></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Email</strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td><input name="resEmail" type="checkbox" id="resEmail" value="true"<%if NOT Request.Form("resEmail") = "true" then%> <%else%>checked<%end if%>></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Telephone</strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td><input name="resTelephone" type="checkbox" id="resTelephone" value="true"<%if Request.Form("resTelephone") = "true" then%> checked<%end if%>></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>No 
        Response</strong></font><img src="/images/transparent.gif" width="5" height="8"></td>
      <td><input name="NoResponse" type="checkbox" id="NoResponse" value="true"<%if Request.Form("NoResponse") = "true" then%> checked<%end if%>></td>
    </tr>
    <tr> 
      <td height="30" colspan="2"><img src="/images/transparent.gif" width="1" height="30"></td>
    </tr>
    <tr align="center"> 
      <td colspan="2"><a href="/en/contact/sales.asp"><img src="/images/clear_form.gif" alt="Clear Form" width="107" height="18" border="0"></a><img src="/images/transparent.gif" width="100" height="1"><a href="#">
        <input type="image" border="0" name="iF" src="/images/send_email.gif" width="101" height="18" alt="Submit Email">
        <input type="hidden" name="MM_insert" value="true">
        <input type="hidden" name="ContactType" value="Sales">
        </a></td>
    </tr>
    <tr> 
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2">&nbsp;</td>
    </tr>
  </table>
</form>
</body>
</html>
