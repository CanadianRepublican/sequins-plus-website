<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<html>
<head>
<title>Sequins Plus Online - General Email Conformation</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="600" border="0" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
    <td colspan="2" align="center"><img src="/images/form_logo.gif" width="281" height="109"></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td><div align="center"><font color="#CC1C0F" size="3" face="Arial, Helvetica, sans-serif"><strong><em>Your 
        e-mail has been successfully sent.</em></strong></font></div></td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td colspan="2"><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
    <td><blockquote> 
        <p><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">This 
          is an automatic response confirmation page.<br>
          This page is a confirmation receipt that your email has been successfully 
          received by our server.<br>
		  <br>
		  We will reply to your inquiry or comment shortly and you may now click on the "continue shopping" link below to return to the Sequins Plus website.</font></p>
      </blockquote></td>
  </tr>
</table>
</td>
  </tr>
  <tr> 
    <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="30"></td>
  </tr>
  <tr> 
    <td colspan="2" align="center"><a href="javascript:window.close()"><img src="/images/continue-shopping-red.gif" width="129" height="18" border="0"></a></td>
  </tr>
  <tr> 
    <td height="10" colspan="2">&nbsp;</td>
  </tr>
</table>

</body>
</html>
