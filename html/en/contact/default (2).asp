<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" -->
<%

MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%>
<HTML>
<HEAD><%
MetaTitle = "Sequins Plus Online - Contact Us"
MetaDesc = "Sequins Plus Dance Supplies located at 377 Wilson Road South Oshawa, Ontario Canada. L1H 6C6 . Open Monday to Friday 10am - 6pm Call (905)433-2319 for more information!!!!"
MetaKey = "sequins, plus, sequinsplus, contact, us, dance, supplies, 377, wilson, road, south, oshawa, ontario, canada, L1H 6C6, general, information, questions, info@sequinsplus.com, order, sales, sales@sequinsplus.com, customer, service, service@sequinsplus.com, website, inqueries, directions, map, ballet, help"
%>
<TITLE><%=MetaTitle%></TITLE>
<!--#include virtual="sequinsplus/inc/meta.asp" -->
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
</style>
</HEAD>
<BODY bgcolor=#DFDFDF leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="MM_preloadImages('/images/top_home_over.gif','/images/top_help_over.gif','/images/about_but_over.jpg','/images/products_but_over.jpg','/images/services_but_over.jpg','/images/contact_but_over.jpg')">
<table width="780" height="741" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" colspan="3"></td>
    <td width="281" height="28" valign="top"><IMG src="/images/logo_top.gif" width=281 height=28 alt=""></td>
    <td height="28" colspan="4"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td height="9" colspan="2" valign="top"><IMG src="/images/logo_lft_top_line.gif" width=155 height=9 alt=""></td>
    <td width="281" height="9" valign="top"><IMG src="/images/logo_topline.gif" width=281 height=9 alt=""></td>
    <td height="9" colspan="3" valign="top"><IMG src="/images/logo_rt_top_line.gif" width=326 height=9 alt=""></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
    <td height="26" colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" valign="top"><IMG src="/images/logo_title.gif" width=281 height=26 alt=""></td>
    <td height="26" colspan="3" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td height="11" colspan="2" valign="top"><IMG src="/images/logo_lft_bot_line.gif" width=155 height=11 alt=""></td>
    <td width="281" height="11" valign="top"><IMG src="/images/logo_bot-line.gif" width=281 height=11 alt=""></td>
    <td height="11" colspan="3" valign="top"><IMG src="/images/logo_rt_bot_line.gif" width=326 height=11 alt=""></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/nav_top.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="281" height="14" valign="top"><IMG src="/images/logo_beige.gif" width=281 height=14 alt=""></td>
    <td height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td height="2" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" valign="top"><IMG src="/images/logo_spacer.gif" width=281 height=2 alt=""></td>
    <td height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="174"></td>
    <td width="153" height="174" bgcolor="#CC1C0F"><a href="/en/about/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('aboutus','','/images/about_but_over.jpg',1)"><img src="/images/about_but.jpg" alt="About Us" name="aboutus" width="153" height="28" vspace="5" border="0"></a><a href="/en/products/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('products','','/images/products_but_over.jpg',1)"><img src="/images/products_but.jpg" alt="Products" name="products" width="153" height="28" vspace="5" border="0"></a><a href="/en/services/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('services','','/images/services_but_over.jpg',1)"><img src="/images/services_but.jpg" alt="Services" name="services" width="153" height="28" vspace="5" border="0"></a><a href="/en/contact/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','/images/contact_but_over.jpg',1)"><img src="/images/contact_but.jpg" alt="Contact Us" name="contact" width="153" height="29" vspace="5" border="0"></a></td>
    <td width="2" height="174" bgcolor="#FFFFFF"></td>
    <td height="174" colspan="4" valign="top"><IMG src="/images/sequinsmain.jpg" width=607 height=174 alt=""></td>
    <td width="10" height="174"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td height="2" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top" background="/images/list_bottom.gif">&nbsp;</td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td height="14" colspan="2" bgcolor="#DFC6A3"></td>
    <td width="264" height="14" rowspan="3" align="center" valign="middle" bgcolor="#FFFFFF"> 
      <div align="center"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Oshawa, 
        Ontario. Canada</strong></font><IMG src="/images/map.jpg" width=264 height=253 alt="Sequins Plus Map 377 Wilson Road South Oshawa, Ontario Canada. L1H 6C6"> 
        <font size="2" face="Arial, Helvetica, sans-serif"><strong>Sequins Plus 
        Dance Supplies</strong></font><br>
        <font color="#616161" size="2" face="Arial, Helvetica, sans-serif">377 Wilson Road South</font></div><br>
      <img src="/images/transparent.gif" width="10" height="1"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>From 
      401:</strong></font><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><br>
      <img src="/images/transparent.gif" width="10" height="1">Exit Harmony Rd./Bloor St. E.<br>
      <img src="/images/transparent.gif" width="10" height="1">Turn West on Bloor St. E.<br>
      <img src="/images/transparent.gif" width="10" height="1">Turn North on Wilson Rd. S.<br>
      <img src="/images/transparent.gif" width="10" height="1">Continue North to 377 Wilson Rd. S.</font></td>
    <td width="30" height="14" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td height="6" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="32" rowspan="2" bgcolor="#EED39E"></td>
    <td width="30" rowspan="2" bgcolor="#EED39E"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td height="370" colspan="3" valign="top" bgcolor="#FFFFFF"> 
      <table width="436" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan="2" align="left"><img src="/images/electronic_mail_title.gif" width="293" height="20"></td>
        </tr>
        <tr> 
          <td height="6" colspan="2"><img src="/images/transparent.gif" width="1" height="6"></td>
        </tr>
        <tr> 
          <td width="10" align="left"><img src="/images/transparent.gif" width="10" height="8"></td>
          <td><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>GENERAL 
            INFORMATION AND QUESTIONS :</strong></font> <font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><a href="javascript:generalEmail()">info@sequinsplus.com</a></font><br> 
            <strong><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif">ORDER 
            INFORMATION AND SALES :</font></strong> <font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><a href="javascript:salesEmail()">sales@sequinsplus.com</a></font><br> 
            <strong><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif">CUSTOMER 
            SERVICE :</font></strong> <font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><a href="javascript:serviceEmail()">service@sequinsplus.com</a></font><br> 
            <strong><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif">WEBSITE 
            INQUIRIES :</font></strong> <font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><a href="javascript:webEmail()">webmaster@sequinsplus.com</a></font></td>
        </tr>
        <tr> 
          <td height="6" colspan="2"><img src="/images/transparent.gif" width="1" height="6"></td>
        </tr>
        <tr> 
          <td colspan="2" align="left"><img src="/images/phone_title.gif" width="293" height="20"></td>
        </tr>
        <tr> 
          <td height="6" colspan="2"></td>
        </tr>
        <tr> 
          <td width="10" height="33" align="left"><img src="/images/transparent.gif" width="10" height="8"></td>
          <td align="center" valign="top"><p><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
              Oshawa Store: </strong></font><span class="style1"><font size="2" face="Arial, Helvetica, sans-serif">1-905-433-2319</font></span><br>
          </p>
          </td>
        </tr>
        <tr> 
          <td height="6" colspan="2"><img src="/images/transparent.gif" width="1" height="6"></td>
        </tr>
        <tr> 
          <td colspan="2"><img src="/images/store_location_title.gif" width="293" height="20"></td>
        </tr>
        <tr> 
          <td height="6" colspan="2"><img src="/images/transparent.gif" width="1" height="6"></td>
        </tr>
        <tr> 
          <td width="10" align="left"><img src="/images/transparent.gif" width="10" height="8"></td>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><p align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Sequins 
                    Plus Oshawa</strong></font><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><br>
                    377 Wilson Road South<br>
                    Oshawa, Ontario<br>
                    Canada. L1H 6C6</font> </p>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="188" align="center" valign="top"><strong><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif">HOURS 
                    OF OPERATION<br>
(July 1 2012 &ndash; Aug 21 2012)<br>
                      </font></strong><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Tuesday - Friday<br>
                      10am - 6pm<br>
                      Saturday<br>
                      9am - 5pm<br>
                      Sunday - Monday<br>
                      Closed</font></td>
                      <td width="4">&nbsp;</td>
                      <td width="188" align="center" valign="top"><p><strong><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif">HOURS 
                        OF OPERATION<br>
                        (Aug 28 2012 - and on)<br>
                        </font></strong><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Monday - Wednesday<br>
                          10am - 6pm<br>
                          Thursday - Friday<BR>
                          10am - 8pm<BR>
                          Saturday<br>
                          9am - 5pm<br>
                          Sunday<br>
                      12pm - 5pm</font></p></td>
                    </tr>
                  </table>                  <p align="center">&nbsp;</p>
                </td>
              </tr>
          </table></td>
        </tr>
        <tr> 
          <td height="6" colspan="2"><img src="/images/transparent.gif" width="1" height="6"></td>
        </tr>
      </table></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8" height="1"></td>
    <td height="1" colspan="6" bgcolor="#EED39E"></td>
    <td width="10" height="1"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td height="49" colspan="6" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td height="7" colspan="6" valign="top"><IMG src="/images/page-bottom.gif" width=762 height=7 alt=""></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td height="10" colspan="8"></td>
  </tr>
</table>
</BODY>
</HTML>