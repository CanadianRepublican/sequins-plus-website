<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" -->
<%MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%>
<%
Dim RSfeatured
Set RSfeatured = Server.CreateObject("ADODB.Recordset")
RSfeatured.ActiveConnection = SPdbString
RSfeatured.Source = "SELECT spItem.id, spItem.Name, spItem.SmallPic, spItem.Desc, spItem.Price, spItem.Manu, spItem.Cat, spItem.ProductCode, spItemCatagory.Name as CatTXT, spItemManufacturer.Name as ManuTXT  FROM spItem, spItemCatagory, spItemManufacturer  WHERE spItem.Cat = spItemCatagory.id AND spItem.Manu =  spItemManufacturer.ID AND spItem.Feat = true"
RSfeatured.CursorType = 0
RSfeatured.CursorLocation = 2
RSfeatured.LockType = 1
RSfeatured.Open()
%><%
set RSmanu = Server.CreateObject("ADODB.Recordset")
RSmanu.ActiveConnection = SPdbString
RSmanu.Source = "SELECT spItemManufacturer.id, spItemManufacturer.Name FROM spItemManufacturer ORDER BY spItemManufacturer.Name ASC"
RSmanu.CursorType = 0
RSmanu.CursorLocation = 2
RSmanu.LockType = 3
RSmanu.Open()
RSmanu_numRows = 0
%><%
set RScategory = Server.CreateObject("ADODB.Recordset")
RScategory.ActiveConnection = SPdbString
RScategory.Source = "SELECT spItemCatagory.id, spItemCatagory.Name FROM spItemCatagory"
RScategory.CursorType = 0
RScategory.CursorLocation = 2
RScategory.LockType = 3
RScategory.Open()
RScategory_numRows = 0
%><%
set RSbanner = Server.CreateObject("ADODB.Recordset")
RSbanner.ActiveConnection = SPdbString
RSbanner.Source = "SELECT spBanners.id, spBanners.NAME, spBanners.Pic, spBanners.ItemID FROM spBanners"
RSbanner.CursorType = 0
RSbanner.CursorLocation = 2
RSbanner.LockType = 3
RSbanner.Open()
RSbanner_numRows = 0
%><% 
' Moving to random record - Steven Jones' Extension
If Not(RSfeatured.bof and RSfeatured.eof) Then
  ' reset the cursor to the beginning
  If (RSfeatured.CursorType > 0) Then
    RSfeatured.MoveFirst
  Else
    RSfeatured.Requery
  End If
  
  RSfeatured_totalrn = -1
  RSfeatured_totalrn = RSfeatured.RecordCount ' ony works on some recordsets, but much faster
  If (RSfeatured_totalrn = -1) Then ' and if it didn't work, we still have to count the records.

    ' count the total records by iterating through the recordset
    RSfeatured_totalrn=0
    While (Not RSfeatured.EOF)
      RSfeatured_totalrn = RSfeatured_totalrn + 1
      RSfeatured.MoveNext
    Wend

    ' reset the cursor to the beginning
    If (RSfeatured.CursorType > 0) Then
      RSfeatured.MoveFirst
    Else
      RSfeatured.Requery
    End If
	
  End If

' now do final adjustments, and move to the random record  
RSfeatured_totalrn = RSfeatured_totalrn - 1
If RSfeatured_totalrn > 0 Then
Randomize
RSfeatured.Move Int((RSfeatured_totalrn + 1) * Rnd)
End If 
End If
' all done; you should always check for an empty recordset before displaying data
%>
<HTML>
<%
MetaTitle = "Welcome To Sequins Plus Online"
MetaDesc = "Sequins Plus offers a wide range of dancewear, we can meet every dancewear need you may have! We also offer a range of ready-made costumes and can custom make your needs for performances and competitions."
MetaKey = "sequins, plus, sequinsplus, dance, supplies, sequins plus online, ontario,"
%>
<HEAD>
<TITLE><%=MetaTitle%></TITLE>
<!--#include virtual="sequinsplus/inc/meta.asp" -->
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY bgcolor=#DFDFDF leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad="MM_preloadImages('images/top_home_over.gif','images/top_cart_over.gif','images/top_help_over.gif','images/about_but_over.jpg','images/products_but_over.jpg','images/services_but_over.jpg','images/contact_but_over.jpg')">
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="3"></td>
    <td width="281" height="28" valign="top"><IMG src="/images/logo_top.gif" width=285 height=28 alt=""></td>
    <td width="336" height="28" colspan="4"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="2" valign="top"><IMG src="/images/logo_lft_top_line.gif" width=155 height=9 alt=""></td>
    <td width="285" height="9" valign="top"><IMG src="/images/logo_topline.gif" width=285 height=9 alt=""></td>
    <td width="326" height="9" colspan="3" valign="top"><IMG src="/images/logo_rt_top_line.gif" width=326 height=9 alt=""></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
   <td width="155" height="26" colspan="2" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" valign="top"><IMG src="/images/logo_title.gif" width=285 height=26 alt=""></td>
    <td width="326" height="26" colspan="3" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td width="155" height="11" colspan="2" valign="top"><IMG src="/images/logo_lft_bot_line.gif" width=155 height=11 alt=""></td>
    <td width="281" height="11" valign="top"><IMG src="/images/logo_bot-line.gif" width=285 height=11 alt=""></td>
    <td width="326" height="11" colspan="3" valign="top"><IMG src="/images/logo_rt_bot_line.gif" width=326 height=11 alt=""></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/nav_top.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="281" height="14" valign="top"><IMG src="/images/logo_beige.gif" width=285 height=14 alt=""></td>
    <td width="326" height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" valign="top"><IMG src="/images/logo_spacer.gif" width=285 height=2 alt=""></td>
    <td width="326" height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="112"></td>
    <td width="153" height="174" rowspan="4" bgcolor="#CC1C0F"><a href="/en/about/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('aboutus','','/images/about_but_over.jpg',1)"><img src="/images/about_but.jpg" alt="About Us" name="aboutus" width="153" height="28" vspace="5" border="0"></a><a href="/en/products/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('products','','/images/products_but_over.jpg',1)"><img src="/images/products_but.jpg" alt="Products" name="products" width="153" height="28" vspace="5" border="0"></a><a href="/en/services/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('services','','/images/services_but_over.jpg',1)"><img src="/images/services_but.jpg" alt="Services" name="services" width="153" height="28" vspace="5" border="0"></a><a href="/en/contact/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','/images/contact_but_over.jpg',1)"><img src="/images/contact_but.jpg" alt="Contact Us" name="contact" width="153" height="29" vspace="5" border="0"></a></td>
    <td width="2" height="174" rowspan="4" bgcolor="#FFFFFF"></td>
    <td width="281" height="174" rowspan="4" valign="top"><IMG src="/images/mainpage_pic2.jpg" width=285 height=174 alt=""></td>
    <td width="32" height="174" rowspan="4" bgcolor="#CC1C0F"></td>
    <td width="264" height="112" valign="top"><% 
' Moving to random record - Steven Jones' Extension
If Not(RSbanner.bof and RSbanner.eof) Then
  ' reset the cursor to the beginning
  If (RSbanner.CursorType > 0) Then
    RSbanner.MoveFirst
  Else
    RSbanner.Requery
  End If
  
  RSbanner_totalrn = -1
  RSbanner_totalrn = RSbanner.RecordCount ' ony works on some recordsets, but much faster
  If (RSbanner_totalrn = -1) Then ' and if it didn't work, we still have to count the records.

    ' count the total records by iterating through the recordset
    RSbanner_totalrn=0
    While (Not RSbanner.EOF)
      RSbanner_totalrn = RSbanner_totalrn + 1
      RSbanner.MoveNext
    Wend

    ' reset the cursor to the beginning
    If (RSbanner.CursorType > 0) Then
      RSbanner.MoveFirst
    Else
      RSbanner.Requery
    End If
	
  End If

' now do final adjustments, and move to the random record  
RSbanner_totalrn = RSbanner_totalrn - 1
If RSbanner_totalrn > 0 Then
Randomize
RSbanner.Move Int((RSbanner_totalrn + 1) * Rnd)
End If 
End If
' all done; you should always check for an empty recordset before displaying data
%>
      <a href="/en/products/details.asp?ItemID=<%=RSbanner.Fields.Item("ItemID").Value%>"><IMG src="<%=RSbanner.Fields.Item("Pic").Value%>" width=264 height=112 border="0" alt="<%=RSbanner.Fields.Item("NAME").Value%>"></a></td>
    <td width="30" height="174" rowspan="4" valign="top"><IMG src="/images/default_25.gif" width=30 height=174 alt=""></td>
    <td width="10" height="112"></td>
  </tr>
  <tr>
    <td width="8" height="21"></td>
    <td width="264" height="21" valign="top"><IMG src="/images/default_26.gif" width=264 height=21 alt=""></td>
    <td width="10" height="21"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="264" height="5" valign="top"><IMG src="/images/default_29.gif" width=264 height=5 alt=""></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="36"></td>
    <td width="264" height="280" rowspan="7" valign="top" bgcolor="#FFFFFF"> 
      <table width="264" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" mm_noconvert="TRUE">
        <tr> 
          <td colspan="4"><img src="/images/subtitle.gif" width="264" height="22"></td>
        </tr>
        <tr> 
          <td colspan="4"><img src="/images/transparent.gif" width="1" height="1"></td>
        </tr>
        <tr align="center"> 
          <td colspan="4" bgcolor="#EED39E"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong><%=(RSfeatured.Fields.Item("CatTXT").Value)%></strong></font></td>
        </tr>
        <tr> 
          <td rowspan="3"><img src="/images/transparent.gif" width="5" height="1"></td>
          <td colspan="2" align="left"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=(RSfeatured.Fields.Item("ManuTXT").Value)%></font></a></td>
          <td rowspan="3" align="center"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><img src="<%="/images/products/small/" & RSfeatured.Fields.Item("SmallPic").Value%>" width="60" height="70" border="0"></a></td>
        </tr>
        <tr> 
          <td colspan="2" align="left" valign="top"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=(RSfeatured.Fields.Item("Name").Value)%></font></a></td>
        </tr>
        <tr> 
          <td><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"># 
           <%pPcode = RSfeatured.Fields.Item("ProductCode").Value%> <%=RSfeatured.Fields.Item("ProductCode").Value%></font></a></td>
          <td width="100" align="center"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><%=RSfeatured.Fields.Item("Price").Value%></font></td>
        </tr>
        <tr align="center"> 
          <% 
' Moving to random record - Steven Jones' Extension
If Not(RSfeatured.bof and RSfeatured.eof) Then
  ' reset the cursor to the beginning
  If (RSfeatured.CursorType > 0) Then
    RSfeatured.MoveFirst
  Else
    RSfeatured.Requery
  End If
  
  RSfeatured_totalrn = -1
  RSfeatured_totalrn = RSfeatured.RecordCount ' ony works on some recordsets, but much faster
  If (RSfeatured_totalrn = -1) Then ' and if it didn't work, we still have to count the records.

    ' count the total records by iterating through the recordset
    RSfeatured_totalrn=0
    While (Not RSfeatured.EOF)
      RSfeatured_totalrn = RSfeatured_totalrn + 1
      RSfeatured.MoveNext
    Wend

    ' reset the cursor to the beginning
    If (RSfeatured.CursorType > 0) Then
      RSfeatured.MoveFirst
    Else
      RSfeatured.Requery
    End If
	
  End If

' now do final adjustments, and move to the random record  
RSfeatured_totalrn = RSfeatured_totalrn - 1
If RSfeatured_totalrn > 0 Then
Randomize
RSfeatured.Move Int((RSfeatured_totalrn + 1) * Rnd)
End If 
End If
' all done; you should always check for an empty recordset before displaying data
%><td colspan="4" bgcolor="#EED39E"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong><%=(RSfeatured.Fields.Item("CatTXT").Value)%></strong></font></td>
        </tr>
        <tr> 
          <td rowspan="3"><img src="/images/transparent.gif" width="5" height="1"></td>
          <td colspan="2" align="left"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=(RSfeatured.Fields.Item("ManuTXT").Value)%></font></a></td>
          <td rowspan="3" align="center"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><img src="<%="/images/products/small/" & RSfeatured.Fields.Item("SmallPic").Value%>" width="60" height="70" border="0"></a></td>
        </tr>
        <tr> 
          <td colspan="2" align="left" valign="top"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=(RSfeatured.Fields.Item("Name").Value)%></font></a></td>
        </tr>
        <tr> 
          <td><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"># 
            <%=RSfeatured.Fields.Item("ProductCode").Value%></font></a></td>
          <td width="100" align="center"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><%=RSfeatured.Fields.Item("Price").Value %></font></td>
        </tr>
        <tr align="center"> 
          <% 
' Moving to random record - Steven Jones' Extension
If Not(RSfeatured.bof and RSfeatured.eof) Then
  ' reset the cursor to the beginning
  If (RSfeatured.CursorType > 0) Then
    RSfeatured.MoveFirst
  Else
    RSfeatured.Requery
  End If
  
  RSfeatured_totalrn = -1
  RSfeatured_totalrn = RSfeatured.RecordCount ' ony works on some recordsets, but much faster
  If (RSfeatured_totalrn = -1) Then ' and if it didn't work, we still have to count the records.

    ' count the total records by iterating through the recordset
    RSfeatured_totalrn=0
    While (Not RSfeatured.EOF)
      RSfeatured_totalrn = RSfeatured_totalrn + 1
      RSfeatured.MoveNext
    Wend

    ' reset the cursor to the beginning
    If (RSfeatured.CursorType > 0) Then
      RSfeatured.MoveFirst
    Else
      RSfeatured.Requery
    End If
	
  End If

' now do final adjustments, and move to the random record  
RSfeatured_totalrn = RSfeatured_totalrn - 1
If RSfeatured_totalrn > 0 Then
Randomize
RSfeatured.Move Int((RSfeatured_totalrn + 1) * Rnd)
End If 
End If
' all done; you should always check for an empty recordset before displaying data
%><td colspan="4" bgcolor="#EED39E"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong><%=(RSfeatured.Fields.Item("CatTXT").Value)%></strong></font></td>
        </tr>
        <tr> 
          <td rowspan="3"><img src="/images/transparent.gif" width="5" height="1"></td>
          <td colspan="2" align="left"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=(RSfeatured.Fields.Item("ManuTXT").Value)%></font></a></td>
          <td rowspan="3" align="center"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><img src="<%="/images/products/small/" & RSfeatured.Fields.Item("SmallPic").Value%>" width="60" height="70" border="0"></a></td>
        </tr>
        <tr> 
          <td colspan="2" align="left" valign="top"><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=(RSfeatured.Fields.Item("Name").Value)%></font></a></td>
        </tr>
        <tr> 
          <td><a href="/en/products/details.asp?ItemID=<%=RSfeatured.Fields.Item("id").Value%>"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"># 
            <%=RSfeatured.Fields.Item("ProductCode").Value%></font></a></td>
          <td width="100" align="center"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><%=RSfeatured.Fields.Item("Price").Value%></font></td>
        </tr>
      </table>
</td>
    <td width="10" height="36"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="468" height="2" colspan="4" bgcolor="#FFFFFF"></td>
    <td width="30" height="2" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/list_bottom.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="313" height="14" colspan="2" bgcolor="#DFC6A3"></td>
    <td width="30" height="14" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="468" height="2" colspan="4" bgcolor="#FFFFFF"></td>
    <td width="30" height="2" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="153" height="14" valign="top"><IMG src="/images/nav_top.gif" width=153 height=14 alt=""></td>
    <td width="2" height="14" bgcolor="#FFFFFF"></td>
    <td width="281" height="237" rowspan="5" valign="top"> 
      <table width="281" border="0" cellpadding="0" cellspacing="0" bgcolor="#EBEBEB" mm_noconvert="TRUE">
        <tr> 
          <td colspan="4" bgcolor="#EED39E"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td width="5"><img src="/images/transparent.gif" width="5" height="8"></td>
          <td colspan="2"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong>Welcome.</strong></font><br>
            <font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
            Please <a href="/en/order/new.asp"><font color="#CC1C0F"><u>Create 
            An Account</u></font></a> or <a href="/en/order/login.asp"><font color="#CC1C0F"><u>Sign 
            In</u></font></a> to Sequins Plus Online.</font> 
            <p align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Sequins 
          Plus offers a wide range of dancewear, we can meet every dancewear need 
          you may have! We also offer a range of ready-made costumes and can custom 
          make your needs for performances and competitions.</font></p></td>
      <td align="right"><img src="/images/transparent.gif" width="5" height="1"></td>
    </tr>
    <tr> 
          <td colspan="4" height="10"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
        <tr> 
          <td colspan="4" bgcolor="#EED39E"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">SEARCH</font></td>
    </tr>
    <tr> 
          <td colspan="4" height="10"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
    <tr>
      <td width="5"><img src="/images/transparent.gif" width="5" height="1"></td>
          <td align="left" valign="top">
<select name="QCat" id="QCat">
                <option value="">Entire Site</option>
              <%
While (NOT RScategory.EOF)
%>
              <option value="<%=(RScategory.Fields.Item("id").Value)%>" <%if (CStr(RScategory.Fields.Item("id").Value) = CStr(Request.QueryString("Cat"))) then Response.Write("SELECTED") : Response.Write("")%> ><%=(RScategory.Fields.Item("Name").Value)%></option>
              <%
RScategory.MoveNext()
Wend
RScategory.close()%>
            </select>
          </td>
          <td align="left" valign="top" nowrap> 
            <input name="QKey" type="text" onSubmit="QuickSearch()" id="QKey" value="Keyword Search" size="14">
              <input name="Qbutton" type="image" onClick="QuickSearch()" id="Qbutton" src="/images/grey_go.gif" alt="Search" align="absmiddle" width="19" height="19" hspace="0" vspace="0" border="0">
          </td>
      <td width="5" align="left" valign="top"><img src="/images/transparent.gif" width="5" height="1"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
          <td colspan="3"><img src="/images/grey-arrow.gif" width="6" height="10" hspace="5" align="absmiddle"><a href="/en/products/advanced.asp"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><u>Advanced 
            Search</u></font></a></td>
    </tr>
    <tr> 
          <td height="5" colspan="4"><img src="/images/transparent.gif" width="1" height="8"></td>
    </tr>
  </table>
</td>
    <td width="32" height="231" rowspan="4" bgcolor="#CC1C0F"></td>
    <td width="30" height="231" rowspan="4" valign="top" bgcolor="#CC1C0F"><IMG src="/images/default_43.gif" width=30 height=231 alt=""></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="209"></td>
    <td width="153" height="209" valign="top">
  <table width="153" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left" valign="top"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" align="left" valign="top"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
	<tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="4" colspan="2" valign="top"><img src="/images/maintable-bottom.gif" width="153" height="4" align="top"></td>
    </tr>
	<tr>
	  <td height="3" colspan="2" valign="top"><img src="/images/maintable-top.gif" width="153" height="3" align="top"></td>
    </tr>
    <tr> 
      <td width="15" height="14" align="left"><img src="/images/maintable-arrow.gif" width="15" height="14" align="top"></td>
      <td width="138" height="14" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><%If not RSmanu.EOF then %> <a href="/en/products/browse.asp?Manu=<%=RSmanu.Fields.Item("id").Value%>"><%=(RSmanu.Fields.Item("name").Value)%></a><% RSmanu.Movenext() 
		  end if%></font></td>
    </tr>
    <tr> 
      <td height="3" colspan="2" valign="top"><img src="/images/maintable-table_bottom.gif" width="153" height="3" align="top"></td>
    </tr>
  </table>
</td>
    <td width="2" height="209" bgcolor="#FFFFFF"></td>
    <td width="10" height="209"></td>
  </tr>
  <tr>
    <td width="8" height="3"></td>
    <td width="153" height="14" rowspan="3" valign="top"><IMG src="/images/list_bottom.gif" width=153 height=14 alt=""></td>
    <td width="2" height="3" bgcolor="#FFFFFF"></td>
    <td width="10" height="3"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="2" height="5" bgcolor="#FFFFFF"></td>
    <td width="264" height="5" valign="top"><IMG src="/images/default_46.gif" width=264 height=5 alt=""></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="2" height="6" bgcolor="#FFFFFF"></td>
    <td width="326" height="6" colspan="3" bgcolor="#CC1C0F"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="1"></td>
    <td width="762" height="1" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="1"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
     <td height="49" colspan="6" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="6" valign="top"><IMG src="/images/page-bottom.gif" width=762 height=7 alt=""></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="8"></td>
  </tr>
  <tr>
    <td width="8" height="1"></td>
    <td width="153" height="1"></td>
    <td width="2" height="1"></td>
    <td width="281" height="1"></td>
    <td width="32" height="1"></td>
    <td width="264" height="1"></td>
    <td width="30" height="1"></td>
    <td width="10" height="1"></td>
  </tr>
</table><script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9811433-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</BODY>
</HTML><%
RSfeatured.close()
RSmanu.close()
RSbanner.close()
%>