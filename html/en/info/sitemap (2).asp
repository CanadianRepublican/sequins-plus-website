<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<%
Set spManu = Server.CreateObject("ADODB.Recordset")
spManu.ActiveConnection = SPdbString
spManu.Source = "SELECT spItemManufacturer.id, spItemManufacturer.Name FROM spItemManufacturer ORDER BY spItemManufacturer.Name ASC"
spManu.CursorType = 0
spManu.CursorLocation = 2
spManu.LockType = 3
spManu.Open()
%>
<%
Set spCatagories = Server.CreateObject("ADODB.Recordset")
spCatagories.ActiveConnection = SPdbString
spCatagories.Source = "SELECT spItemCatagory.id, spItemCatagory.QuickLinkPic, spItemCatagory.Name,spItemCatagory.QuickLink FROM spItemCatagory WHERE spItemCatagory.QuickLink = true ORDER BY spItemCatagory.Name ASC"
spCatagories.CursorType = 0
spCatagories.CursorLocation = 2
spCatagories.LockType = 3
spCatagories.Open()
%>
<html>
<head>
<link href="/main.css" rel="stylesheet" type="text/css"> 
<title>Sequins Plus Online - Site Map</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="500" height="203" border="0" align="left" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
    <td width="500" height="109" align="center"><img src="/images/form_logo.gif" width="281" height="109"></td>
  </tr>
  <tr> 
    <td width="500" height="10"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td width="500" align="center"> 
      <font color="#000000" size="4" face="Arial, Helvetica, sans-serif"><strong>Sequins 
        Plus Online Site Map</strong></font></td>
  </tr>
  <tr> 
    <td width="500" align="center"><br>
<table width="420" border="0" align="center" cellspacing="2">
        <tr> 
            
          <td width="178" align="left" valign="top"><a href="/en/default.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong>Main 
            Page</strong></span></font></a><br>
            <a href="/en/about/default.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong>About 
            Us Page</strong></span></font></a><br>
            <a href="/en/contact/default.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif">
              <span class="Head2ub"><strong>Contact Us Page </strong></span></font></a><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
              <br>
              &nbsp; &#8226; General Email <br>
              &nbsp; &#8226; Sales Email<br>
              &nbsp; &#8226; Service Email<br>
              &nbsp; &#8226; Webmaster Email</font></span><br><a href="/en/services/default.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Services Page</strong></span></font></a><br>
            <a href="/en/order/cart.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
            Shopping Cart</strong></span></font></a>
            <br>
               <span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&nbsp; &#8226;</font></span> <a href="/en/order/login.asp" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">User Login Page</font></span></a><br>
              <span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&nbsp; &#8226;</font></span> <a href="/en/order/new.asp" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">New User Page</font></span></a><br>
              <span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&nbsp; &#8226;</font></span> <a href="/en/order/wishlist.asp" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Wishlist Page</font></span></a><br>
              <span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&nbsp; &#8226;</font></span> <a href="/en/order/default.asp" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">My Account Page</font></span></a><br>
              <span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&nbsp; &#8226;</font></span> <a href="/en/order/forgot.asp" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Password Recoverey Page</font></span></a><br><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Shoe Sizeing Charts</strong></span></font><br> <font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Child Sizeing Chart</strong></span></font><br> <font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Adult Sizeing Chart</strong></span></font><br> <font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Disclaimer</strong></span></font><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
              <br>
              &nbsp; &#8226; Terms And Conditions<br>
              &nbsp; &#8226; Copyright Notice<br>
              &nbsp; &#8226; Submissions<br>
              &nbsp; &#8226; Colours<br>
              &nbsp; &#8226; Privacy Policy<br>
              &nbsp; &#8226; Security<br>
              &nbsp; &#8226; More Information</font></span><br> <font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Help</strong></span></font><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
              <br>
              &nbsp; &#8226; Shopping Cart Help<br>
              &nbsp; &#8226; Products Section Help<br>
              &nbsp; &#8226; Sizeing Help<br>
              &nbsp; &#8226; Wishlist Help<br>
              &nbsp; &#8226; Services Help<br>
              &nbsp; &#8226; Techincal Assistance</font></span><br> </td>
                      <td width="232" align="left" valign="top"><a href="/en/products/advanced.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong>Advanced Search</strong></span></font></a><br><a href="/en/products/default.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Products Page</strong></span></font></a>
              <%while NOT (spCatagories.EOF OR spCatagories.BOF)%><br>
			  &nbsp; &#8226; <a href="/en/products/default.asp?Cat=<%=spCatagories.Fields.Item("id").Value%>" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=(spCatagories.Fields.Item("Name").Value)%> Page</font></span></a><%spCatagories.MoveNext()
			  wEnd
			  spCatagories.requery()%>
              <br><a href="/en/products/browse.asp" TARGET="SPmainBrowser"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><span class="Head2ub"><strong> 
              Products Browse Page</strong></span></font></a><br><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
              &nbsp; <em>Browse Catagories</em></font></span><%while NOT (spCatagories.EOF OR spCatagories.BOF)%><br>
              &nbsp; &nbsp; &#8226; <a href="/en/products/browse.asp?Cat=<%=spCatagories.Fields.Item("id").Value%>" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=(spCatagories.Fields.Item("Name").Value)%> Browse Page</font></span></a><%spCatagories.MoveNext()
			  wEnd
			  spCatagories.Close()%><br><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
              &nbsp; <em>Browse Manufacturers</em></font></span><%while NOT (spManu.EOF OR spManu.BOF)%><%Set spCheck = Server.CreateObject("ADODB.Recordset")
spCheck.ActiveConnection = SPdbString
spCheck.Source = "SELECT * FROM spItem Where spItem.Manu = " & spManu.Fields.Item("id").Value
spCheck.CursorType = 0
spCheck.CursorLocation = 2
spCheck.LockType = 3
spCheck.Open()
if (NOT spCheck.EOF) OR (NOT spCheck.BOF) then%><br>
			  &nbsp; &nbsp; &#8226; <a href="/en/products/browse.asp?Manu=<%=spManu.Fields.Item("id").Value%>" TARGET="SPmainBrowser"><span class="popout"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=(spManu.Fields.Item("Name").Value)%> Browse Page</font></span></a><%
end if
			  spManu.MoveNext()
			  spCheck.close()
			   wEnd
			  spManu.close()%></td>
          </tr>
        </table>
        <br>
      <a href="javascript:window.close()"><img src="/images/close.gif" width="124" height="18" border="0"></a><br>
		<br>

      </blockquote></td>
  </tr>
  <tr> 
    <td width="500">
<blockquote> 
        <p><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Have 
          a question or comment? We value the opinion of our customers, please 
          feel free to tell us what you think. We are happy to hear from you and 
          assist you any way we can.<br>
          The information you provide will be handled according to our <font color="#0033FF"><u>privacy 
          policy</u></font>.</font></p>
      </blockquote></td>
  </tr>
  <tr> 
    <td width="500" height="18">&nbsp;</td>
  </tr>
</table>
</body>
</html>