<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<html>
<head>
<title>Sequins Plus Online - Website Help</title>
<link href="/main.css" rel="stylesheet" type="text/css">
<META NAME="DC.Title" CONTENT="Sequins Plus Online - Website Help">
<META NAME="keywords" CONTENT="Sequins, Plus, SequinsPlus, Online, Help, website, policy, item, return, shipping">
<META NAME="description" CONTENT="Help with Sequins Plus Online website, such as ordering, shipping, returning and item, provacy policy, and needed help for website use.">
<META NAME="abstract" CONTENT="Help with Sequins Plus Online website, such as ordering, shipping, returning and item, provacy policy, and needed help for website use.">
<META HTTP-EQUIV="Content-Language" CONTENT="EN">
<META NAME="author" CONTENT="Digital Evolution (Rahim Khoja and Brent Nickelchok)">
<META HTTP-EQUIV="Expires" CONTENT="30 days">
<META NAME="revisit-after" CONTENT="1 days">
<META NAME="rating" CONTENT="General">
<META NAME="copyright" CONTENT="Sequins Plus">
<META NAME="robots" CONTENT="FOLLOW,INDEX">
<META NAME="Pragma" CONTENT="no-cache">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK REV="made" href="mailto:webmaster@sequinsplus.com">
<BODY bgcolor=#DFDFDF leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
</HEAD>
<table width="700" border="0" align="left" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
    <td width="700" height="109" align="center"><img src="/images/form_logo.gif" width="281" height="109"></td>
  </tr>
  <tr> 
    <td width="700" height="10"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td width="700" align="center"> <font color="#000000" size="4" face="Arial, Helvetica, sans-serif"><strong>Sequins 
      Plus Online - Website Help</strong></font></td>
  </tr>
  <tr> 
    <td width="700" align="center"><br>
      <table width="500" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="232" valign="top" nowrap><div align="right"> <em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Dance 
              Teacher Discount Program : </font></strong></em></div></td>
          <td width="268" rowspan="2" align="center" valign="middle"> <div align="center"><font size="2" face="Arial, Helvetica, sans-serif">Sequins 
              Plus offers a discount program to Dance Teachers and Dance Instututions 
              if you wish to find out if you qualify for a discount or what your 
              discount will be.</font></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"> <em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Shipping 
              Rates and Policies : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"> <em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Returning 
              an Item : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"> <em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Sequins 
              Plus Privacy Policy : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Shopping 
              Cart : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">My 
              Account : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Ordering : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Wish 
              List : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Tell 
              a Friend : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Sizeing 
              Charts : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
		<tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Product 
              Details </font><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">: 
              </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
		<tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Contact 
              Us : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
		<tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Current/Recent 
              Orders </font><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">: 
              </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
		<tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Forgot 
              Your Password : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
		<tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">Account 
              Settings : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
		<tr> 
          <td valign="top" nowrap><div align="right"><em><strong><font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">New 
              Sequins Plus Cart User : </font></strong></em></div></td>
          <td rowspan="2" align="center" valign="middle"><div align="center"></div>
            <div align="center"></div></td>
        </tr>
        <tr> 
          <td valign="top" nowrap><div align="right"></div></td>
        </tr>
      </table>
      <p><br>
        <br>
        <a href="javascript:window.close()"><img src="/images/close.gif" width="124" height="18" border="0"></a><br>
        <br>
        <br>
      </p>
      </td>
  </tr>
  <tr> 
    <td width="700"> <blockquote> 
        <p><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Have 
          a question or comment? We value the opinion of our customers, please 
          feel free to tell us what you think. We are happy to hear from you and 
          assist you any way we can.<br>
          The information you provide will be handled according to our <font color="#0033FF"><u>privacy 
          policy</u></font>.</font></p>
      </blockquote></td>
  </tr>
  <tr> 
    <td width="700" height="18">&nbsp;</td>
  </tr>
</table><script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9811433-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
