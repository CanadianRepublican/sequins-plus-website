<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<html>
<head>
<title>Sequins Plus Online - Disclaimer</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/main.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<link href="/main.css" rel="stylesheet" type="text/css">
<table width="700" border="0" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center"><img src="/images/form_logo.gif" width="281" height="109"></td>
    <td align="center"><a href="javascript:window.close()"><img src="/images/form-close.gif" width="140" height="109" border="0" align="absmiddle"></a></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td height="10" colspan="5"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td colspan="5"></td>
  </tr>
  <tr> 
    <td width="10" align="left"><img src="/images/transparent.gif" width="10" height="1"></td>
    <td width="153" height="14" align="left"><img src="/images/nav_top.gif" width="153" height="14"></td>
    <td bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="374" height="14"></td>
    <td width="153" height="14" align="right"><img src="/images/nav_top-rt.gif" width="153" height="14"></td>
    <td width="10" align="right"><img src="/images/transparent.gif" width="10" height="1"></td>
  </tr>
  <tr> 
    <td><img src="/images/transparent.gif" width="1" height="10"></td>
    <td background="/images/list_border.gif"><img src="/images/transparent.gif" width="1" height="10"></td>
    <td colspan="2" background="/images/disclaimer_border.gif"><img src="/images/transparent.gif" width="1" height="10"></td>
    <td><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td valign="top">&nbsp;</td>
    <td valign="top" background="/images/list_border.gif"><br>
      <img src="/images/transparent.gif" width="5" height="1">&#8226; 
      <a href="#terms"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><u>Terms And Conditions<br>
      <img src="/images/transparent.gif" width="5" height="1" hspace="5" border="0">Of 
      Website Use</u></font></a><br>
      <br>
      <img src="/images/transparent.gif" width="5" height="1">&#8226; <a href="#copy"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><u>Copyright 
      Notice</u></font></a><br>
      <br>
      <img src="/images/transparent.gif" width="5" height="1">&#8226; <a href="#submission"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><u>Submissions</u></font></a><br>
      <br>
      <img src="/images/transparent.gif" width="5" height="1">&#8226; <a href="#colours"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><u>Colours</u></font></a><br>
      <br>
      <img src="/images/transparent.gif" width="5" height="1">&#8226; <a href="#privacy"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><u>Privacy 
      Policy</u></font></a><br>
      <br>
      <img src="/images/transparent.gif" width="5" height="1">&#8226; <a href="#security"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><u>Security</u></font></a><br>
      <br>
      <img src="/images/transparent.gif" width="5" height="1">&#8226; <a href="#more"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><u>More 
      Information</u></font></a><br>
      <br>
      </font></td>
    <td colspan="2" valign="top" background="/images/disclaimer_border.gif"> <blockquote> 
        <p id="terms"><font color="#CC1C0F" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><u><em>TERMS 
          AND CONDITIONS OF WEBSITE USE</em></u></strong></font></p>
        <p ><font size="2" face="Arial, Helvetica, sans-serif">Please take a few 
          minutes to review these Terms and Conditions. Your use of this Web site 
          constitutes your agreement to follow these rules and to be bound by 
          them. If you do not agree with any of these Terms and Conditions, do 
          not use the Sequinsplus.com Web site.<br>
          <br>
          Sequins Plus reserves the right to update or modify these Terms and 
          Conditions at any time without prior notice. Your use of this Web site 
          following any such change constitutes your agreement to follow and be 
          bound by the Terms and Conditions as changed. For this reason, we encourage 
          you to review these Terms and Conditions whenever you use the Web site.<br>
          <br>
          All product description content is provided for general product information 
          and product education purposes. The information on this site may contain 
          typographical errors or inaccuracies and may not be complete or current. 
          We therefore reserve the right to correct any errors, inaccuracies or 
          omissions and change or update information at any time without prior 
          notice (including after you have submitted your order). Please note 
          that such errors, inaccuracies or omissions may relate to product description, 
          pricing and availability. We also reserve the right to limit quantities 
          (including after you have submitted your order). We apologize for any 
          inconvenience this may cause you. If you are not fully satisfied with 
          your purchase you may return it with the original packing receipt (or 
          visit <a href="/en/order/default.asp"><font color="#0000FF"><u>My Account</u></font></a> 
          and click <font color="#0000FF"><u>Return an Item</u></font> link for 
          return options) within 90 days of the order date. Additional restrictions 
          apply. Please see our <font color="#0000FF"><u>Return Policy</u></font> 
          for more details.<br>
          <br>
          If you have any questions about our Terms and Conditions, please contact 
          Sequins Plus directly at +1-905-433-2319 or e-mail us at <a href="mailto:info@sequinsplus.com"><font color="#0000FF"><u>info@sequinsplus.com</u></font></a>.</font></p>
        <p id="copy"><font color="#CC1C0F" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><u><em>COPYRIGHT 
          NOTICE</em></u></strong></font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif">All of the Content 
          you see on the Sequinsplus.com Web site, including, for example, all 
          of the page headers, images, illustrations, graphics and text are subject 
          to trademark, copyright and/or other intellectual property rights or 
          licenses held by Sequins Plus, one of its affiliates or by third parties 
          who have consented with the use of their materials by SequinsPlus.com. 
          The Sequins Plus Logo Design is a registered trademark of Sequins Plus. 
          The entire content of this site is copyrighted as a collective work 
          under Canadian copyright laws, and Sequins Plus retains a copyright 
          in the selection, coordination, arrangement and enhancement of the content.<br>
          <br>
          The Content of this Web site, and the site as a whole, is intended for 
          solely for personal, noncommercial use by the users of our site. You 
          may download, print and store selected portions of the Content, provided 
          you (1) only use these copies of the Content for your own personal, 
          non-commercial use, (2) do not copy or post the Content on any network 
          computer or broadcast the Content in any media, and (3) do not modify 
          or alter the Content in any way, or delete or change any copyright or 
          trademark notice.<br>
          <br>
          Except as noted above, you may not copy, download, reproduce, modify, 
          publish, distribute, transmit, transfer or create derivative works from 
          the Content without first obtaining written permission from Sequins 
          Plus.</font></p>
        <p id="submission"><font color="#CC1C0F" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><u><em>SUBMISSIONS</em></u></strong></font><font size="2" face="Arial, Helvetica, sans-serif"><br>
          <br>
          We welcome your comments regarding this Web site. However, any comments, 
          feedback, notes, messages, ideas, suggestions or other communications 
          (collectively, &#8220;Comments&#8221;) sent to SequinsPlus.com shall 
          be and remain the exclusive property of Sequins Plus. Your submission 
          of any such Comments shall constitute an assignment to Sequins Plus 
          of all worldwide rights, titles and interests in all copyrights and 
          other intellectual property rights in the Comments.</font></p>
        <p id="colours"><font color="#CC1C0F" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><u><em>COLOURS</em></u></strong></font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif">We have done our 
          best to display as accurately as possible the colours of the products 
          shown on this Web site. However, because the colours you see depend 
          on your monitor, we cannot guarantee that your monitor&#8217;s display 
          of any colour will be accurate.</font></p>
        <p id="privacy"><font color="#CC1C0F" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><u><em>PRIVACY 
          POLICY</em></u></strong> </font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif">On our website we 
          only collect and store any information you enter on our Web site or 
          give to our Web site in any other way. For example, we collect information 
          you provide when you place an order, create an account, send us an e-mail 
          or call us, this information may include:<br>
          <br>
          &#8226; Your name<br>
          &#8226; Your mailing address<br>
          &#8226; Your e-mail address<br>
          &#8226; Your phone number</font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif">It also may include 
          any other personal or preference information you provide us. It also 
          may include information you submit about other people, for example, 
          the name and e-mail address of a Tell-A-Friend recipient.<br>
          <br>
          We only collect and store the personal information you provide, and 
          only use that information for the purpose for which it was provided, 
          for example, to respond to your e-mail message and to place and ship 
          your order.<br>
          <br>
          We receive and store certain types of information whenever you interact 
          with our Web site. For example, we use &#8220;cookies&#8221; like many 
          other Web sites. Through &#8220;cookies&#8221; we obtain certain types 
          of information whenever your browser accesses our Web site. There are 
          utilities designed to help you visit Web sites anonymously. Although 
          we cannot provide you with a personalized experience or certain services 
          at our Web site if we cannot recognize you, we want you to be aware 
          that these tools exist. Cookies are only used to help you navigate our 
          website and are not used to monitor you.<br>
          <br>
          Cookies are alphanumeric identifiers that we transfer to your computer&#8217;s 
          hard drive through your Web browser. Cookies allow us to provide such 
          features as Recently Viewed Items, Recent Searches and Cart Items between 
          visits.<br>
          <br>
          The &#8220;help&#8221; portion of the toolbar on most browsers will 
          tell you how to prevent your browser from accepting new cookies, how 
          to have the browser notify you when you receive a new cookie or how 
          to disable cookies altogether. However, cookies allow you to take full 
          advantage of some of our Web site&#8217;s best features, so we recommend 
          that you leave them turned on.<br>
          <br>
          If you provide our Web site with your e-mail addresses, you may receive 
          e-mails from us. If you do not want to receive marketing or promotional 
          e-mails from us, please <font color="#0000FF"><u>click here</u></font>. 
          You may still receive select e-mails from us to confirm an order or 
          e-mail submissions. You may receive e-mails from other users of SequinsPlus.com 
          as a result of our Tell-A-Friend service.<br>
          <br>
          We recognize the particular importance of protecting privacy where children 
          are involved. We are committed to protecting children&#8217;s privacy 
          on the Internet and we comply fully with the Children&#8217;s Online 
          Privacy Protection Act (COPPA). We don not knowingly collect personally 
          identifiable information from children under the age of 13. If a child 
          has provided our Web site with personally identifiable information, 
          we ask that a parent or guardian contact Sequins Plus directly at +1-905-433-2319 
          or contact us at <a href="mailto:info@sequinsplus.com"><font color="#0000FF"><u>info@sequinsplus.com</u></font></a> 
          and we will delete the information about the child from our files.<br>
          <br>
          At Sequins Plus.com, we are committed to protecting your privacy. This 
          means that we will not distribute, sell or rent your name or personal 
          information about you to any third party or organization.<br>
          <br>
          By using our Web site, you consent to our use of information that is 
          collected or submitted as described in this online privacy policy. We 
          may change or add to this privacy policy so we encourage you to review 
          it periodically. To help you track changes we will include a history 
          of material changes at the end of this Disclaimer document. This history 
          will begin as of the end of November, 2004 and will include the date 
          the privacy policy was last updated and a description of any material 
          changes throughout this entire document.</font></p>
        <p id="security"><font color="#CC1C0F" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><u><em>SECURITY</em></u></strong></font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif">We have appropriate 
          physical, electronic and procedural security safeguards to protect and 
          secure the information we collect.<br>
          <br>
          Our Web site uses Secure Sockets Layering (SSL) to encrypt your personal 
          information, before it travels over the Internet to avoid the decoding 
          of your information by anyone other than Sequins Plus. SSL technology 
          is the industry standard for secure online transactions. We understand 
          the importance of safe secure transactions and although we use SSL, 
          we never actually require our customers to enter their personal credit 
          card information to place an order on our Web site. Our online shopping 
          experience is completely safe because we only collect credit card information 
          after the order is placed through over the phone order verification.</font></p>
        <p id="more"><font color="#CC1C0F" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><u><em>MORE 
          INFORMATION</em></u></strong></font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif">Please call +1-905-433-2319 
          or contact us at <a href="mailto:info@sequinsplus.com"><font color="#0000FF"><u>info@sequinsplus.com</u></font></a> 
          if you have further questions about anything contained in this Online 
          Disclaimer document.</font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif"><strong>LAST UPDATED 
          November 21, 2004</strong></font></p>
        <p><font size="2" face="Arial, Helvetica, sans-serif">November, 2004 &#8211; 
          Document amendment and upload. </font><br>
          <br>
        </p>
      </blockquote></td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr> 
    <td width="10" align="left"><img src="/images/transparent.gif" width="10" height="1"></td>
    <td width="153" height="14" align="left"><img src="/images/nav_bottom.gif" width="153" height="14"></td>
    <td bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="374" height="14"></td>
    <td width="153" height="14" align="right"><img src="/images/nav_bot-rt.gif" width="153" height="14"></td>
    <td width="10" align="right"><img src="/images/transparent.gif" width="10" height="1"></td>
  </tr>
</table>
</body>
</html>
