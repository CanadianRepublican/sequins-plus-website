<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<SCRIPT LANGUAGE=JavaScript RUNAT=Server NAME="UC_CART">
//
// UltraDev UCart include file Version 1.0
//
function UC_ShoppingCart(Name, cookieLifetime, colNames, colComputed)  // Cart constructor
{
	// Name is the name of this cart. This is not really used in this implementation.
	// cookieLifeTime is in days. A value of 0 means do not use cookies.
	// colNames is a list of column names (must contain: ProductID, Quantity, Price, Total)
	// colComputed is a list of computed columns (zero length string means don't compute col.)

  // Public methods or UC_Cart API
  this.AddItem           = UCaddItem;        // Add an item to the cart
  this.GetColumnValue    = GetColumnValue;   // Get a value from the cart
  this.Destroy           = UCDestroy;        // remove all items, delete session, delete client cookie (if any)
	this.SaveToDatabase    = SaveToDatabase;   // persist cart to database.
	this.GetItemCount      = GetItemCount;     // the number of items in the cart.
	this.Update            = Update;           // Update the cart quantities.
	this.GetColumnTotal    = GetColumnTotal;   // Get the sum of a cart column for all items (e.g. price or shipping wt.).
  this.GetContentsSerial = UCGetContentsSerial// Get the contents of the cart as a single delimited string
  this.SetContentsSerial = UCSetContentsSerial// Set the contents of the cart from a serial string (obtained from GetContentsSerial)
  this.GetColNamesSerial = UCGetColNamesSerial// Get the list of column names as a delimited string.
  
	// PROPERTIES
	this.SC				= null;			// Cart data array
	this.numCols		= colNames.length;
	this.colComputed	= colComputed;
	this.colNames		= colNames;
	this.Name			= Name;
	this.cookieLifetime = cookieLifetime;
	this.bStoreCookie	= (cookieLifetime != 0);

	// *CONVENIENCE* PROPERTIES
	// (not used internally, but added to provide a place to store this data)
	this.CustomerID			= null;
	this.OrderID				= null;
	this.Tax						= null;
	this.ShippingCost		= null;

  // CONSTANTS
  this.PRODUCTID	= "ProductID";  // Required SKU cart column
  this.QUANTITY		= "Quantity";   // Required Quantity cart column
	this.PRICE			= "Price";			// Required Price cart column
	this.TOTAL			= "Total";			// Required Total column
  this.cookieColDel = "#UC_C#"
  this.cookieRowDel = "#UC_R#"

	// METHODS
	this.AssertCartValid = AssertCartValid

  // Private methods - don't call these unless you understand the internals.
  this.GetIndexOfColName = UCgetIndexOfColName;
  this.GetDataFromBindings = UCgetDataFromBindings;
	this.FindItem = UCfindItem;
	this.ComputeItemTotals = ComputeItemTotals;
  this.persist = UCpersist;
  
	this.BuildInsertColumnList = BuildInsertColumnList;
	this.BuildInsertValueList = BuildInsertValueList;
	this.UpdateQuantities = UpdateQuantities;
	this.UpdateTotals = UpdateTotals;
	this.DeleteItemsWithNoQuantity = DeleteItemsWithNoQuantity;
	this.CheckAddItemConfig = CheckAddItemConfig;
	this.ColumnExistsInRS = ColumnExistsInRS;
	this.DeleteLineItem = DeleteLineItem;
	this.GetCookieName = GetCookieName;
	this.SetCookie = SetCookie;
	this.PopulateFromCookie = PopulateFromCookie;
	this.DestroyCookie = UCDestroyCookie;

// Cart "internals" documentation:
// The this.SC datastructure is a single variable of type array.
// Each array element corresponds to a cart column. For example: 
//    Array element 1: ProductID
//    Array element 2: Quantity
//    Array element 3: Price
//    Array elemetn 4: Total
//
// Each of these is an array. Each array index corresponds to a line item.
// As such, each array should always be exactly the same length.
  this.AssertCartValid(colNames, "Cart Initialization: ");
	if (Session(this.Name) != null) {
		this.SC = Session(this.Name).SC;
	} else {
		this.SC = new Array(this.numCols);
		for (var i = 0; i < this.numCols; i++) this.SC[i] = new Array();

		// Since the cart doesn't exist in session, check for cookie from previous session
		if (this.bStoreCookie){
			cookieName = this.GetCookieName();
			cookieStr = Request.Cookies(cookieName);
			if (cookieStr != null && String(cookieStr) != "undefined" && cookieStr != "")
				this.PopulateFromCookie(cookieStr);
		}
		// Create a reference in the Session, pass the whole object (methods are not copied)
    this.persist();
	}  
}

// convert vb style arrays to js style arrays.
function UC_VbToJsArray(a) {
	if (a!=null && a.length==null) {
		a = new VBArray(a);
		a = a.toArray();
	}
	return a;
}

function UCpersist() {
  Session(this.Name) = this;
  if (this.bStoreCookie) this.SetCookie();
}

function UCDestroy(){
	this.SC = new Array(this.numCols);  // empty the "in-memory" cart.
	for (var i = 0; i < this.numCols; i++) this.SC[i] = new Array();
  this.persist();
	if (this.bStoreCookie) this.DestroyCookie() // remove the cookie
}

function UCgetDataFromBindings(adoRS, bindingTypes, bindingValues) {
	var values = new Array(bindingTypes.length)
	for (i=0; i<bindingTypes.length; i++) {
		var bindVal = bindingValues[i];
		if (bindingTypes[i] == "RS"){
			values[i] = String(adoRS(bindVal).Value)
			if (values[i] == "undefined") values[i] = "";
		}
		else if (bindingTypes[i] == "FORM"){
			values[i] = String(Request(bindVal))
			if (values[i] == "undefined") values[i] = "";
		} 
		else if (bindingTypes[i] == "LITERAL") values[i] = bindVal;
		else if (bindingTypes[i] == "NONE") values[i] = "";						// no binding
		else assert(false,"Unrecognized binding type: " + bindingTypes[i]);		// Unrecognized binding type
	}
	return values;
}

function UCfindItem(bindingTypes, values){
  // A product is a duplicate if it has the same unique ID
  // AND all values from form bindings (except quantity) are the same
  var indexProductID = this.GetIndexOfColName(this.PRODUCTID);
  var indexQuantity  = this.GetIndexOfColName(this.QUANTITY);
  assert(indexProductID >=0, "UC_Cart.js: Internal error 143");
  assert(indexQuantity >=0, "UC_Cart.js: Internal error 144");
	var newRow = -1
  for (var iRow=0; iRow<this.GetItemCount(); iRow++) {
    found = true;  // assume found
    for (var iCol=0; iCol<this.numCols; iCol++) {
      if (iCol != indexQuantity) {
        if ((iCol==indexProductID) || (bindingTypes[iCol]=="FORM")) {
          if (this.SC[iCol][iRow] != values[iCol]) {
            found = false;
            break;
        } }
    } }
    if (found) {
      newRow = iRow;
      break;
    }
  }
	return newRow
}

function UCaddItem(adoRS, bindingTypes, bindingValues, alreadyInCart){
  // alreadyInCart can be "increment" or "replace" to handle duplicate items in cart.
	bindingTypes = UC_VbToJsArray(bindingTypes);
	bindingValues = UC_VbToJsArray(bindingValues);

	// Check that length of binding types/values arrays is consistent with cart configuration
	assert(bindingTypes.length  == this.numCols, "UCaddItem: Array length mismatch (internal error 403)");
	assert(bindingValues.length == this.numCols, "UCaddItem: Array length mismatch (internal error 404)");

  // debug call
	//this.CheckAddItemConfig(adoRS, bindingTypes, bindingValues);

	var values = this.GetDataFromBindings(adoRS, bindingTypes, bindingValues) // get the actual values based on bindings
  var newRow = this.FindItem(bindingTypes, values);							// Check if this item is already in cart
  if (newRow == -1) {													// append a new item
		newRow = this.GetItemCount();    
    for (var iCol=0; iCol<this.numCols; iCol++) { // add data
      this.SC[iCol][newRow] = values[iCol];
    }
		this.ComputeItemTotals(newRow);						// add computed columns (defined in colsComputed)		
    this.persist();
	} else if (alreadyInCart == "increment") {
    var indexQuantity  = this.GetIndexOfColName(this.QUANTITY);
    this.SC[indexQuantity][newRow] = parseInt(this.SC[indexQuantity][newRow]) + parseInt(values[indexQuantity])
    if (isNaN(this.SC[indexQuantity][newRow])) this.SC[indexQuantity][newRow] = 1;
		this.ComputeItemTotals(newRow);
    this.persist();
	}
}

function UCgetIndexOfColName(colName) {
  var retIndex = -1;
  for (var i=0; i<this.numCols; i++) {
    if (this.colNames[i] == colName) {
      retIndex = i;
      break;
		} 
	}
  return retIndex;
}

function ComputeItemTotals(row){
	var indexQuantity = this.GetIndexOfColName(this.QUANTITY);
  var qty = parseInt(this.SC[indexQuantity][row])
	for (var iCol=0; iCol<this.numCols; iCol++) {
		var colToCompute = this.colComputed[iCol];
		if (colToCompute != "") {
		  indexColToCompute = this.GetIndexOfColName(colToCompute);
		  this.SC[iCol][row] = parseFloat(this.SC[indexColToCompute][row]) * qty;
		}
	}
}

function CheckAddItemConfig(adoRS, bindingTypes, bindingValues) {
	var ERR_SOURCE = "CheckAddItemConfig: "
	var ERR_RS_BINDING_VALUE = "Column for Recordset binding does not exist in recordset";
	// Check that all rs column names exist for rs binding types
	for (var i = 0; i < bindingTypes.length; i++) {
		if (bindingTypes[i] == "RS"){
			assert(this.ColumnExistsInRS(adoRS, bindingValues[i]), ERR_SOURCE + bindingValues[i] + ": " + ERR_RS_BINDING_VALUE);	
		}
	}  
}

function ColumnExistsInRS(adoRS, colName) {
	var bColExists = false;
	var items = new Enumerator(adoRS.Fields);
	while (!items.atEnd()) {
		if (items.item().Name == colName){
			bColExists = true;
			break;
		}
		items.moveNext();
	}
	return bColExists;
}

function GetColumnValue(colName, row){
	var retValue = "&nbsp;";
  var indexCol = this.GetIndexOfColName(colName);
	assert(!isNaN(row), "cart.GetColumnValue: row is not a number - row = " + row);
  assert(indexCol >=0, "cart.GetColumnValue: Could not find column \"" + colName + "\" in the cart");
  assert(row>=0, "cart.GetColumnValue: Bad row number input to cart - row = " + row);
  assert(this.GetItemCount()>0, "cart.GetColumnValue: The cart is empty - the requested data is unavailable");
  assert(row<this.GetItemCount(), "cart.GetColumnValue: The line item number is greater than the number of items in the cart - row = " + row + "; GetItemCount = " + this.GetItemCount());
  if (this.GetItemCount()>0) {
	  retValue = this.SC[indexCol][row];
	}
	return retValue;
}

function UpdateQuantities(formElementName) {
	var items = new Enumerator(Request.Form(formElementName))
	var j = 0;
  indexQuantity = this.GetIndexOfColName(this.QUANTITY);
	while(!items.atEnd()){
		var qty = parseInt(items.item());
		if (isNaN(qty) || qty < 0) {
		  this.SC[indexQuantity][j++] = 0
		} else {
		  this.SC[indexQuantity][j++] = qty;
		}
		items.moveNext();
	}
}

function UpdateTotals() {
  // this would be a little more efficient by making the outer loop over cols rather than rows.
	for (var iRow=0; iRow<this.GetItemCount(); iRow++) {
		this.ComputeItemTotals(iRow);
	}
}

function DeleteItemsWithNoQuantity() {
	var tmpSC= new Array(this.numCols);
  for (var iCol=0; iCol<this.numCols; iCol++) tmpSC[iCol] = new Array();

  var indexQuantity = this.GetIndexOfColName(this.QUANTITY);
  var iDest = 0;
	for (var iRow=0; iRow<this.GetItemCount(); iRow++) {    
    if (this.SC[indexQuantity][iRow] != 0) {
      for (iCol=0; iCol<this.numCols; iCol++) {
        tmpSC[iCol][iDest] = this.SC[iCol][iRow];
      }
      iDest++;
		}
	}
  this.SC = tmpSC;
}

function Update(formElementName){
	// Get new quantity values from Request object.
	// Assume they are all named the same, so you will get 
	// an array. The array length should be the same as the number
	// of line items and in the same order.
	this.UpdateQuantities(formElementName);
	this.DeleteItemsWithNoQuantity();
	this.UpdateTotals();
	this.persist();
}

function BuildInsertColumnList(orderIDCol, mappings){
	var colList = orderIDCol;
	for (var i = 0; i < mappings.length; i++) {
		if (mappings[i] != ""){
			colList += ", " + mappings[i];
		}
	}
	colList = "(" + colList + ")";
	return colList;
}

function BuildInsertValueList(orderIDColType, orderIDVal, destCols, destColTypes, row){
  var values = "";
  if (orderIDColType == "num") {
    values += orderIDVal;
  } else {
    values += "'" + orderIDVal.toString().replace(/'/g, "''") + "'";
  }

	for (var iCol=0; iCol<this.numCols; iCol++){
		if (destCols[iCol] != "") {
			if (destColTypes[iCol] == "num") {
        assert(this.SC[iCol][row] != "", "SaveToDatabase: A numeric value is missing in the SQL statement in column " + this.colNames[iCol]);
			  values += ", " + this.SC[iCol][row];
			} else {
			  values += ", '" + (this.SC[iCol][row]).toString().replace(/'/g, "''") + "'";  
			} 
		}	
	}
	values = "(" + values + ")";
	return values;
}

function SaveToDatabase(adoConn, dbTable, orderIDCol, orderIDColType, orderIDVal, destCols, destColTypes){
	// we are going to build SQL INSERT statements and 
	// throw it at the connection / table
	// Similar to existing UD insert to database behavior
	var ERR_MAPPINGS_LENGTH = "Array length must match the number of cart columns<BR>";
	var ERR_TRANS = "An error occured when inserting cart items in the database.  The transaction was rolled back<BR>";
	destCols = UC_VbToJsArray(destCols);
	destColTypes = UC_VbToJsArray(destColTypes);
	assert (destCols.length == this.numCols, "SaveToDatabase: " + "destCols - " + ERR_MAPPINGS_LENGTH);
	assert (destColTypes.length == this.numCols, "SaveToDatabase: " + "destColTypes - " + ERR_MAPPINGS_LENGTH);

	var insertColList = this.BuildInsertColumnList(orderIDCol, destCols);

	if (insertColList != "") { //proceed only if we have a column list
		var insertClause = "INSERT INTO " + dbTable + " " + insertColList + " VALUES ";
		var recs;
		adoConn.BeginTrans();
		for (var iRow=0; iRow<this.GetItemCount(); iRow++){
			var valList = this.BuildInsertValueList(orderIDColType, orderIDVal, destCols, destColTypes, iRow);
			var sql = insertClause + valList;
			adoConn.Execute(sql, recs, 1 /*adCmdText*/); 
		}
		if (adoConn.Errors.Count == 0){ 
			adoConn.CommitTrans();
			this.Destroy();	// All items saved to database, we can trash the cart
		}	else {
			adoConn.RollbackTrans();
			//assert(false, "SaveToDatabase: " + ERR_TRANS); Don't assert here - let ASP display the database error.
		}
	}
}

function GetItemCount(){
	return this.SC[0].length
}

function GetColumnTotal(colName){
	// Generic column Total function
	var colTotal = 0.0;
	index = this.GetIndexOfColName(colName);
	for (var i=0; i<this.SC[index].length; i++)
		colTotal += parseFloat(this.SC[index][i]);
    
	return colTotal
}


function DeleteLineItem(row){
	assert(!isNaN(row), "Failure in call to DeleteLineItem - row is not a number");
  assert(row>=0 && row <this.GetItemCount(), "failure in call to DeleteLineItem (internal error 121)");

	var tmpSC= new Array(this.numCols);
  var iDest = 0;
  for (var iCol=0; iCol<this.numCols; iCol++) tmpSC[iCol] = new Array();
  for (var iRow=0; iRow<this.GetItemCount(); iRow++) {
    if (iRow != row) {
      for (iCol=0; iCol<this.numCols; iCol++) {
        tmpSC[iCol][iDest] = this.SC[iCol][iRow];
      }
      iDest++;
		}
	}
  this.SC = tmpSC;
  this.persist();
}

function UCGetColNamesSerial(colDelim) {
  var serialCols = "";
  for (var iCol=0; iCol<this.numCols; iCol++) {
    if (iCol != 0) serialCols += colDelim;
    serialCols += this.colNames[iCol];
  }
  return serialCols;
}

function UCGetContentsSerial(colDelim, rowDelim) {
  var serialCart = "";
  for (var iRow=0; iRow<this.GetItemCount(); iRow++) {
    if (iRow != 0) serialCart += rowDelim
    for (var iCol=0; iCol<this.numCols; iCol++) {
      if (iCol != 0) serialCart += colDelim;
      serialCart += this.SC[iCol][iRow];
    }
  }
  return serialCart;
}

function UCSetContentsSerial(serialCart, colDelim, rowDelim) {
	var Rows = String(serialCart).split(rowDelim)
	for (iRow = 0; iRow < Rows.length; iRow++) {
		if (Rows[iRow] != "undefined" && Rows[iRow] != "") {
			Cols = Rows[iRow].split(colDelim)
			iCol = 0
			for (iCol = 0; iCol<Cols.length; iCol++) {
				this.SC[iCol][iRow] = Cols[iCol]
			}
		}
	}
	this.persist();
}

function SetCookie(){
	var cookieName = this.GetCookieName()
	var cookieStr = this.GetContentsSerial(this.cookieColDel, this.cookieRowDel)
	var cookieExp = GetCookieExp(this.cookieLifetime)
	Response.Cookies(cookieName) = cookieStr
	Response.Cookies(cookieName).expires = cookieExp
}

function GetCookieName(){
	var server = Request.ServerVariables("SERVER_NAME");
	return  server + this.Name;
}

function UCDestroyCookie(){
	cookieName = this.GetCookieName();
	Response.Cookies(cookieName) = ""
	Response.Cookies(cookieName).expires = "1/1/90"
}

function PopulateFromCookie(cookieStr){
  this.SetContentsSerial(cookieStr, this.cookieColDel, this.cookieRowDel)
}

// ***************** debug code ********************
function assert(bool, msg) {
	if (!bool) {
		Response.Write("<BR><BR>An error occured in the UltraDev shopping cart:<BR>" + msg + "<BR>");
		//Response.End();
	}
}

function AssertCartValid(colNames, msg) {
	// go through all cart data structures and insure consistency.
	// For example all column arrays should be the same length.
	// this function should be called often, especially just after
	// makeing changes to the data structures (adding, deleting, etc.)
	// also verify we always have the required columns:
	// ProductID, Quantity, Price, Total

	// the input arg is some I add as I code this package like
	// "Prior to return from AddToCart"
	//
	var ERR_COOKIE_SETTINGS = "Cookie settings on this page are inconsistent with those stored in the session cart<BR>"; 
	var ERR_BAD_NAME = "Cart name defined on this page is inconsistent with the cart name stored in the session<BR>";
	var ERR_COLUMN_COUNT = "The number of cart columns defined on this page is inconsistent with the cart stored in the session<BR>";
	var ERR_REQUIRED_COLUMNS = "Too few columns; minimum number of columns is 4<BR>";
	var ERR_REQUIRED_COLUMN_NAME = "Required Column is missing or at the wrong offset: ";
	var ERR_COLUMN_NAMES = "Cart column names defined on this page are inconsistent with the cart stored in the session";
	var ERR_INCONSISTENT_ARRAY_LENGTH = "Length of the arrays passed to cart constructor are inconsistent<BR>"
	var errMsg = "";
	var sessCart = Session(this.Name);

	if (sessCart != null) { // Validate inputs against session cart if it exists
		if (sessCart.Name != this.Name) errMsg += ERR_BAD_NAME;
		if (this.numCols < 4) errMsg += ERR_REQUIRED_COLUMNS;
		if (sessCart.numCols != this.numCols) errMsg += "Column Name Array: " + ERR_COLUMN_COUNT;
		if (sessCart.numCols != this.colComputed.length) errMsg += "Computed Column Array: " + ERR_COLUMN_COUNT;
		if (sessCart.bStoreCookie != this.bStoreCookie) errMsg += "Using Cookies: " + ERR_COOKIE_SETTINGS;
		if (sessCart.cookieLifetime != this.cookieLifetime) errMsg += "Cookie Lifetime: " + ERR_COOKIE_SETTINGS;

		// check that required columns are in the same place
		var productIndex = this.GetIndexOfColName(this.PRODUCTID);
		var quantityIndex = this.GetIndexOfColName(this.QUANTITY);
		var priceIndex = this.GetIndexOfColName(this.PRICE);
		var totalIndex = this.GetIndexOfColName(this.TOTAL);

		if (colNames[productIndex] != "ProductID") errMsg += ERR_REQUIRED_COLUMN_NAME + "ProductID<BR>";
		if (colNames[quantityIndex] != "Quantity") errMsg += ERR_REQUIRED_COLUMN_NAME + "Quantity<BR>";
		if (colNames[priceIndex] != "Price") errMsg += ERR_REQUIRED_COLUMN_NAME + "Price<BR>";
		if (colNames[totalIndex] != "Total") errMsg += ERR_REQUIRED_COLUMN_NAME + "Total<BR>";
	}
	else { // if cart doesn't exist in session, validate input array lengths and presence of reqiured columns
		if (this.numCols != this.colComputed.length) errMsg += ERR_INCONSISTENT_ARRAY_LENGTH;
		
		var bProductID = false, bQuantity = false, bPrice = false, bTotal = false;

		for (var j = 0; j < colNames.length; j++) {
			if (colNames[j] == "ProductID") bProductID = true;
			if (colNames[j] == "Quantity") bQuantity= true;
			if (colNames[j] == "Price") bPrice = true;
			if (colNames[j] == "Total") bTotal = true;
		}
		if (!bProductID) errMsg += ERR_REQUIRED_COLUMN_NAME + "ProductID<BR>";
		if (!bQuantity) errMsg += ERR_REQUIRED_COLUMN_NAME + "Quantity<BR>";
		if (!bPrice) errMsg += ERR_REQUIRED_COLUMN_NAME + "Price<BR>";
		if (!bTotal) errMsg += ERR_REQUIRED_COLUMN_NAME + "Total<BR>";
	}
	
	if (errMsg != "") {
		Response.Write(msg + "<BR>");
		Response.Write(errMsg + "<BR>");
		Response.End();
	}
}

function VBConstuctCart(Name, cookieLifetime, vbArrColNames, vbArrColComputed){
	var myObj;
	var a = new VBArray(vbArrColNames);
	var b = new VBArray(vbArrColComputed);
	eval("myObj = new UC_ShoppingCart(Name, cookieLifetime, a.toArray(), b.toArray())");
	return myObj;
}
</SCRIPT>
<SCRIPT LANGUAGE=vbscript runat=server NAME="UC_CART">
Function GetCookieExp(expDays) 
 	vDate = DateAdd("d", CInt(expDays), Now())
 	GetCookieExp = CStr(vDate)
End Function
</SCRIPT>
<SCRIPT RUNAT=SERVER LANGUAGE=VBSCRIPT NAME="UC_CART">
function DoNumber(str, nDigitsAfterDecimal, nLeadingDigit, nUseParensForNeg, nGroupDigits)
	DoNumber = FormatNumber(str, nDigitsAfterDecimal, nLeadingDigit, nUseParensForNeg, nGroupDigits)
End Function

function DoCurrency(str, nDigitsAfterDecimal, nLeadingDigit, nUseParensForNeg, nGroupDigits)
	DoCurrency = FormatCurrency(str, nDigitsAfterDecimal, nLeadingDigit, nUseParensForNeg, nGroupDigits)
End Function

function DoDateTime(str, nNamedFormat, nLCID)
	dim strRet
	dim nOldLCID

	strRet = str
	If (nLCID > -1) Then
		oldLCID = Session.LCID
	End If

	On Error Resume Next

	If (nLCID > -1) Then
		Session.LCID = nLCID
	End If

	If ((nLCID < 0) Or (Session.LCID = nLCID)) Then
		strRet = FormatDateTime(str, nNamedFormat)
	End If
										
	If (nLCID > -1) Then
		Session.LCID = oldLCID
	End If			
										
	DoDateTime = strRet
End Function

function DoPercent(str, nDigitsAfterDecimal, nLeadingDigit, nUseParensForNeg, nGroupDigits)
	DoPercent = FormatPercent(str, nDigitsAfterDecimal, nLeadingDigit, nUseParensForNeg, nGroupDigits)
End Function							

function DoTrim(str, side)
	dim strRet
	strRet = str

	If (side = "left") Then
		strRet = LTrim(str)
	ElseIf (side = "right") Then
		strRet = RTrim(str)
	Else
		strRet = Trim(str)
	End If
	DoTrim = strRet
End Function
</SCRIPT>
<%
UC_CartColNames=Array("Special","SizeType","SizeTypeID","Size","SizeID","Colour","ColourID","ProductCode","ProductID","Quantity","Name","Price","Total")
UC_ComputedCols=Array("","","","","","","","","","","","","Price")
set UCCart1=VBConstuctCart("SPCart",30,UC_CartColNames,UC_ComputedCols)
UCCart1__i=0
%>
<%
UC_updateAction = CStr(Request("URL"))
If (Request.QueryString <> "") Then
  UC_updateAction = UC_updateAction & "?" & Request.QueryString
End If
If (Request.Form("Quantity").Count > 0) Then
  UCCart1.Update("Quantity")
  If ("" <> "") Then
    Response.Redirect("")
  End If
End If
%>
<HTML>
<HEAD>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%><%MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%>
<!--#include virtual="DB/spDB.asp" -->
<!--#include virtual="inc/date.asp" -->
<%
Set RSquickcat = Server.CreateObject("ADODB.Recordset")
RSquickcat.ActiveConnection = SPdbString
RSquickcat.Source = "SELECT spItemCatagory.id, spItemCatagory.Name FROM spItemCatagory ORDER BY spItemCatagory.Name ASC"
RSquickcat.CursorType = 0
RSquickcat.CursorLocation = 2
RSquickcat.LockType = 3
RSquickcat.Open()
%>
<TITLE>Sequins Plus Online - Shopping Cart</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<!--#include virtual="inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR=#DFDFDF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="MM_preloadImages('/images/cartbut_1_over.gif','/images/cartbut_2_over.gif','/images/cartbut_3_over.gif','/images/cartbut_4_over.gif','/images/cartbut_5_over.gif','/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="28" colspan="4"></td>
    <td height="28" colspan="4" valign="top"><IMG SRC="/images/logo_top.gif" WIDTH=281 HEIGHT=28></td>
    <td height="28" colspan="7"></td>
  </tr>
  <tr> 
    <td width="8" height="9"></td>
    <td height="9" colspan="3" valign="top"><IMG SRC="/images/logo_lft_top_line.gif" WIDTH=155 HEIGHT=9></td>
    <td height="9" colspan="4" valign="top"><IMG SRC="/images/logo_topline.gif" WIDTH=281 HEIGHT=9></td>
    <td height="9" colspan="6" valign="top"><IMG SRC="/images/logo_rt_top_line.gif" WIDTH=326 HEIGHT=9></td>
    <td width="10" height="9"></td>
  </tr>
  <tr> 
    <td width="8" height="26"></td>
    <td height="26" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td height="26" colspan="4" valign="top"><IMG SRC="/images/logo_title.gif" WIDTH=281 HEIGHT=26></td>
    <td height="26" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr> 
    <td width="8" height="11"></td>
    <td height="11" colspan="3" valign="top"><IMG SRC="/images/logo_lft_bot_line.gif" WIDTH=155 HEIGHT=11></td>
    <td height="11" colspan="4" valign="top"><IMG SRC="/images/logo_bot-line.gif" WIDTH=281 HEIGHT=11></td>
    <td height="11" colspan="6" valign="top"><IMG SRC="/images/logo_rt_bot_line.gif" WIDTH=326 HEIGHT=11></td>
    <td width="10" height="11"></td>
  </tr>
  <tr> 
    <td width="8" height="14"></td>
    <td height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td height="14" colspan="4" valign="top"><IMG SRC="/images/logo_beige.gif" WIDTH=281 HEIGHT=14></td>
    <td height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr> 
    <td width="8" height="2"></td>
    <td height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td height="2" colspan="4" valign="top"><IMG SRC="/images/logo_spacer.gif" WIDTH=281 HEIGHT=2></td>
    <td height="2" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr> 
    <td width="8" height="19"></td>
    <td height="19" colspan="3" bgcolor="#FFFFFF"></td>
    <td height="19" colspan="4" valign="top"><IMG SRC="/images/cartpage_22.gif" WIDTH=281 HEIGHT=19></td>
    <td height="19" colspan="6" valign="top" bgcolor="#FFFFFF"><img src="/images/red_arrow.gif" width="17" height="17" align="absmiddle"><img src="/images/transparent.gif" width="5" height="1"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong>Shopping 
      Cart Items:</strong></font> <%=UCCart1.GetItemCount()%> <strong><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Subtotal:</font></strong> 
      $<%=FormatNumber(UCCart1.GetColumnTotal("Total"), 2, -2, -2, -1)%></td></td>
    <td width="4" height="19"></td>
  </tr>
  <tr> 
    <td width="8" height="5"></td>
    <td height="5" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="5"></td>
  </tr>
  <tr> 
    <td width="8" height="20"></td>
    <td width="12" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('homepage','','/images/cartbut_1_over.gif',1)"><img src="/images/cartbut_1.gif" alt="Homepage" name="homepage" width="141" height="20" border="0"></a></td>
    <td height="20" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/orders.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('check','','/images/cartbut_2_over.gif',1)"><img src="/images/cartbut_2.gif" alt="Check Your Order Status" name="check" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td height="20" colspan="2" valign="top"><a href="/en/order/new.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('create','','/images/cartbut_3_over.gif',1)"><img src="/images/cartbut_3.gif" alt="Create A New Account" name="create" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/login.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('signin','','/images/cartbut_4_over.gif',1)"><img src="/images/cartbut_4.gif" alt="Sign In" name="signin" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('account','','/images/cartbut_5_over.gif',1)"><img src="/images/cartbut_5.gif" alt="Account Services" name="account" width="141" height="20" border="0"></a></td>
    <td width="13" height="20" bgcolor="#FFFFFF"></td>
    <td width="10" height="20"></td>
  </tr>
  <tr> 
    <td width="8" height="6"></td>
    <td height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr> 
    <td width="8" height="113"></td>
    <td height="344" colspan="2" rowspan="3" align="center" valign="top" bgcolor="#FFFFFF"><!--#include virtual="inc/cartSB.asp" --></td>
    <td width="2" height="113" bgcolor="#FFFFFF"></td>
    <td width="6" height="113" bgcolor="#FFFFFF"></td>
    <td height="113" colspan="9" valign="top" bgcolor="#FFFFFF"> <table width="601" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
        <tr> 
          <td height="10" colspan="2" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="1"><font color="#FFFFFF" size="5" face="Arial, Helvetica, sans-serif">&#8226; 
            Shopping Cart</font></td>
          <form name="jumpmenu" onSubmit="return jump(document.jumpmenu.menu.options[document.jumpmenu.menu.options.selectedIndex].value)">
            <td align="right" valign="top" bgcolor="#CC1C0F"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font> 
              <select align="middle" name="menu" >
                <option value="none" selected>More Categories</option>
                <% While (NOT RSquickcat.EOF) %>
                <option value="<%="/en/products/default.asp?Cat="&(RSquickcat.Fields.Item("id").Value)%>"  ><%=(RSquickcat.Fields.Item("Name").Value)%></option>
                <% 
		  RSquickcat.MoveNext()
		  Wend
		  RSquickcat.close()
		  %>
              </select> <input name="button" type="image" src="/images/btn_go.jpg" alt="Search" align="middle" width="27" height="27" hspace="0" vspace="0" border="0"> 
            </td>
          </form>
        </tr>
        <tr> 
          <td height="10" colspan="2" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr valign="top"> 
          <td colspan="2"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">You 
            have choosen to order the items listed below. To complete your order 
            click the "Continue Checkout" button.</font></td>
        </tr>
        <tr valign="top"> 
          <td height="20" colspan="2"><img src="/images/transparent.gif" width="1" height="20"></td>
        </tr>
      </table></td>
    <td width="10" height="113"></td>
  </tr>
  <tr valign="top"> 
    <td width="8" height="473" ></td>
    <td width="2"  bgcolor="#FFFFFF"></td>
    <td width="6"  bgcolor="#FFFFFF"></td>
    <td  colspan="9" rowspan="2" valign="top" bgcolor="#FFFFFF"> <table width="601" height="55" border="0" cellpadding="5" cellspacing="1" bgcolor="#6A6A6A" mm_noconvert="TRUE">
        <tr> 
          <td width="68" align="center" valign="top" bgcolor="#EBEBEB"><img src="/images/remove-grey.gif" width="68" height="15"></td>
          <td width="110" align="center" valign="top" bgcolor="#EBEBEB"><font color="#666666" size="2" face="Arial, Helvetica, sans-serif"><strong>Description</strong></font></td>
          <td align="center" valign="top" bgcolor="#EBEBEB"><font color="#666666" size="2" face="Arial, Helvetica, sans-serif"><strong>Size 
            &amp; </strong></font><font color="#666666" size="2" face="Arial, Helvetica, sans-serif"><strong>Colour 
            Options </strong></font></td>
          <td width="50" align="center" valign="top" bgcolor="#EBEBEB"><font color="#666666" size="2" face="Arial, Helvetica, sans-serif"><strong>Price</strong></font></td>
          <td width="25" align="center" valign="top" bgcolor="#EBEBEB"><font color="#666666" size="2" face="Arial, Helvetica, sans-serif"><strong>Qty</strong></font></td>
          <td width="81" align="center" valign="top" bgcolor="#EBEBEB"><font color="#666666" size="2" face="Arial, Helvetica, sans-serif"><strong>Total</strong></font></td>
        </tr>
        <%if UCCart1.GetItemCount() = 0 then%>
        <tr> 
          <td colspan="6" align="center" valign="top" bgcolor="#FFFFFF"><br> <b>Your 
            cart is empty</b><br> <br> </td>
        </tr><form action="<%=UC_updateAction%>" method="POST" name="ShoppingCart">
        <%else%>
        <form action="<%=UC_updateAction%>" method="POST" name="ShoppingCart">
          <% For UCCart1__i=0 To UCCart1.GetItemCount()-1 %>
          <tr> 
            <td width="68" rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF"> 
              <input input name="Remove" class="text_nb" id="Quanity" type="checkbox" value="0"> 
            </td>
            <td width="110" bgcolor="#FFFFFF"><%=(UCCart1.GetColumnValue("Name",UCCart1__i))%></td>
            <%
Set RScartsizes = Server.CreateObject("ADODB.Recordset")
RScartsizes.ActiveConnection = SPdbString
RScartsizes.Source = "SELECT spItemSizes.id, spItemSizes.ItemType, spItemSizes.itemSize, spItemSizes.itemID, spItemSize.id as sizeid, spItemSize.ItemSizeTxt, spItemSizeType.id as typeid, spItemSizeType.name as typename FROM spItemSizes, spItemSize, spItemSizeType WHERE spItemSizes.itemID = " & UCCart1.GetColumnValue("ProductID",UCCart1__i) & " AND spItemSizes.itemSize = spItemSize.id AND  spItemSizeType.id = spItemSizes.ItemType ORDER BY spItemSizeType.name DESC"
RScartsizes.CursorType = 0
RScartsizes.CursorLocation = 2
RScartsizes.LockType = 1
RScartsizes.Open()
RScartsizes_numRows = 0
%><%
if  NOT (RScartsizes.EOF or RScartsizes.bOF) then
cstype = RScartsizes.Fields.Item("typename").Value
cstypeid = RScartsizes.Fields.Item("typeid").Value
cstypearray = RScartsizes.Fields.Item("typename").Value
cstypeidarray = RScartsizes.Fields.Item("typeid").Value
end if
while NOT (RScartsizes.EOF or RScartsizes.bOF) 
if cstype <> RScartsizes.Fields.Item("typename").Value then
cstype = RScartsizes.Fields.Item("typename").Value
cstypeid = RScartsizes.Fields.Item("typeid").Value
cstypearray = cstypearray & "|" & RScartsizes.Fields.Item("typename").Value
cstypeidarray = cstypeidarray & "|" & RScartsizes.Fields.Item("typeid").Value
end if
RScartsizes.movenext
wend
Dim csasizes
csasizes = split(cstypearray, "|")
csasizeid = split(cstypeidarray, "|")
RScartsizes.Close
%><td rowspan="2" align="center" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><strong>Size:</strong> 
                <%=(UCCart1.GetColumnValue("Size",UCCart1__i))%><br><strong>Size Type:</strong> 
                <%if UBound(csasizes) = 0 then%>
                <%=csasizes(0)%> 
                <input name="SizeType2" type="hidden" value="<%=csasizes(0)%>">
                <%else%>
                <select name="select" id="select3">
                  <option selected>Unspecified</option>
                  <%for i = 0 to UBound(csasizes)%>
                  <option value="<%=csasizeid(i)%>"><%=csasizes(i)%></option>
                  <%next%>
                  </select>
                <%end if%>
                <br><strong>Colour:</strong><%set RScartCol = Server.CreateObject("ADODB.Recordset")
RScartCol.ActiveConnection = SPdbString
RScartCol.Source = "SELECT spItemColours.itemID, spItemColours.itemColour, spItemColours.itemPic, spItemColours.id as colid, spItemColour.ItemColTxt, spItemColour.id  FROM spItemColours, spItemColour  WHERE spItemColours.itemColour =  spItemColour.id AND spItemColours.itemID = " & UCCart1.GetColumnValue("ProductID",UCCart1__i)
RScartCol.CursorType = 0
RScartCol.CursorLocation = 2
RScartCol.LockType = 1
RScartCol.Open()
RScartCol_numRows = 0
ccollen = 0 
while NOT (RScartCol.EOF OR RScartCol.BOF)
ccollen = ccollen + 1
RScartCol.MoveNext()
Wend
RScartCol.requery
if ccollen = 1 then%><b><%=RScartCol.Fields.Item("ItemColTxt").Value%></b> 
                <input name="Colour2" type="hidden" value="<%=RScartCol.Fields.Item("ItemColTxt").Value%>">
                <%else%>
                <input type="hidden" name="Colour2" value="<%=RScartCol.Fields.Item("ItemColTxt").Value%>">
                <input type="hidden" name="ColourID2" value="<%=RScartCol.Fields.Item("colid").Value%>">
                <select name="colour" id="select">
                  <option value="<%=RScartCol.Fields.Item("colid").Value%>"><%=RScartCol.Fields.Item("ItemColTxt").Value%></option>
                </select>
                <%RScartCol.close()
		  end if%>
                </font></p>
              </td>
            <td rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$<%=FormatNumber(UCCart1.GetColumnValue("Price",UCCart1__i), 2, -2, -2, -1)%></font></font></td>
            <td rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF"> 
              <input name="Quantity" type="text" value="<%=(UCCart1.GetColumnValue("Quantity",UCCart1__i))%>" size="2" maxlength="2"> 
            </td>
            <td width="81" rowspan="2" align="center" valign="middle" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">$<%=(UCCart1.GetColumnValue("Total",UCCart1__i))%></font></strong></font></td>
          </tr>
          <tr> 
            <td width="110" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
              #:<%=(UCCart1.GetColumnValue("ProductCode",UCCart1__i))%></font></td>
          </tr>
          <% Next 'UCCart1__i %>
          <%end if%>
          <tr align="right"> 
            <td colspan="3" align="left" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="15" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">If 
              you make any changes above click</font><img src="/images/transparent.gif" width="10" height="1"> 
              <input name="Update" type="image" id="Update" src="/images/update-grey.gif" align="absmiddle" width="56" height="16" border="0"></td>
            <td colspan="3" align="right" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong>Subtotal:</strong></font><font size="2" face="Arial, Helvetica, sans-serif"> 
              $</font><font color="#000000"><strong><%=(UCCart1.GetColumnTotal("Total"))%></strong></font></td>
          </tr>
        </form>
        <tr> 
          <td colspan="3" bgcolor="#EBEBEB">&nbsp;</td>
          <% ' ---- tax calc ----

Dim vTax
Dim vTaxTotal
Dim vTaxRate

vTax = 0.00
vTaxRate = 0.15

vTaxTotal = (1.0 + vTaxRate) * (UCCart1.GetColumnTotal("Total"))
vTax = vTaxTotal - UCCart1.GetColumnTotal("Total")
%>
          <td colspan="3" align="right" bgcolor="#EBEBEB"><font color="#666666" size="2" face="Arial, Helvetica, sans-serif"><strong>Taxes: 
            </strong></font><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$</font></font><font color="#000000"><strong><%=FormatNumber(vTax, 2, -2, -2, -1)%></strong></font></td>
        </tr>
        <tr> 
          <td colspan="3" bgcolor="#FFFFFF">&nbsp;</td>
          <td colspan="3" align="right" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong>Total:</strong></font><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"> 
            <strong>$<%=FormatNumber(vTaxTotal, 2, -2, -2, -1)%></strong></font></td>
        </tr>
        <tr> 
          <td height="35" colspan="6" bgcolor="#FFFFFF"><a href="<%=Request.ServerVariables("HTTP_REFERER")%>"><img src="/images/continue-shopping-red.gif" width="129" height="18" align="absmiddle" border="0"></a><img src="/images/transparent.gif" width="280" height="8"><img src="/images/continue-checkout-red.gif" width="174" height="25" align="absmiddle"></td>
        </tr>
      </table>
      <div align="center"><br>
        <table width="590" border="0" cellspacing="0" cellpadding="0">
          <tr valign="top"> 
            <td align="center"><img src="/images/phone_order.gif" width="151" height="51" hspace="5" vspace="5"></td>
            <td ><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Place 
              your order by calling Sequins Plus direct.<br>
              Use the Catalogue #'s and information above and place your order 
              without having to create an account.</font></td>
          </tr>
        </table>
      </div>
      <table width="590" border="0" cellpadding="5" valign="top" cellspacing="1" bgcolor="#6A6A6A">
        <tr valign="top" bgcolor="#FFFFFF"> 
          <td colspan="2"><font color="#CC1C0F" size="3" face="Arial, Helvetica, sans-serif"><strong>YOUR 
            SITE SERVICES</strong></font></td>
        </tr>
        <tr> 
          <td valign="top" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong>My 
            Order</strong></font><br> <font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&#149; 
            Track your <u>Recent Orders</u>.<br>
            &#149; View or change your orders in <u><a href="/en/order/default.asp">My 
            Account</a></u>.<br>
            &#149; See our <u>Shipping Rates and Policies</u>.<br>
            </font></td>
          <td valign="top" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong>Help</strong></font><br> 
            <font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&#149; 
            Forgot your password? <u><a href="/en/order/forgot.asp">Click Here</a></u><a href="/en/order/forgot.asp">.</a><br>
            &#149; Visit the online <u>Help</u> section.<br>
            &#149; <u>Return</u> an item.<br>
            </font></td>
        </tr>
      </table></td>
    <td width="10" valign="top" ></td>
  </tr>
  <tr valign="top"> 
    <td width="8" height="2" ></td>
    <td width="2"  bgcolor="#FFFFFF"></td>
    <td width="6"  bgcolor="#FFFFFF"></td>
    <td width="10" ></td>
  </tr>
  <tr> 
    <td width="8" height="1"></td>
    <td width="12" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="2" height="1" bgcolor="#FFFFFF"></td>
    <td width="6" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="8" height="1" bgcolor="#FFFFFF"></td>
    <td width="126" height="1" bgcolor="#FFFFFF"></td>
    <td width="18" height="1" bgcolor="#FFFFFF"></td>
    <td width="8" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="8" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="13" height="1" bgcolor="#FFFFFF"></td>
    <td width="10" height="1"></td>
  </tr>
  <tr> 
    <td width="8" height="14"></td>
    <td height="14" colspan="13" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr> 
    <td width="8" height="49"></td>
    <td height="49" colspan="13" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr> 
    <td width="8" height="7"></td>
    <td height="7" colspan="13" valign="top"><IMG SRC="/images/page-bottom.gif" WIDTH=762 HEIGHT=7></td>
    <td width="10" height="7"></td>
  </tr>
  <tr> 
    <td height="10" colspan="15"></td>
  </tr>
  <tr> 
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="12" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="12" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="2" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="2" height="1"></td>
    <td width="6" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="6" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="126" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="126" height="1"></td>
    <td width="18" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="15" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="13" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="13" height="1"></td>
    <td width="10" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="10" height="1"></td>
  </tr>
</table>
</BODY>
</HTML>