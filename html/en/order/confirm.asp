<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%><%' *** Restrict Access To Page: Grant or deny access to this page
MM_authorizedUsers=""
MM_authFailedURL="/en/order/login.asp"
MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If
If Not MM_grantAccess Then
  MM_qsChar = "?"
  If (InStr(1,MM_authFailedURL,"?") >= 1) Then MM_qsChar = "&"
  MM_referrer = Request.ServerVariables("URL")
  if (Len(Request.QueryString()) > 0) Then MM_referrer = MM_referrer & "?" & Request.QueryString()
  MM_authFailedURL = MM_authFailedURL & MM_qsChar & "accessdenied=" & Server.URLEncode(MM_referrer)
  Response.Redirect(MM_authFailedURL)
End If
%>
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" --><%
UniqKey = ""
Randomize
For i = 1 To 16
num = Int((90 - 65 + 1) * Rnd + 65)
UniqKey = UniqKey & Chr(num)
Next
badform = true
' top
if request("Cancel") = "Yes" AND session("OrderKey") <> "" then 
key = session("OrderKey")
orderID = session("OrderID")
orderName = session("OrderName")
Set spDBdelete = Server.CreateObject("ADODB.Recordset")
spDBdelete.ActiveConnection = SPdbString
spDBdelete.Source = "SELECT * FROM spOrders WHERE spOrders.Key='" & key &"'" 
spDBdelete.CursorType = 0
spDBdelete.CursorLocation = 2
spDBdelete.LockType = 3
spDBdelete.Open()
while NOT (spDBdelete.EOF OR spDBdelete.BOF)
spDBdelete.Delete
spDBdelete.MoveNext
wend
spDBdelete.Close()
Set spDBdelete = Nothing
Set spDBdelete = Server.CreateObject("ADODB.Recordset")
spDBdelete.ActiveConnection = SPdbString
spDBdelete.Source = "SELECT * FROM spOrderDetails WHERE spOrderDetails.OrderID = " & orderID 
spDBdelete.CursorType = 0
spDBdelete.CursorLocation = 2
spDBdelete.LockType = 3
spDBdelete.Open()
while NOT (spDBdelete.EOF OR spDBdelete.BOF)
spDBdelete.Delete()
spDBdelete.MoveNext
wend
spDBdelete.Close()
Set spDBdelete = Nothing
response.Redirect("/en/order/cart.asp")
end if
if request("Cancel") = "Yes" AND session("OrderKey") = "" then 
response.Redirect("/en/order/cart.asp")
end if
'bottom
if request("STATUS") = "submit" AND trim(request("Name")) <> "" then 
badform = false 
end if
if badform = false  then
Set spOnum = Server.CreateObject("ADODB.Recordset")
spOnum.ActiveConnection = SPdbString
spOnum.Source = "SELECT * FROM spOrders ORDER BY OrderNumber desc"
spOnum.CursorType = 0
spOnum.CursorLocation = 2
spOnum.LockType = 3
spOnum.Open()
OrderNumber = spOnum.Fields.Item("OrderNumber").Value + 1
spOnum.Close()
Set spOrderadd = Server.CreateObject("ADODB.Recordset")
spOrderadd.ActiveConnection = SPdbString
spOrderadd.Source = "SELECT * FROM spOrders ORDER BY id desc"
spOrderadd.CursorType = 0
spOrderadd.CursorLocation = 2
spOrderadd.LockType = 3
spOrderadd.Open()
spOrderadd.AddNew
spOrderadd("SPuser")= Session("MM_UserID")
spOrderadd("Name")= request("Name")
spOrderadd("OrderNumber")= OrderNumber
spOrderadd("orderDate") = FormatDateTime(Date)
spOrderadd("IP") = Request.serverVariables("REMOTE_HOST")
spOrderadd("DateTxT")=  FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y") & " @ " & FormatDateTime(Now, 3)
if request("special") = "Type your message or special request here." then
spOrderadd("Special")= ""
else
spOrderadd("Special")= request("special")
end if
spOrderadd("Key")= UniqKey
if request("Pickup") = "True" then
spOrderadd("ShippingAmmount")= 0
else
spOrderadd("ShippingAmmount")= 25
end if
spOrderadd("SubTotal")= "$$Call For Price$$"
spOrderadd.update
spOrderadd.Close()
Set spOrderadd = Nothing
session("OrderKey") = UniqKey
session("OrderName") = request("Name")
Set RSorder = Server.CreateObject("ADODB.Recordset")
RSorder.ActiveConnection = SPdbString
RSorder.Source = "SELECT * FROM spOrders Where spOrders.Key = '" & session("OrderKey") & "'"
RSorder.CursorType = 0
RSorder.CursorLocation = 2
RSorder.LockType = 3
RSorder.Open()
OrderId = RSorder.Fields.Item("id").Value
session("OrderID") = RSorder.Fields.Item("id").Value
RSorder.close()
Set RSorder = Nothing
For CartItems=0 To UCCart1.GetItemCount()-1
Set spOrderDetails = Server.CreateObject("ADODB.Recordset")
spOrderDetails.ActiveConnection = SPdbString
spOrderDetails.Source = "SELECT * FROM spOrderDetails"
spOrderDetails.CursorType = 0
spOrderDetails.CursorLocation = 2
spOrderDetails.LockType = 3
spOrderDetails.Open()
spOrderDetails.AddNew
spOrderDetails("OrderID")=OrderId 
spOrderDetails("ItemName")= (UCCart1.GetColumnValue("Name",CartItems))
spOrderDetails("ItemProductCode")= (UCCart1.GetColumnValue("ProductCode",CartItems))
spOrderDetails("ItemID")= (UCCart1.GetColumnValue("ProductID",CartItems))
spOrderDetails("ItemColour")= (UCCart1.GetColumnValue("Colour",CartItems)) 
spOrderDetails("ItemSize")= (UCCart1.GetColumnValue("Size",CartItems))
spOrderDetails("ItemSizeType")= (UCCart1.GetColumnValue("SizeType",CartItems))
spOrderDetails("ItemQuantity")= (UCCart1.GetColumnValue("Quantity",CartItems))
spOrderDetails("ItemPrice")= (UCCart1.GetColumnValue("Price",CartItems))
spOrderDetails("ItemTotal")= (UCCart1.GetColumnValue("Total",CartItems))
spOrderDetails.update
spOrderDetails.Close()
Set spOrderDetails = Nothing
next
DBsaved = "added"
end if
if DBsaved = "added" AND badform = false  then
if request("Pickup")  = "True" then
response.Redirect("/en/order/pickup.asp")
else
response.Redirect("/en/order/shipping.asp")
end if
end if
%><HTML>
<HEAD>
<TITLE>Sequins Plus Online - Confirm Order - Step One</TITLE>
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<script language="JavaScript">
<!--
function Cancel() {
var Sform=document.Confirm; Sform.Cancel.value = "Yes"
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR=#DFDFDF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="MM_preloadImages('/images/cartbut_1_over.gif','/images/cartbut_2_over.gif','/images/cartbut_3_over.gif','/images/cartbut_5_over.gif','/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif','/images/cartbut_7_over.gif','/images/cartbut_6_over.gif')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="4"></td>
    <td width="281" height="28" colspan="4" valign="top"><IMG SRC="/images/logo_top.gif" WIDTH=281 HEIGHT=28></td>
    <td width="336" height="28" colspan="7"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="3" valign="top"><IMG SRC="/images/logo_lft_top_line.gif" WIDTH=155 HEIGHT=9></td>
    <td width="281" height="9" colspan="4" valign="top"><IMG SRC="/images/logo_topline.gif" WIDTH=281 HEIGHT=9></td>
    <td width="326" height="9" colspan="6" valign="top"><IMG SRC="/images/logo_rt_top_line.gif" WIDTH=326 HEIGHT=9></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
     <td height="26" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" colspan="4" valign="top"><IMG SRC="/images/logo_title.gif" WIDTH=281 HEIGHT=26></td>
    <td width="326" height="26" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="12"></td>
    <td width="155" height="12" colspan="3" valign="top"><IMG SRC="/images/logo_lft_bot_line.gif" WIDTH=155 HEIGHT=11></td>
    <td width="281" height="12" colspan="4" valign="top"><IMG SRC="/images/logo_bot-line.gif" WIDTH=281 HEIGHT=11></td>
    <td width="326" height="12" colspan="6" valign="top"><IMG SRC="/images/logo_rt_bot_line.gif" WIDTH=326 HEIGHT=11></td>
    <td width="10" height="12"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="155" height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="281" height="14" colspan="4" valign="top"><IMG SRC="/images/logo_beige.gif" WIDTH=281 HEIGHT=14></td>
    <td width="326" height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" colspan="4" valign="top"><IMG SRC="/images/logo_spacer.gif" WIDTH=281 HEIGHT=2></td>
    <td width="326" height="2" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="19"></td>
    <td colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="19" colspan="4" valign="top"><IMG SRC="/images/cartpage_22.gif" WIDTH=281 HEIGHT=19></td>
    <td width="326" height="19" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="19"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="762" height="5" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="20"></td>
    <td width="12" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut1','','/images/cartbut_1_over.gif',1)"><img src="/images/cartbut_1.gif" alt="SequinsPlus Homepage" name="cartbut1" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/orders.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut2','','/images/cartbut_2_over.gif',1)"><img src="/images/cartbut_2.gif" alt="Check My Orders Page !" name="cartbut2" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" colspan="2" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('info','','/images/cartbut_6_over.gif',1)"><img src="/images/cartbut_6.gif" alt="Account Settings Page!" name="info" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="logout.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut4','','/images/cartbut_7_over.gif',1)"><img src="/images/cartbut_7.gif" alt="Sign Out!" name="cartbut4" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut5','','/images/cartbut_5_over.gif',1)"><img src="/images/cartbut_5.gif" alt="My Account Page!" name="cartbut5" width="141" height="20" border="0"></a></td>
    <td width="13" height="20" bgcolor="#FFFFFF"></td>
    <td width="10" height="20"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="167"></td>
    <td width="762" height="167" colspan="13" align="center" valign="top" bgcolor="#FFFFFF"> 
      <table width="700" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
        <tr> 
      <td colspan="7"><img src="/images/topbar.gif" width="700" height="9"></td>
    </tr>
    <tr> 
      <td colspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="25" height="1"><font color="#FFFFFF" size="4" face="Arial, Helvetica, sans-serif">&#8226; 
        Cart Checkout - <font size="3"><strong>Step 1 (1 of 3) Order Confirmation</strong></font></font></td>
    </tr>
    <tr> 
      <td colspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td width="1" rowspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
      <td colspan="5"><img src="/images/transparent.gif" width="1" height="10"></td>
      <td width="1" rowspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td width="10"><img src="/images/transparent.gif" width="10" height="8"></td>
      <td colspan="3"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">You 
        have choosen to order the items listed below :</font></td>
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
    </tr>
    <tr> 
      <td height="5"><img src="/images/transparent.gif" width="1" height="5"></td>
          <td height="5" colspan="3"><img src="/images/transparent.gif" width="1" height="5"></td>
      <td height="5"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td width="17" valign="top"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">(i)<br>
          (ii)<br><br><br>
          (iii)</font></td>
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
      <td width="651"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
          Please note that all prices are in Canadian dollars.<br>
          Shipping is an approximate value. After you place your order a Sequins 
          Plus representative will contact you to verify payment and arrange shipping 
          method. If you would like, you can pick up your order at one of our 
          store locations.<br>
          Verify that your order is correct and then click the 'Continue Checkout' 
          button.</font></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="5"><img src="/images/transparent.gif" width="1" height="20"></td>
    </tr>
    <tr> 
      <td height="2"><img src="/images/transparent.gif" width="1" height="2"></td>
      <td height="2" colspan="3" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="2"></td>
      <td height="2"><img src="/images/transparent.gif" width="1" height="2"></td>
    </tr>
    <tr> 
      <td height="5" colspan="5"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
  </table>
    </td>
    <td width="10" height="167"></td>
  </tr>
  <tr>
    <td width="8" height="423"></td>
    <td width="762" height="423" colspan="13" align="center" valign="top" bgcolor="#FFFFFF"> 
      <form action="/en/order/confirm.asp" method="post" name="Confirm"><input name="Cancel" type="hidden" value="No"><input name="STATUS" type="hidden" value="submit"><table width="700" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
    <tr> 
      <td rowspan="6" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
      <td><img src="/images/transparent.gif" width="10" height="1"></td>
      <td><table width="678" border="0" cellspacing="1" cellpadding="0">
              <tr> 
                <td colspan="2" align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Product 
                  Description</strong></font></td>
                <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Size 
                  Type</strong></font></td>
                <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Size</strong></font></td>
                <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Colour</strong></font></td>
                <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Price</strong></font></td>
                <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Qty</strong></font></td>
                <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Total</strong></font></td>
              </tr>
              <tr> 
                <td colspan="8"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr><% For UCCart1__i=0 To UCCart1.GetItemCount()-1 %>
              <tr> 
                <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=(UCCart1.GetColumnValue("Name",UCCart1__i))%></font></td>
                <td align="center"><div align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
                      #: <%=(UCCart1.GetColumnValue("ProductCode",UCCart1__i))%></font></div></td>
                <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=(UCCart1.GetColumnValue("SizeType",UCCart1__i))%></font></td>
                <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=(UCCart1.GetColumnValue("Size",UCCart1__i))%></font></td>
                <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=(UCCart1.GetColumnValue("Colour",UCCart1__i))%></font></td>
                <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000"><%= "Call Now!"%></font></font></td>
                <td align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=(UCCart1.GetColumnValue("Quantity",UCCart1__i))%></font></td>
                <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F"><%= "Call Now!"%></font></strong></font></td>
              </tr><%next%>
              <tr> 
                <td colspan="8"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td colspan="6" align="right" bgcolor="#ececec"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Subtotal 
                  :</strong></font></td>
                <td colspan="2" align="right" bgcolor="#ececec"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong><%= "Call (905) 433-2319" %></strong></font></td>
              </tr>
              <tr> 
                <td colspan="6" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">**</font> 
                  Shipping :</strong></font></td>
                <td colspan="2" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>$$$</strong></font></td>
              </tr>
              <tr> 
                <td colspan="6" align="right" bgcolor="#ececec"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Taxes 
                  :</strong></font></td>
                <% ' ---- tax calc ----

Dim vTax
Dim vTaxTotal
Dim vTaxRate

vTax = 0.00
vTaxRate = 0.15

vTaxTotal = (1.0 + vTaxRate) * (UCCart1.GetColumnTotal("Total"))
vTax = vTaxTotal - UCCart1.GetColumnTotal("Total")
%>
                <td colspan="2" align="right" bgcolor="#ececec"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><%= "$$$"%></strong></font></td>
              </tr>
              <tr> 
                <td height="5" colspan="8"><img src="/images/transparent.gif" width="1" height="5"></td>
              </tr>
              <tr> 
                <td height="2" colspan="8" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="2"></td>
              </tr>
              <tr> 
                <td height="5" colspan="8"><img src="/images/transparent.gif" width="1" height="5"></td>
              </tr>
              <tr> 
                <td colspan="6" align="right"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>Total 
                  :</strong></font></td>
                <td colspan="2" align="right"><font color="#CC1C0F" size="3" face="Arial, Helvetica, sans-serif"><strong><%="Call For Price"%></strong></font></td>
              </tr>
            </table></td>
      <td><img src="/images/transparent.gif" width="10" height="1"></td>
      <td rowspan="6" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="1"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td height="20"><img src="/images/transparent.gif" width="1" height="20"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><table width="678" border="0" cellpadding="5" cellspacing="1" bgcolor="#CECECE">
              <tr bgcolor="#ececec"> 
                <td><b><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">**</font> 
                  SHIPPING <font color="#CC1C0F">**</font></strong></font></b><img src="/images/transparent.gif" width="10" height="1"> 
                  <input name="Pickup" type="checkbox" id="Pickup" value="True"> <img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Check 
                  this box if you would like to pick up your order at one of our 
                  store locations.</font></td>
              <tr bgcolor="#ececec"> 
                <td align="center"> <textarea name="Special" cols="60" rows="5" id="Special">Type your message or special request here.</textarea> 
              <tr bgcolor="#ececec">
                <td align="center"><b><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>ORDER NAME :</strong></font></b><img src="/images/transparent.gif" width="10" height="1">
                  <input name="Name" type="text" id="Name" maxlength="25"><img src="/images/transparent.gif" width="5" height="1"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">(Name your order here for easy reference in the My Orders Page)</font></table></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><img src="/images/transparent.gif" width="1" height="20"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><img src="/images/transparent.gif" width="90" height="1"><input name="Ccelbtn" Onclick="Cancel()" type="image" src="/images/cancel-order.gif" alt="Cancel this order and continue shopping" width="112" height="18" border="0" align="middle"><img src="/images/transparent.gif" width="212" height="1"><input name="Continue" type="image" src="/images/continue-checkout-red.gif" alt="Continue Checkout" align="middle" width="174" height="25" border="0"><img src="/images/transparent.gif" width="90" height="1"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><img src="/images/transparent.gif" width="1" height="20"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="5" bgcolor="#CC1C0F"><img src="/images/bottombar.gif" width="700" height="9"></td>
    </tr>
  </table></form>
    </td>
    <td width="10" height="423"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="13" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td width="762" height="49" colspan="13" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="13" valign="top"><IMG SRC="/images/page-bottom.gif" WIDTH=762 HEIGHT=7></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="15"></td>
  </tr>
  <tr>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="12" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="12" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="2" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="2" height="1"></td>
    <td width="6" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="6" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="126" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="126" height="1"></td>
    <td width="15" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="15" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="13" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="13" height="1"></td>
    <td width="10" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="10" height="1"></td>
  </tr>
</table>
</BODY>
</HTML>