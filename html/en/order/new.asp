<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" --><%MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%>
<%
Function ValidateUserName(User,Conn)
Dim recEx__MMColParam, recEx
recEx__MMColParam = User

set recEx = Server.CreateObject("ADODB.Recordset")
recEx.ActiveConnection = Conn
recEx.Source = "SELECT * FROM spCartUser WHERE Login LIKE '" + Replace(recEx__MMColParam, "'", "''") + "'"
recEx.CursorType = 0
recEx.CursorLocation = 2
recEx.LockType = 3
recEx.Open()
recEx_numRows = 0
If Not recEx.EOF AND Not recEx.BOF Then
	ValidateUserName = true
Else
	ValidateUserName = false
End If
recEx.Close()
End Function
%><%
set spCountries = Server.CreateObject("ADODB.Recordset")
spCountries.ActiveConnection = SPdbString
spCountries.Source = "SELECT * FROM Countries"
spCountries.CursorType = 0
spCountries.CursorLocation = 2
spCountries.LockType = 3
spCountries.Open()
spCountries_numRows = 0
%>
<%
Function ValidateEmail(email)
   dim atCnt
   ValidateEmail = false

   if len(cstr(email)) < 7 then 
      ValidateEmail = true

   elseif instr(email,"@") = 0 then
      ValidateEmail = true

   elseif instr(email,".") = 0 then
      ValidateEmail = true

   elseif len(email) - instrrev(email,".") > 3 then
      ValidateEmail = true

   elseif instr(email,"_") <> 0 and _
      instrrev(email,"_") > instrrev(email,"@") then
      ValidateEmail = true

   else
   atCnt = 0
      for i = 1 to len(email)
         if mid(email,i,1) = "@" then
            atCnt = atCnt + 1
         end if
      next

   if atCnt > 1 then
      ValidateEmail = true
   end if

   for i = 1 to len(email)
      if not isnumeric(mid(email,i,1)) and _
         (lcase(mid(email,i,1)) < "a" or _
         lcase(mid(email,i,1)) > "z") and _
         mid(email,i,1) <> "_" and _
         mid(email,i,1) <> "." and _
         mid(email,i,1) <> "@" and _
         mid(email,i,1) <> "-" then
         ValidateEmail = true
      end if
   next
   end if
End Function
%>
<%
Set RSquickcat = Server.CreateObject("ADODB.Recordset")
RSquickcat.ActiveConnection = SPdbString
RSquickcat.Source = "SELECT spItemCatagory.id, spItemCatagory.Name FROM spItemCatagory ORDER BY spItemCatagory.Name ASC"
RSquickcat.CursorType = 0
RSquickcat.CursorLocation = 2
RSquickcat.LockType = 3
RSquickcat.Open()

Dim cUserName : cUserName = Request.Form("UserName")
Dim cPassword : cPassword = Request.Form("Password")
Dim cPasswordConfirm : cPasswordConfirm = Request.Form("PasswordConfirm")
Dim cFirstName : cFirstName = Request.Form("FirstName")
Dim cInitial : cInitial = Request.Form("Initial")
Dim cLastName : cLastName = Request.Form("LastName")
Dim cEmail : cEmail = Request.Form("Email")
Dim cCountry : cCountry = Request.Form("Country")
Dim cPostal : cPostal = Request.Form("Postal")
Dim cProvince : cProvince = Request.Form("Province")
Dim cCity : cCity = Request.Form("City")
Dim cAddress1 : cAddress1 = Request.Form("Address1")
Dim cAddress2 : cAddress2 = Request.Form("Address2")
Dim cPhone3 : cPhone3 = Request.Form("Phone3")
Dim cPhone4 : cPhone4 = Request.Form("Phone4")
Dim cPhoneAC : cPhoneAC = Request.Form("PhoneAC")
Dim cPhoneEXT : cPhoneEXT = Request.Form("PhoneEXT")
Dim cAppartment : cAppartment = Request.Form("Appartment")
' ------------ validate form ------------
bFormSubmitted = false
if Request.Form("MM_insert") <> "" then
bFormSubmitted = true
bFormBad = false
	bBad01 = false : If Len(cFirstName) = 0 Then bBad01 = true
	bBad02 = false : If Len(cLastName) = 0 Then bBad02 = true
	bBad03 = false : If Len(cUserName) = 0 Then bBad03 = true

	bBad04 = false : If Len(cEmail) = 0 Then bBad04 = true
	bBad05 = false : if ValidateEmail(cEmail) then bBad05 = true
	bBad06 = false : if Len(cPassword) < 7 Then bBad06 = true
	bBad07 = false : if not (Trim(cPasswordConfirm) = trim(cPassword)) Then bBad07 = true
	bBad08 = false : if Len(cAddress1) = 0 Then bBad08 = true
	bBad09 = false : if Len(cCity) = 0 Then bBad09 = true
	bBad10 = false : if Len(cProvince) = 0 Then bBad10 = true
	bBad11 = false : if Len(cPostal) = 0 Then bBad11 = true
	bBad12 = false : if ValidateUserName(cUserName,SPdbString) then bBad12 = true
	bBad13 = false : if Len(cPhoneAC) = 0 Then bBad13 = true
	bBad14 = false : if Len(cPhone3) = 0 Then bBad14 = true
	bBad15 = false : if Len(cPhone4) = 0 Then bBad15 = true
	If  ( bBad01 OR bBad02 OR bBad03 OR bBad04 OR bBad05 OR bBad06 OR bBad07 OR bBad08 OR bBad09 OR bBad10  OR bBad11 OR bBad12 OR bBad13 OR bBad14 OR bBad15 )  AND _
		( Request.Form("MM_insert") <> "" ) Then bFormBad = true
end if
%>
<%
' *** Edit Operations: declare variables
if bFormSubmitted AND NOT (bFormBad) then
Dim MM_editAction
Dim MM_abortEdit
Dim MM_editQuery
Dim MM_editCmd

Dim MM_editConnection
Dim MM_editTable
Dim MM_editRedirectUrl
Dim MM_editColumn
Dim MM_recordId

Dim MM_fieldsStr
Dim MM_columnsStr
Dim MM_fields
Dim MM_columns
Dim MM_typeArray
Dim MM_formVal
Dim MM_delim
Dim MM_altVal
Dim MM_emptyVal
Dim MM_i

MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Request.QueryString
End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: set variables

If (CStr(Request("MM_insert")) = "NewCartUser") Then

  MM_editConnection = SPdbString
  MM_editTable = "spCartUser"
  MM_editRedirectUrl = "/en/order/default.asp"
  MM_fieldsStr  = "FirstName|value|Initial|value|LastName|value|UserName|value|Password|value|Address1|value|Appartment|value|Address2|value|City|value|Province|value|Postal|value|Country|value|Company|value|phoneAC|value|phone3|value|phone4|value|phoneext|value|Email|value"
  MM_columnsStr = "FirstName|',none,''|Initial|',none,''|LastName|',none,''|Login|',none,''|Password|',none,''|Address1|',none,''|Apt|',none,''|Address2|',none,''|City|',none,''|Province|',none,''|Postal|',none,''|Country|',none,''|Company|',none,''|PhoneAC|none,none,NULL|Phone3|none,none,NULL|Phone4|none,none,NULL|Ext|none,none,NULL|Email|',none,''"

  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  
  ' set the form values
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(MM_i+1) = CStr(Request.Form(MM_fields(MM_i)))
  Next

  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If

End If
%>
<%
' *** Insert Record: construct a sql insert statement and execute it

Dim MM_tableValues
Dim MM_dbValues

If (CStr(Request("MM_insert")) <> "") Then

  ' create the sql insert statement
  MM_tableValues = ""
  MM_dbValues = ""
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_formVal = MM_fields(MM_i+1)
    MM_typeArray = Split(MM_columns(MM_i+1),",")
    MM_delim = MM_typeArray(0)
    If (MM_delim = "none") Then MM_delim = ""
    MM_altVal = MM_typeArray(1)
    If (MM_altVal = "none") Then MM_altVal = ""
    MM_emptyVal = MM_typeArray(2)
    If (MM_emptyVal = "none") Then MM_emptyVal = ""
    If (MM_formVal = "") Then
      MM_formVal = MM_emptyVal
    Else
      If (MM_altVal <> "") Then
        MM_formVal = MM_altVal
      ElseIf (MM_delim = "'") Then  ' escape quotes
        MM_formVal = "'" & Replace(MM_formVal,"'","''") & "'"
      Else
        MM_formVal = MM_delim + MM_formVal + MM_delim
      End If
    End If
    If (MM_i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End If
    MM_tableValues = MM_tableValues & MM_columns(MM_i)
    MM_dbValues = MM_dbValues & MM_formVal
  Next
  MM_editQuery = "insert into " & MM_editTable & " (" & MM_tableValues & ") values (" & MM_dbValues & ")"

  If (Not MM_abortEdit) Then
    ' execute the insert
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_editConnection
    MM_editCmd.CommandText = MM_editQuery
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close
'
' *** Validate request to log in to this site.
MM_valUsername=cUserName
If MM_valUsername <> "" Then
  MM_fldUserAuthorization=""
  MM_flag="ADODB.Recordset"
  set MM_rsUser = Server.CreateObject(MM_flag)
  MM_rsUser.ActiveConnection = SPdbString
  MM_rsUser.Source = "SELECT * "
  If MM_fldUserAuthorization <> "" Then MM_rsUser.Source = MM_rsUser.Source & "," & MM_fldUserAuthorization
  MM_rsUser.Source = MM_rsUser.Source & " FROM spCartUser WHERE Login='" & MM_valUsername &"' AND Password='" & cPassword & "'"
  MM_rsUser.CursorType = 0
  MM_rsUser.CursorLocation = 2
  MM_rsUser.LockType = 3
  MM_rsUser.Open
  If Not MM_rsUser.EOF Or Not MM_rsUser.BOF Then 
    ' username and password match - this is a valid user
    Session("MM_SPshopping") = MM_valUsername
    Session("MM_UserID") = CStr(MM_rsUser.Fields.Item("ID_BUYER").Value)
   
  End If
  MM_rsUser.Close
End If
'
    If (MM_editRedirectUrl <> "") Then
      Response.Redirect(MM_editRedirectUrl)
    End If
  End If

End If
end if
%>
<HTML>
<HEAD>
<TITLE>Sequins Plus Online - Shopping Cart New Account</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR=#DFDFDF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="MM_preloadImages('/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif','/images/cartbut_1_over.gif','/images/cartbut_2_over.gif','/images/cartbut_6_over.gif','/images/cartbut_4_over.gif','/images/cartbut_5_over.gif')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="4"></td>
    <td width="281" height="28" colspan="4" valign="top"><IMG SRC="/images/logo_top.gif" WIDTH=281 HEIGHT=28></td>
    <td width="336" height="28" colspan="7"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="3" valign="top"><IMG SRC="/images/logo_lft_top_line.gif" WIDTH=155 HEIGHT=9></td>
    <td width="281" height="9" colspan="4" valign="top"><IMG SRC="/images/logo_topline.gif" WIDTH=281 HEIGHT=9></td>
    <td width="326" height="9" colspan="6" valign="top"><IMG SRC="/images/logo_rt_top_line.gif" WIDTH=326 HEIGHT=9></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
    <td width="155" height="26" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td
    ><td width="281" height="26" colspan="4" valign="top"><IMG SRC="/images/logo_title.gif" WIDTH=281 HEIGHT=26></td>
    <td width="326" height="26" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td width="155" height="11" colspan="3" valign="top"><IMG SRC="/images/logo_lft_bot_line.gif" WIDTH=155 HEIGHT=11></td>
    <td width="281" height="11" colspan="4" valign="top"><IMG SRC="/images/logo_bot-line.gif" WIDTH=281 HEIGHT=11></td>
    <td width="326" height="11" colspan="6" valign="top"><IMG SRC="/images/logo_rt_bot_line.gif" WIDTH=326 HEIGHT=11></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="155" height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="281" height="14" colspan="4" valign="top"><IMG SRC="/images/logo_beige.gif" WIDTH=281 HEIGHT=14></td>
    <td width="326" height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" colspan="4" valign="top"><IMG SRC="/images/logo_spacer.gif" WIDTH=281 HEIGHT=2></td>
    <td width="326" height="2" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="19"></td>
    <td width="155" height="19" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="19" colspan="4" valign="top"><IMG SRC="/images/cartpage_22.gif" WIDTH=281 HEIGHT=19></td>
    <td width="326" height="19" colspan="6" valign="top" bgcolor="#FFFFFF"><img src="/images/red_arrow.gif" width="17" height="17" align="absmiddle"><img src="/images/transparent.gif" width="5" height="1"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong>Shopping 
      Cart Items:</strong></font> <%=UCCart1.GetItemCount()%> <strong><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Subtotal:</font></strong> 
      $<%=FormatNumber(UCCart1.GetColumnTotal("Total"), 2, -2, -2, -1)%></td>
    <td width="10" height="19"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="762" height="5" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="20"></td>
    <td width="12" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('homepage','','/images/cartbut_1_over.gif',1)"><img src="/images/cartbut_1.gif" alt="Homepage" name="homepage" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('status','','/images/cartbut_2_over.gif',1)"><img src="/images/cartbut_2.gif" alt="Check Your Order Status" name="status" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" colspan="2" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('info','','/images/cartbut_6_over.gif',1)"><img src="/images/cartbut_6.gif" alt="Account Information" name="info" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/login.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('sign in','','/images/cartbut_4_over.gif',1)"><img src="/images/cartbut_4.gif" alt="Sign In To Your Account" name="sign in" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('myaccount','','/images/cartbut_5_over.gif',1)"><img src="/images/cartbut_5.gif" alt="Account Services" name="myaccount" width="141" height="20" border="0"></a></td>
    <td width="13" height="20" bgcolor="#FFFFFF"></td>
    <td width="10" height="20"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="916"></td>
    <td width="153" height="924" colspan="2" rowspan="2" valign="top" bgcolor="#FFFFFF"> 
  <table width="153" border="0" cellpadding="0" cellspacing="0" background="/images/list_border.gif" mm_noconvert="TRUE">
    <tr> 
          <td valign="bottom"><img src="/images/nav_top.gif" width="153" height="14"></td>
    </tr>
    <tr> 
      <td height="2" valign="bottom"><img src="/images/transparent.gif" width="2" height="2"></td>
    </tr>
    <tr> 
      <td height="50" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="50"></td>
    </tr>
    <tr> 
      <td align="center" bgcolor="#CC1C0F"><img src="/images/create_1.jpg" width="82" height="66"></td>
    </tr>
    <tr> 
      <td height="55" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="55"></td>
    </tr>
    <tr> 
          <td align="center" bgcolor="#CC1C0F"><img src="/images/create_2.jpg" width="79" height="66"></td>
    </tr>
    <tr> 
      <td height="55" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="55"></td>
    </tr>
    <tr> 
      <td align="center" bgcolor="#CC1C0F"><img src="/images/create_3.jpg" width="81" height="66"></td>
    </tr>
    <tr> 
      <td height="55" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="55"></td>
    </tr>
    <tr> 
      <td align="center" bgcolor="#CC1C0F"><img src="/images/create.jpg" width="81" height="66"></td>
    </tr>
    <tr> 
      <td height="55" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="55"></td>
    </tr>
    <tr> 
      <td align="center" bgcolor="#CC1C0F"><img src="/images/create_4.jpg" width="82" height="66"></td>
    </tr>
    <tr> 
      <td height="55" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="55"></td>
    </tr>
    <tr> 
      <td align="center" bgcolor="#CC1C0F"><img src="/images/create_5.jpg" width="79" height="66"></td>
    </tr>
    <tr> 
      <td height="55" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="55"></td>
    </tr>
    <tr> 
      <td align="center" bgcolor="#CC1C0F"><img src="/images/create_6.jpg" width="81" height="66"></td>
    </tr>
    <tr> 
      <td height="50" align="center" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="50"></td>
    </tr>
    <tr>
      <td height="2" valign="top"><img src="/images/transparent.gif" width="10" height="2"></td>
    </tr>
    <tr> 
      <td height="14" valign="top"><img src="/images/list_bottom.gif" width="153" height="14"></td>
    </tr>
  </table>
</td>
    <td width="2" height="916" bgcolor="#FFFFFF"></td>
    <td width="6" height="916" bgcolor="#FFFFFF"></td>
    <td width="601" height="916" colspan="9" valign="top" bgcolor="#FFFFFF"> 
  <table width="601" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
    <tr> 
      <td height="10" colspan="3" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="2" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="1"><font color="#FFFFFF" size="5" face="Arial, Helvetica, sans-serif">&#8226; 
        Create A New Account</font></td>
          <form name="jumpmenu" onSubmit="return jump(document.jumpmenu.menu.options[document.jumpmenu.menu.options.selectedIndex].value)"><td width="256" align="right" valign="top" bgcolor="#CC1C0F"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font><select align="middle" name="menu" >
                <option value="none" selected>More Categories</option><% While (NOT RSquickcat.EOF) %>
          <option value="<%="/en/products/default.asp?Cat="&(RSquickcat.Fields.Item("id").Value)%>"  ><%=(RSquickcat.Fields.Item("Name").Value)%></option>
          <% 
		  RSquickcat.MoveNext()
		  Wend
		  RSquickcat.close()
		  %></select>
              <input name="button" type="image" src="/images/btn_go.jpg" alt="Search" align="middle" width="27" height="27" hspace="0" vspace="0" border="0">
      </td></form>
    </tr>
    <tr> 
      <td height="10" colspan="3" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td colspan="3"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Thanks 
        for choosing Sequins Plus.<br>
        To register for your secure online shopping cart, fill out and submit 
        the form below.<br>
        <br>
            <font color="#FF0000">*</font><font color="#000000" size="2"> Indicates 
            required feilds</font></font></td>
    </tr>
    <tr> 
      <td height="20" colspan="3"><img src="/images/transparent.gif" width="1" height="20"></td>
    </tr><form method="POST" action="<%=MM_editAction%>" name="NewCartUser" id="NewCartUser">
    <tr> 
      <td height="20" colspan="3"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>Personal 
        Information: </strong></font></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td colspan="2"></td>
    </tr>
    <tr> 
      <td width="182" height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>First 
        Name : <font color="#FF0000">*</font></strong></font> </td>
          <td width="163" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="FirstName" type="text" id="FirstName" value="<%=cFirstName%>"></td>
            <td height="20" align="left">
              <% If bFormBad AND bBad01 Then %>
              <font color="#CC0000">Can't be blank </font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Initial 
        : <img src="/images/transparent.gif" width="5" height="1"></strong></font> </td>
          <td height="20" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Initial" type="text" id="Initial" value="<%=cinitial%>" size="2"> 
          </td>
      <td height="20">&nbsp;</td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Last 
        Name : <font color="#FF0000">*</font> </strong></font> </td>
          <td height="20" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="LastName" type="text" id="LastName" value="<%=cLastName%>"> 
          </td>
            <td height="20">
              <% If bFormBad AND bBad02 Then %>
              <font color="#CC0000">Can't be blank </font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="20" colspan="3"></td>
    </tr>
    <tr> 
      <td><font color="#000000" size="1" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
            <td colspan="2" align="left"><p><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong>* 
                We recommend using your e-mail address as a<br>
                </strong></font><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong>Username 
                as it is unique and easy to remember.<br>
                </strong></font></p>
              </td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Username 
        : <font color="#FF0000">*</font> </strong></font> </td>
          <td height="20" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="UserName" type="text" id="UserName" value="<%=cUserName%>"> </td>
            <td height="20">
              <% If BformBad AND bBad03 Then %><font color="#CC0000">Can't be blank </font> <% Else If BformBad AND bBad12 AND NOT bBad03 Then %>
              <font color="#CC0000">User with specified name already exists, please 
              choose another name. If you forgot your password pleas follow <a href="/en/order/forgotpass.asp">this 
              link</a>.</font><% End If
			   End If %>
            </td>
    </tr>
    <tr> 
            <td height="20" colspan="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
	        <td height="20" colspan="2"><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"><strong>* 
              Password me be alphanumeric and 7-14 characters in length.</strong></font></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Password 
        : <font color="#FF0000">*</font> </strong></font> </td>
          <td height="20"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Password" type="password" id="Password"></td>
            <td height="20">
              <% If BformBad AND bBad06 Then %>
              <font color="#CC0000">The password you specified is not correct</font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Password 
        again : <font color="#FF0000">*</font></strong></font></td>
          <td height="20"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="PasswordConfirm" type="password" id="PasswordConfirm"></td>
            <td height="20">
              <% If BformBad AND bBad07 Then %>
              <font color="#CC0000">Doesn't match &quot;Password&quot; field above</font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="20" colspan="3"><img src="/images/transparent.gif" width="1" height="20"></td>
    </tr>
    <tr> 
      <td height="20" colspan="3"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>Billing 
        Address : </strong><font size="2">(the address to which your credit card 
        statement is mailed)</font></font></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong></strong></font> 
      </td>
      <td height="20" colspan="2" valign="bottom"><font size="2" face="Arial, Helvetica, sans-serif"><font color="#000000" size="1">(Street Address - In 
        this order please: house or building number and street name)</font></font></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Street 
        Address 1 : <font color="#FF0000">*</font></strong></font></td>
          <td height="20" colspan="2"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Address1" type="text" id="Address1" value="<%=cAddress1%>" size="30">
              <% If bFormBad AND bBad08 Then %>
              <font color="#CC0000">Can't be blank </font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Apartment 
        : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
          <td height="20" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Appartment" type="text" id="Appartment" value="<%=cAppartment%>" size="5"> 
          </td>
      <td height="20">&nbsp;</td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Street 
        Address 2 : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
          <td height="20" colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Address2" type="text" id="Address2" value="<%=cAddress2%>" size="30"> 
          </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>City 
        : <font color="#FF0000">*</font></strong></font></td>
          <td height="20"  align="left"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="City" type="text" id="City" value="<%=cCity%>" maxlength="25"></td>
			<td height="20">
              <% If BformBad AND bBad09 Then %>
              <font color="#CC0000">Can't be blank </font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Province 
        / State : <font color="#FF0000">*</font></strong></font></td>
          <td height="20"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Province" type="text" id="Province" value="<%=cProvince%>" maxlength="25"></td>
            <td height="20">
              <% If BformBad AND bBad10 Then %>
              <font color="#CC0000">Can't be blank </font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Postal 
        Code / Zip Code : <font color="#FF0000">*</font></strong></font></td>
          <td height="20"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Postal" type="text" id="Postal" value="<%=cPostal%>" size="14" maxlength="14"></td>
            <td height="20">
              <% If BformBad AND bBad11 Then %>
              <font color="#CC0000">Can't be blank </font> 
              <% End If %>
            </td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Country 
        : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
          <td height="20"><img src="/images/transparent.gif" width="5" height="1"> 
            <select name="Country" class="input_textbox">
          <option value="">Please Choose A Country</option>
          <%
While (NOT spCountries.EOF)
%>
          <option value="<%=(spCountries.Fields.Item("Country").Value)%>" <%if (CStr(spCountries.Fields.Item("Country").Value) = CStr(cCountry)) then Response.Write("SELECTED") : Response.Write("")%> ><%=(spCountries.Fields.Item("Country").Value)%></option>
          <%
  spCountries.MoveNext()
Wend

  spCountries.close
%>
        </select></td>
      <td height="20">&nbsp;</td>
    </tr>
    <tr> 
      <td height="20" colspan="3"><img src="/images/transparent.gif" width="1" height="20"></td>
    </tr>
    <tr> 
      <td height="20" colspan="3"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>Contact 
        Information: </strong></font></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Company 
        / Organization : <img src="/images/transparent.gif" width="5" height="1"></strong></font></td>
          <td height="20" colspan="2"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Company" type="text" id="Company" value="<%=cCompany%>" size="35"></td>
    </tr>
    <tr> 
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Phone 
        : <font color="#FF0000">*</font></strong></font></td>
      <td height="20" colspan="2" align="left"><img src="/images/transparent.gif" width="5" height="1"> 
        <font size="5" face="Arial, Helvetica, sans-serif">(</font> <input name="PhoneAC" type="text" id="PhoneAC" value="<%=cPhoneAC%>" size="5"> 
        <font size="5" face="Arial, Helvetica, sans-serif">)</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone3" type="text" id="Phone3" value="<%=cPhone3%>" size="5"> <img src="/images/transparent.gif" width="5" height="1"><font size="5" face="Arial, Helvetica, sans-serif">-</font><img src="/images/transparent.gif" width="8" height="8"> 
        <input name="Phone4" type="text" id="Phone4" value="<%=cPhone4%>" size="7">
              <img src="/images/transparent.gif" width="15" height="0"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Ext 
              : <img src="/images/transparent.gif" width="5" height="1"> 
              <input name="PhoneEXT" type="text" id="PhoneEXT" value="<%=cPhoneEXT%>" size="7">
             
              </strong><strong> </strong></font></td>
    </tr>
    <tr> 
      <td height="10" ><img src="/images/transparent.gif" width="1" height="10"></td>
	        <td  colspan="2"><img src="/images/transparent.gif" width="1" height="10"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%If BformBad AND (bBad13 OR bBad14 OR bBad15) Then %>
              <font color="#CC0000">Can't be blank</font> 
              <% End If %>
              </font></td>
    </tr>
    <tr> 
      <td height="20" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Email 
        : <font color="#FF0000">*</font></strong></font></td>
          <td height="20" colspan="2"><img src="/images/transparent.gif" width="5" height="1"> 
            <input name="Email" type="text" id="Email" value="<%=cEmail%>" size="40">
            </td>
    </tr>
    <tr> 
	      <td>&nbsp;</td>
            <td height="20" colspan="2"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">		<% If BformBad AND bBad04 Then %>
              <font color="#CC0000">Can't be blank</font>
              <% Else if BformBad AND bBad05 AND NOT bBad04 then%><font color="#CC0000">Invalid Email Address</font><%End If 
			  End If%>
              </font></td>
	     </tr>
    <tr> 
            <td height="20" colspan="3"> 
              <input type="hidden" name="MM_insert" value="NewCartUser"></td>
    </tr>
    <tr align="center"> 
      <td height="20" colspan="3"><input name="Sign Up" type="image" src="/images/sign_up.gif" alt="Create New Account" width="76" height="18"></td>
    </tr></form>
    <tr> 
      <td colspan="3">&nbsp;</td>
    </tr>
  </table>
</td>
    <td width="10" height="916"></td>
  </tr>
  <tr>
    <td width="8" height="8"></td>
    <td width="2" height="8" bgcolor="#FFFFFF"></td>
    <td width="6" height="8" bgcolor="#FFFFFF"></td>
    <td width="141" height="8" bgcolor="#FFFFFF"></td>
    <td width="8" height="8" bgcolor="#FFFFFF"></td>
    <td width="126" height="8" bgcolor="#FFFFFF"></td>
    <td width="15" height="8" bgcolor="#FFFFFF"></td>
    <td width="8" height="8" bgcolor="#FFFFFF"></td>
    <td width="141" height="8" bgcolor="#FFFFFF"></td>
    <td width="8" height="8" bgcolor="#FFFFFF"></td>
    <td width="141" height="8" bgcolor="#FFFFFF"></td>
    <td width="13" height="8" bgcolor="#FFFFFF"></td>
    <td width="10" height="8"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="12" height="6" bgcolor="#FFFFFF"></td>
    <td width="141" height="6" bgcolor="#FFFFFF"></td>
    <td width="2" height="6" bgcolor="#FFFFFF"></td>
    <td width="6" height="6" bgcolor="#FFFFFF"></td>
    <td width="141" height="6" bgcolor="#FFFFFF"></td>
    <td width="8" height="6" bgcolor="#FFFFFF"></td>
    <td width="126" height="6" bgcolor="#FFFFFF"></td>
    <td width="15" height="6" bgcolor="#FFFFFF"></td>
    <td width="8" height="6" bgcolor="#FFFFFF"></td>
    <td width="141" height="6" bgcolor="#FFFFFF"></td>
    <td width="8" height="6" bgcolor="#FFFFFF"></td>
    <td width="141" height="6" bgcolor="#FFFFFF"></td>
    <td width="13" height="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="13" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td width="762" height="49" colspan="13" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="13" valign="top"><IMG SRC="/images/page-bottom.gif" WIDTH=762 HEIGHT=7></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="15"></td>
  </tr>
</table>
</BODY>
</HTML>