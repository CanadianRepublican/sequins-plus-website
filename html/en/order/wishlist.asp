<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%><%MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%>
<SCRIPT RUNAT=SERVER LANGUAGE=VBSCRIPT>					
function DoDateTime(str, nNamedFormat, nLCID)				
	dim strRet								
	dim nOldLCID								
										
	strRet = str								
	If (nLCID > -1) Then							
		oldLCID = Session.LCID						
	End If									
										
	On Error Resume Next							
										
	If (nLCID > -1) Then							
		Session.LCID = nLCID						
	End If									
										
	If ((nLCID < 0) Or (Session.LCID = nLCID)) Then				
		strRet = FormatDateTime(str, nNamedFormat)			
	End If									
										
	If (nLCID > -1) Then							
		Session.LCID = oldLCID						
	End If									
										
	DoDateTime = strRet							
End Function									
</SCRIPT>									
<HTML>
<HEAD>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" -->
<%
Set SPrsWish = Server.CreateObject("ADODB.Recordset")
SPrsWish.ActiveConnection = SPdbString
SPrsWish.Source = "SELECT spWishList.id as wlid, spWishList.UserID, spWishList.ItemID, spWishList.Comments, spWishList.AddDate, spItem.ProductCode, spItem.Manu, spItem.Name, spItem.Price, spItem.id, spItemManufacturer.id, spItem.SmallPic, spItemManufacturer.Name as ManuName FROM spWishlist, spItemManufacturer, spItem Where spWishList.UserID = "&Session("MM_UserID")&" AND spItem.Manu = spItemManufacturer.id AND spItem.id = spWishList.ItemID"
SPrsWish.CursorType = 0
SPrsWish.CursorLocation = 2
SPrsWish.LockType = 3
SPrsWish.Open()
SPrsWish_numRows = 0
%><%
Dim HLooper1__numRows
HLooper1__numRows = 4
Dim HLooper1__index
HLooper1__index = 0
SPrsWish_numRows = SPrsWish_numRows + HLooper1__numRows
%>
<%
'  *** Recordset Stats, Move To Record, and Go To Record: declare stats variables
' set the record count
SPrsWish_total = SPrsWish.RecordCount

' set the number of rows displayed on this page
If (SPrsWish_numRows < 0) Then
  SPrsWish_numRows = SPrsWish_total
Elseif (SPrsWish_numRows = 0) Then
  SPrsWish_numRows = 1
End If

' set the first and last displayed record
SPrsWish_first = 1
SPrsWish_last  = SPrsWish_first + SPrsWish_numRows - 1

' if we have the correct record count, check the other stats
If (SPrsWish_total <> -1) Then
  If (SPrsWish_first > SPrsWish_total) Then SPrsWish_first = SPrsWish_total
  If (SPrsWish_last > SPrsWish_total) Then SPrsWish_last = SPrsWish_total
  If (SPrsWish_numRows > SPrsWish_total) Then SPrsWish_numRows = SPrsWish_total
End If
%>
<%
' *** Recordset Stats: if we don't know the record count, manually count them
SPrsWish_total = -1
If (SPrsWish_total = -1) Then

  ' count the total records by iterating through the recordset
  SPrsWish_total=0
  While (Not SPrsWish.EOF)
    SPrsWish_total = SPrsWish_total + 1
    SPrsWish.MoveNext
  Wend

  ' reset the cursor to the beginning
  If (SPrsWish.CursorType > 0) Then
    SPrsWish.MoveFirst
  Else
    SPrsWish.Requery
  End If

  ' set the number of rows displayed on this page
  If (SPrsWish_numRows < 0 Or SPrsWish_numRows > SPrsWish_total) Then
    SPrsWish_numRows = SPrsWish_total
  End If

  ' set the first and last displayed record
  SPrsWish_first = 1
  SPrsWish_last = SPrsWish_first + SPrsWish_numRows - 1
  If (SPrsWish_first > SPrsWish_total) Then SPrsWish_first = SPrsWish_total
  If (SPrsWish_last > SPrsWish_total) Then SPrsWish_last = SPrsWish_total

End If
%>
<%
' *** Move To Record and Go To Record: declare variables

Set MM_rs    = SPrsWish
MM_rsCount   = SPrsWish_total
MM_size      = SPrsWish_numRows
MM_uniqueCol = ""
MM_paramName = ""
MM_offset = 0
MM_atTotal = false
MM_paramIsDefined = false
If (MM_paramName <> "") Then
  MM_paramIsDefined = (Request.QueryString(MM_paramName) <> "")
End If

SPrsWish_pages = SPrsWish_total / HLooper1__numRows
RSBR = CInt(SPrsWish_pages)
if RSBR >= SPrsWish_pages then SPrsWish_pages = RSBR else SPrsWish_pages = RSBR + 1
%>
<%
' *** Move To Record: handle 'index' or 'offset' parameter

if (Not MM_paramIsDefined And MM_rsCount <> 0) then

  ' use index parameter if defined, otherwise use offset parameter
  r = Request.QueryString("index")
  If r = "" Then r = Request.QueryString("offset")
  If r <> "" Then MM_offset = Int(r)

  ' if we have a record count, check if we are past the end of the recordset
  If (MM_rsCount <> -1) Then
    If (MM_offset >= MM_rsCount Or MM_offset = -1) Then  ' past end or move last
      If ((MM_rsCount Mod MM_size) > 0) Then         ' last page not a full repeat region
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' move the cursor to the selected record
  i = 0
  While ((Not MM_rs.EOF) And (i < MM_offset Or MM_offset = -1))
    MM_rs.MoveNext
    i = i + 1
  Wend
  If (MM_rs.EOF) Then MM_offset = i  ' set MM_offset to the last possible record

End If
%>
<%
' *** Move To Record: if we dont know the record count, check the display range


If (MM_rsCount = -1) Then

  ' walk to the end of the display range for this page
  i = MM_offset
  While (Not MM_rs.EOF And (MM_size < 0 Or i < MM_offset + MM_size))
    MM_rs.MoveNext
    i = i + 1
  Wend

  ' if we walked off the end of the recordset, set MM_rsCount and MM_size
  If (MM_rs.EOF) Then
    MM_rsCount = i
    If (MM_size < 0 Or MM_size > MM_rsCount) Then MM_size = MM_rsCount
  End If

  ' if we walked off the end, set the offset based on page size
  If (MM_rs.EOF And Not MM_paramIsDefined) Then
    If (MM_offset > MM_rsCount - MM_size Or MM_offset = -1) Then
      If ((MM_rsCount Mod MM_size) > 0) Then
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' reset the cursor to the beginning
  If (MM_rs.CursorType > 0) Then
    MM_rs.MoveFirst
  Else
    MM_rs.Requery
  End If

  ' move the cursor to the selected record
  i = 0
  While (Not MM_rs.EOF And i < MM_offset)
    MM_rs.MoveNext
    i = i + 1
  Wend
End If
%>
<%
' *** Move To Record: update recordset stats

' set the first and last displayed record
SPrsWish_first = MM_offset + 1
SPrsWish_last  = MM_offset + MM_size
If (MM_rsCount <> -1) Then
  If (SPrsWish_first > MM_rsCount) Then SPrsWish_first = MM_rsCount
  If (SPrsWish_last > MM_rsCount) Then SPrsWish_last = MM_rsCount
End If

' set the boolean used by hide region to check if we are on the last record
MM_atTotal = (MM_rsCount <> -1 And MM_offset + MM_size >= MM_rsCount)
%>
<%
' *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

' create the list of parameters which should not be maintained
MM_removeList = "&index=&x=&y=&SortOn="
If (MM_paramName <> "") Then MM_removeList = MM_removeList & "&" & MM_paramName & "="
MM_keepURL="":MM_keepForm="":MM_keepBoth="":MM_keepNone=""

' add the URL parameters to the MM_keepURL string
For Each Item In Request.QueryString
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepURL = MM_keepURL & NextItem & Server.URLencode(Request.QueryString(Item))
  End If
Next

' add the Form variables to the MM_keepForm string
For Each Item In Request.Form
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepForm = MM_keepForm & NextItem & Server.URLencode(Request.Form(Item))
  End If
Next

' create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL & MM_keepForm
if (MM_keepBoth <> "") Then MM_keepBoth = Right(MM_keepBoth, Len(MM_keepBoth) - 1)
if (MM_keepURL <> "")  Then MM_keepURL  = Right(MM_keepURL, Len(MM_keepURL) - 1)
if (MM_keepForm <> "") Then MM_keepForm = Right(MM_keepForm, Len(MM_keepForm) - 1)

' a utility function used for adding additional parameters to these strings
Function MM_joinChar(firstItem)
  If (firstItem <> "") Then
    MM_joinChar = "&"
  Else
    MM_joinChar = ""
  End If
End Function
%>
<%
' *** Move To Record: set the strings for the first, last, next, and previous links

MM_keepMove = MM_keepBoth
MM_moveParam = "index"

' if the page has a repeated region, remove 'offset' from the maintained parameters
If (MM_size > 0) Then
  MM_moveParam = "offset"
  If (MM_keepMove <> "") Then
    params = Split(MM_keepMove, "&")
    MM_keepMove = ""
    For i = 0 To UBound(params)
      nextItem = Left(params(i), InStr(params(i),"=") - 1)
      If (StrComp(nextItem,MM_moveParam,1) <> 0) Then
        MM_keepMove = MM_keepMove & "&" & params(i)
      End If
    Next
    If (MM_keepMove <> "") Then
      MM_keepMove = Right(MM_keepMove, Len(MM_keepMove) - 1)
    End If
  End If
End If

' set the strings for the move to links
If (MM_keepMove <> "") Then MM_keepMove = MM_keepMove & "&"
urlStr = Request.ServerVariables("URL") & "?" & MM_keepMove & MM_moveParam & "="
MM_moveFirst = urlStr & "0"
MM_moveLast  = urlStr & "-1"
MM_moveNext  = urlStr & Cstr(MM_offset + MM_size)
prev = MM_offset - MM_size
If (prev < 0) Then prev = 0
MM_movePrev  = urlStr & Cstr(prev)
MM_TotalPages = SPrsWish_pages
%><%
Set RSquickcat = Server.CreateObject("ADODB.Recordset")
RSquickcat.ActiveConnection = SPdbString
RSquickcat.Source = "SELECT spItemCatagory.id, spItemCatagory.Name FROM spItemCatagory ORDER BY spItemCatagory.Name ASC"
RSquickcat.CursorType = 0
RSquickcat.CursorLocation = 2
RSquickcat.LockType = 3
RSquickcat.Open()
%>
<TITLE>Sequins Plus Online - Wish List</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR=#DFDFDF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="MM_preloadImages('/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif','/images/cartbut_1_over.gif','/images/cartbut_2_over.gif','/images/cartbut_3_over.gif','/images/cartbut_4_over.gif','/images/cartbut_5_over.gif')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" colspan="4"></td>
    <td width="281" colspan="4" valign="top"><IMG SRC="/images/logo_top.gif" WIDTH=281 HEIGHT=28></td>
    <td width="336" colspan="7"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="155" colspan="3" valign="top"><IMG SRC="/images/logo_lft_top_line.gif" WIDTH=155 HEIGHT=9></td>
    <td width="281" colspan="4" valign="top"><IMG SRC="/images/logo_topline.gif" WIDTH=281 HEIGHT=9></td>
    <td width="326" colspan="6" valign="top"><IMG SRC="/images/logo_rt_top_line.gif" WIDTH=326 HEIGHT=9></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="155" height="26" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" colspan="4" valign="top"><IMG SRC="/images/logo_title.gif" WIDTH=281 HEIGHT=26></td>
    <td width="326" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="155" colspan="3" valign="top"><IMG SRC="/images/logo_lft_bot_line.gif" WIDTH=155 HEIGHT=11></td>
    <td width="281" colspan="4" valign="top"><IMG SRC="/images/logo_bot-line.gif" WIDTH=281 HEIGHT=11></td>
    <td width="326" colspan="6" valign="top"><IMG SRC="/images/logo_rt_bot_line.gif" WIDTH=326 HEIGHT=11></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="155" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="281" colspan="4" valign="top"><IMG SRC="/images/logo_beige.gif" WIDTH=281 HEIGHT=14></td>
    <td width="326" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="155" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" colspan="4" valign="top"><IMG SRC="/images/logo_spacer.gif" WIDTH=281 HEIGHT=2></td>
    <td width="326" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="155" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" colspan="4" valign="top"><IMG SRC="/images/cartpage_22.gif" WIDTH=281 HEIGHT=19></td>
    <td width="326" colspan="6" bgcolor="#FFFFFF"><img src="/images/red_arrow.gif" width="17" height="17" align="absmiddle"><img src="/images/transparent.gif" width="5" height="1"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong>Shopping 
      Cart Items:</strong></font> <strong><%=UCCart1.GetItemCount()%></strong> <strong><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Subtotal:</font></strong> 
      $<strong><%=FormatNumber(UCCart1.GetColumnTotal("Total"), 2, -2, -2, -1)%></strong></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="762" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="12" bgcolor="#FFFFFF"></td>
    <td width="141" valign="top"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('homepage','','/images/cartbut_1_over.gif',1)"><img src="/images/cartbut_1.gif" alt="Homepage" name="homepage" width="141" height="20" border="0"></a></td>
    <td width="8" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="141" valign="top"><a href="/en/order/orders.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('check','','/images/cartbut_2_over.gif',1)"><img src="/images/cartbut_2.gif" alt="Check Your Order Status" name="check" width="141" height="20" border="0"></a></td>
    <td width="8" bgcolor="#FFFFFF"></td>
    <td width="141" colspan="2" valign="top"><a href="/en/order/new.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('create','','//images/cartbut_3_over.gif',1)"><img src="/images/cartbut_6.gif" alt="Account Settings" name="create" width="141" height="20" border="0"></a></td>
    <td width="8" bgcolor="#FFFFFF"></td>
    <td width="141" valign="top"><a href="/en/order/logout.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('signin','','/images/cartbut_4_over.gif',1)"><img src="/images/cartbut_7.gif" alt="Sign Out" name="signin" width="141" height="20" border="0"></a></td>
    <td width="8" bgcolor="#FFFFFF"></td>
    <td width="141" valign="top"><a href="/en/order/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('account','','/images/cartbut_5_over.gif',1)"><img src="/images/cartbut_5.gif" alt="Account Services" name="account" width="141" height="20" border="0"></a></td>
    <td width="13" bgcolor="#FFFFFF"></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="153" colspan="2" rowspan="3" valign="top" bgcolor="#FFFFFF"><!--#include virtual="sequinsplus/inc/cartSB.asp" --></td>
    <td width="2" bgcolor="#FFFFFF"></td>
    <td width="6" bgcolor="#FFFFFF"></td>
    <td width="601" colspan="9" valign="top" bgcolor="#FFFFFF"> 
      <table width="601" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
        <tr bgcolor="#CC1C0F"> 
          <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td colspan="6" align="center" bgcolor="#CC1C0F"><img src="/images/wishlist_title.gif" width="267" height="40"></td>
          <form name="jumpmenu" onSubmit="return jump(document.jumpmenu.menu.options[document.jumpmenu.menu.options.selectedIndex].value)"> <td align="right" valign="top" bgcolor="#CC1C0F"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font>
            <select align="middle" name="menu" >
                <option value="none" selected>More Categories</option><% While (NOT RSquickcat.EOF) %>
          <option value="<%="/en/products/default.asp?Cat="&(RSquickcat.Fields.Item("id").Value)%>"  ><%=(RSquickcat.Fields.Item("Name").Value)%></option>
          <% 
		  RSquickcat.MoveNext()
		  Wend
		  RSquickcat.close()
		  %>
		  </select>
              <input name="button" type="image" src="/images/btn_go.jpg" alt="Search" align="middle" width="27" height="27" hspace="0" vspace="0" border="0">
      </td></form>
        </tr>
        <tr> 
          <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td colspan="7"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Welcome 
            to your Wish List. Keep track of the stuff you really want, when you 
            are ready you can add these items to your cart.</font></td>
        </tr>
        <tr> 
          <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td height="1" colspan="7" bgcolor="#6A6A6A"><img src="/images/transparent.gif" width="1" height="1"></td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td width="10" align="right" valign="bottom" bgcolor="#EBEBEB"><img src="/images/transparent.gif" width="10" height="8"></td>
          <td align="right" valign="bottom" bgcolor="#EBEBEB"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong>Sort 
            By :</strong></font><img src="/images/transparent.gif" width="5" height="1"></td>
          <td align="left" valign="bottom" bgcolor="#EBEBEB"> <select name="select">
              <option>Date Added</option>
              <option>Last Updated</option>
              <option>Category</option>
              <option>Manufacturer</option>
              <option>Price: Low to High</option>
              <option>Price: High to Low</option>
            </select> <input name="search4" type="image" src="/images/grey_go.gif" alt="Sort" align="absmiddle" width="19" height="19" hspace="0" vspace="0" border="0"> 
          </td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td align="right" valign="bottom" bgcolor="#EBEBEB">&nbsp;</td>
          <td colspan="2" align="left" valign="bottom" bgcolor="#EBEBEB">&nbsp; </td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr bgcolor="#EBEBEB"> 
          <td height="5" colspan="7"><img src="/images/transparent.gif" width="1" height="5"></td>
        </tr>
        <tr> 
          <td height="1" colspan="7" bgcolor="#6A6A6A"><img src="/images/transparent.gif" width="1" height="1"></td>
        </tr>
        <tr> 
          <td height="20" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="9" height="8"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Wishlist 
            Items: <%=SPrsWish_total%></font></td>
          <td align="right" bgcolor="#FFFFFF"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Viewing 
            1 - 4 of <%=SPrsWish_total%></font><img src="/images/transparent.gif" width="20" height="8"></td>
        </tr>
      </table>
</td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="2" bgcolor="#FFFFFF"></td>
    <td width="6" bgcolor="#FFFFFF"></td>
    <td width="601" colspan="9" valign="top" bgcolor="#FFFFFF"> <table width="601" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="90" height="14" align="center" valign="middle" bgcolor="#EED39E"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Item</strong></font></td>
          <td height="14"> <table width="100%" height="14" border="0" cellpadding="0" cellspacing="0" bgcolor="#EED39E">
              <tr> 
                <td width="165" height="14" align="center" valign="middle"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Description</strong></font></td>
                <td width="65" align="center" valign="middle"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Price</strong></font></td>
                <td width="125" align="center" valign="middle"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Date 
                  Added </strong></font></td>
                <td width="115">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <%
startrw = 0
endrw = HLooper1__index
numberColumns = 1
numrows = 4
while((numrows <> 0) AND (Not  SPrsWish.EOF))
	startrw = endrw + 1
	endrw = endrw + numberColumns
 %>
        <tr valign="top"> 
          <%
While ((startrw <= endrw) AND (Not SPrsWish.EOF))
%>
        <tr> 
          <td align="center" valign="middle"><img src="/images/products/small/<%=(SPrsWish.Fields.Item("SmallPic").Value)%>" width="60" height="70" vspace="5"></td>
          <td align="left" valign="top"> 
            <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
              <tr> 
                <td width="165" height="40" align="center" valign="middle"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><%=(SPrsWish.Fields.Item("ManuName").Value)& " " & (SPrsWish.Fields.Item("Name").Value)%></strong></font></td>
                <td width="65" align="center" valign="middle"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><%=SPrsWish.Fields.Item("Price").Value%></strong></font></td>
                <td width="125" align="center" valign="middle"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><%=DoDateTime(SPrsWish.Fields.Item("AddDate").Value, 1, -1)%></strong></font></td>
                <td width="115" align="center" valign="middle"><form action="/en/order/add.asp" method="POST" name="ShoppingCart">
                              
                    <input name="add" type="image" src="/images/add-to-cart.gif" alt="Add To Cart" align="middle" width="100" height="17" border="0">
                              <input type="hidden" name="ProductID" value="<%=(SPrsWish.Fields.Item("id").Value)%>">
                              <input type="hidden" name="ProductCode" value="<%=(SPrsWish.Fields.Item("ProductCode").Value)%>">
                              <input type="hidden" name="Quantity" value="1">
                              <input type="hidden" name="Name" value="<%=(SPrsWish.Fields.Item("ManuName").Value)& " " & (SPrsWish.Fields.Item("Name").Value)%>">
                              <input type="hidden" name="ColourID" value="">
                              <input type="hidden" name="Colour" value="">
                              <input type="hidden" name="SizeTypeID" value="">
                              <input type="hidden" name="SizeID" value="">
                              <input type="hidden" name="SizeType" value="">
                              <input type="hidden" name="Size" value="">
                              <input type="hidden" name="Price" value="<%=(SPrsWish.Fields.Item("Price").Value)%>">
                              <input type="hidden" name="Special" value="">
                              <input type="hidden" name="UC_recordId" value="<%= SPrsWish.Fields.Item("id").Value %>">
                            </form></td>
              </tr>
              <tr> 
                <td align="center" valign="top" colspan="4">
                  <form action="/en/order/Update.asp" method="post" name="Update" id="Update">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="5" align="right" valign="top">&nbsp;</td>
                      <td align="center">&nbsp;</td>
                      <td align="center" valign="middle">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="71" align="right" valign="top"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Comments: 
                        </font></td>
                      <td width="284" align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
                          <textarea name="com" cols="40" rows="3" id="com"><%=(SPrsWish.Fields.Item("Comments").Value)%></textarea>
                        </font></td>
                      <td width="115" align="center" valign="top"><input name="Update" type="image" src="/images/update_red.gif" alt="Save Changes" width="76" height="18"><input type="hidden" name="MM_update" value="Update">
  <input type="hidden" name="MM_recordId" value="<%=SPrsWish.Fields.Item("wlid").Value %>"></td>
                    </tr>
                  </table></form>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="177" height="35">&nbsp;</td>
                      <td width="177" height="35" valign="top">&nbsp;</td>
                      <td width="115" height="35" align="center" valign="top"> 
                        <form name="Remove" method="POST" action="/en/order/remove.asp">
                          <input name="RemoveButton" type="image" src="/images/remove_red.gif" alt="Remove Item From Wish List" align="middle" width="112" height="18" border="0">
                          <input type="hidden" name="MM_delete" value="Remove">
                          <input type="hidden" name="MM_recordId" value="<%=SPrsWish.Fields.Item("wlid").Value%>">
                        </form></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td height="1" colspan="5" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="1"></td>
        </tr>
        <%	
  startrw = startrw + 1
  SPrsWish.MoveNext
  		wend
	 numrows=numrows-1
		wend%>
        <tr align="right" valign="bottom"> 
          <td height="25" colspan="5" bgcolor="#FFFFFF"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#000000"> 
            <% If Not SPrsWish.EOF Or Not SPrsWish.BOF Then %>
            <% If MM_offset <> 0 Then %>
            <a href="<%if (Not SSortOn = "ANY") then%><%=MM_movePrev & "&SortOn=" & SSortOn%><%else%><%=MM_movePrev%><%end if%>"><strong><img src="/images/btn-arrow-left_gry.gif" border="0" width="15" height="15" align="texttop"></strong></a> 
            <% Else %>
            <% End If ' end MM_offset <> 0 %>
            <% End If ' end Not SPrsWish.EOF Or NOT SPrsWish.BOF %>
            <font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
            <% if NOT (MM_TotalPages <= 1) then%>
            <strong>Page(s):</strong> <strong> 
            <%if pagnum <= 5 then 
			  sNum = 1 
			  dNum = 10
			  else 
			  sNum = pagnum - 4
			  if (pagnum+5) >= MM_TotalPages then
			  dNum = MM_TotalPages
			  else
			  dNum = pagnum + 5
			  end if
			  end if
			  if MM_TotalPages <= 10 then
			  sNum = 1 
			  dNum = MM_TotalPages
			  end if
			FOR i = sNum TO dNum
			if i = MM_TotalPages OR i = dNum then
			urlmult = i - 1 
			urloffsetval = urlmult * SPrsWish_numRows
			if (NOT SSortOn = "ANY") then
			pageURL = urlStr & urloffsetval & "&SortOn=" & SSortOn
			else 
			pageURL = urlStr & urloffsetval
			end if
			%>
            <a href="<%=pageURL%>"><%=i%></a> 
            <%
			else urlmult = i - 1 
			urloffsetval = urlmult * SPrsWish_numRows
			if (NOT SSortOn = "ANY") then
			pageURL = urlStr & urloffsetval & "&SortOn=" & SSortOn
			else 
			pageURL = urlStr & urloffsetval
			end if
			%>
            <a href="<%=pageURL%>"><%=i%>,</a> 
            <%
			end if 
			NEXT%>
            <%end if%>
            </strong></font> 
            <% If Not SPrsWish.EOF Or Not SPrsWish.BOF Then %>
            <% If Not MM_atTotal Then %>
            <a href="<%if (Not SSortOn = "ANY") then%><%=MM_moveNext & "&SortOn=" & SSortOn%><%else%><%=MM_moveNext%><%end if%>"> 
            <strong><img src="/images/btn-arrow-right.gif" width="15" height="15" border="0" align="texttop"></strong></a> 
            <% Else %>
            <% End If ' end Not MM_atTotal %>
            <% End If ' end Not SPrsWish.EOF Or NOT SPrsWish.BOF %>
            <img src="/images/transparent.gif" width="40" height="1"> <br>
            </font></strong></font> <img src="/images/transparent.gif" width="20" height="8"></td>
        </tr>
        <tr> 
          <td height="10" colspan="5" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td colspan="5" bgcolor="#FFFFFF"><a href="#"><img src="/images/continue-shopping-red.gif" alt="Continue Shopping in Products" width="129" height="18" hspace="10" border="0" align="absmiddle"></a></td>
        </tr>
        <tr> 
          <td height="10" colspan="5" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
      </table>
      <br>
      <table width="590" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td colspan="2"><font color="#CC1C0F" size="3" face="Arial, Helvetica, sans-serif"><strong>YOUR 
            SITE SERVICES</strong></font></td>
        </tr>
        <tr> 
          <td valign="top"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong>My 
            Order</strong></font><br> <font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&#149; 
            Track your <u>Recent Orders</u>.<br>
            &#149; View or change your orders in <u><a href="/en/order/default.asp">My 
            Account</a></u><a href="/en/order/default.asp">.</a><br>
            &#149; See our <u>Shipping Rates and Policies</u>.<br>
            </font></td>
          <td valign="top"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong>Help</strong></font><br>
            <font color="#616161" size="2" face="Arial, Helvetica, sans-serif">&#149; 
            Forgot your password? <a href="/en/order/forgot.asp"><u>Click Here</u></a>.<br>
            &#149; Visit the online <a href="javascript:Help()"><u>Help</u></a> 
            section.<br>
            &#149; <u>Return</u> an item.<br>
            </font></td>
        </tr>
      </table></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="2" bgcolor="#FFFFFF"></td>
    <td width="6" bgcolor="#FFFFFF"></td>
    <td width="601" colspan="9" rowspan="2" valign="top" align="center" style="padding-top: 5; padding-bottom: 5; padding-left: 5; padding-right:5;" bgcolor="#FFFFFF">&nbsp; 
    </td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="153" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="2" bgcolor="#FFFFFF"></td>
    <td width="6" bgcolor="#FFFFFF"></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="762" height="14" colspan="13" bgcolor="#DFC6A3"></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="762" height="49" colspan="13" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="8"></td>
    <td width="762" colspan="13" valign="top"><IMG SRC="/images/page-bottom.gif" WIDTH=762 HEIGHT=7></td>
    <td width="10"></td>
  </tr>
  <tr>
    <td width="780" colspan="15"></td>
  </tr>
  <tr>
    <td width="8" valign="top"><img src="transparent.gif" alt="" width="8" height="1"></td>
    <td width="12" valign="top"><img src="transparent.gif" alt="" width="12" height="1"></td>
    <td width="141" valign="top"><img src="transparent.gif" alt="" width="141" height="1"></td>
    <td width="2" valign="top"><img src="transparent.gif" alt="" width="2" height="1"></td>
    <td width="6" valign="top"><img src="transparent.gif" alt="" width="6" height="1"></td>
    <td width="141" valign="top"><img src="transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" valign="top"><img src="transparent.gif" alt="" width="8" height="1"></td>
    <td width="126" valign="top"><img src="transparent.gif" alt="" width="126" height="1"></td>
    <td width="15" valign="top"><img src="transparent.gif" alt="" width="15" height="1"></td>
    <td width="8" valign="top"><img src="transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" valign="top"><img src="transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" valign="top"><img src="transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" valign="top"><img src="transparent.gif" alt="" width="141" height="1"></td>
    <td width="13" valign="top"><img src="transparent.gif" alt="" width="13" height="1"></td>
    <td width="10" valign="top"><img src="transparent.gif" alt="" width="10" height="1"></td>
  </tr>
</table>
<%
		SPrsWish.close%>
</BODY>
</HTML>
