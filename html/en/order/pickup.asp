<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%><%' *** Restrict Access To Page: Grant or deny access to this page
MM_authorizedUsers=""
MM_authFailedURL="/en/order/login.asp"
MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If
If Not MM_grantAccess Then
  MM_qsChar = "?"
  If (InStr(1,MM_authFailedURL,"?") >= 1) Then MM_qsChar = "&"
  MM_referrer = Request.ServerVariables("URL")
  if (Len(Request.QueryString()) > 0) Then MM_referrer = MM_referrer & "?" & Request.QueryString()
  MM_authFailedURL = MM_authFailedURL & MM_qsChar & "accessdenied=" & Server.URLEncode(MM_referrer)
  Response.Redirect(MM_authFailedURL)
End If
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" -->
<!--#include virtual="sequinsplus/inc/cart.asp" --><%
if request.form("Submit") = "Yup" then 
	FormNoError = true
	BformBad = false
end if
if FormNoError = True then
Dim MM_editAction
Dim MM_abortEdit
Dim MM_editQuery
Dim MM_editCmd

Dim MM_editConnection
Dim MM_editTable
Dim MM_editRedirectUrl
Dim MM_editColumn
Dim MM_recordId

Dim MM_fieldsStr
Dim MM_columnsStr
Dim MM_fields
Dim MM_columns
Dim MM_typeArray
Dim MM_formVal
Dim MM_delim
Dim MM_altVal
Dim MM_emptyVal
Dim MM_i

MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Request.QueryString
End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Update Record: set variables
  MM_editConnection = SPdbString
  MM_editTable = "spOrders"
  MM_editColumn = "id"
  MM_recordId = session("OrderID")
  MM_editRedirectUrl = "/en/order/complete.asp"
  MM_fieldsStr  = "Location|value"
  MM_columnsStr = "PickupAtStore|',none,''"
  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")
  ' set the form values
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(MM_i+1) = CStr(Request.Form(MM_fields(MM_i)))
  Next
  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And Request.QueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
    End If
  End If
' *** Update Record: construct a sql update statement and execute it
  ' create the sql update statement
  MM_editQuery = "update " & MM_editTable & " set "
  For MM_i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_formVal = MM_fields(MM_i+1)
    MM_typeArray = Split(MM_columns(MM_i+1),",")
    MM_delim = MM_typeArray(0)
    If (MM_delim = "none") Then MM_delim = ""
    MM_altVal = MM_typeArray(1)
    If (MM_altVal = "none") Then MM_altVal = ""
    MM_emptyVal = MM_typeArray(2)
    If (MM_emptyVal = "none") Then MM_emptyVal = ""
    If (MM_formVal = "") Then
      MM_formVal = MM_emptyVal
    Else
      If (MM_altVal <> "") Then
        MM_formVal = MM_altVal
      ElseIf (MM_delim = "'") Then  ' escape quotes
        MM_formVal = "'" & Replace(MM_formVal,"'","''") & "'"
      Else
        MM_formVal = MM_delim + MM_formVal + MM_delim
      End If
    End If
    If (MM_i <> LBound(MM_fields)) Then
      MM_editQuery = MM_editQuery & ","
    End If
    MM_editQuery = MM_editQuery & MM_columns(MM_i) & " = " & MM_formVal
  Next
  MM_editQuery = MM_editQuery & " where " & MM_editColumn & " = " & MM_recordId

  If (Not MM_abortEdit) Then
    ' execute the update
    Set MM_editCmd = Server.CreateObject("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_editConnection
    MM_editCmd.CommandText = MM_editQuery
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    If (MM_editRedirectUrl <> "") Then
      Response.Redirect(MM_editRedirectUrl)
    End If
  End If
end if
' end
%><%
  set SPcartuser = Server.CreateObject("ADODB.Recordset")
  SPcartuser.ActiveConnection = SPdbString
  SPcartuser.Source = "SELECT * FROM spCartUser WHERE ID_BUYER=" & Session("MM_UserID")
  SPcartuser.CursorType = 0
  SPcartuser.CursorLocation = 2
  SPcartuser.LockType = 3
  SPcartuser.Open%><HTML>
<HEAD>
<TITLE>Sequins Plus Online - Confirm Pickup Location - Step Two</TITLE>
<link href="/main.css" rel="stylesheet" type="text/css">
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<script language="JavaScript" type="text/JavaScript">
<!--
function Cancel() {
var Sform=document.Confirm; Sform.Cancel.value = "Yes";
}
function submitpickup() {
var Sform=document.SPpickup; Sform.Submit.value="Yup";return(true);
}
//-->

</script></HEAD>
<BODY BGCOLOR=#DFDFDF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="MM_preloadImages('/images/cartbut_1_over.gif','/images/cartbut_2_over.gif','/images/cartbut_3_over.gif','/images/cartbut_5_over.gif','/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif','/images/cartbut_7_over.gif')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="4"></td>
    <td width="281" height="28" colspan="4" valign="top"><IMG SRC="/images/logo_top.gif" WIDTH=281 HEIGHT=28></td>
    <td width="336" height="28" colspan="7"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="3" valign="top"><IMG SRC="/images/logo_lft_top_line.gif" WIDTH=155 HEIGHT=9></td>
    <td width="281" height="9" colspan="4" valign="top"><IMG SRC="/images/logo_topline.gif" WIDTH=281 HEIGHT=9></td>
    <td width="326" height="9" colspan="6" valign="top"><IMG SRC="/images/logo_rt_top_line.gif" WIDTH=326 HEIGHT=9></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
    <td height="26" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" colspan="4" valign="top"><IMG SRC="/images/logo_title.gif" WIDTH=281 HEIGHT=26></td>
    <td width="326" height="26" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td width="155" height="11" colspan="3" valign="top"><IMG SRC="/images/logo_lft_bot_line.gif" WIDTH=155 HEIGHT=11></td>
    <td width="281" height="11" colspan="4" valign="top"><IMG SRC="/images/logo_bot-line.gif" WIDTH=281 HEIGHT=11></td>
    <td width="326" height="11" colspan="6" valign="top"><IMG SRC="/images/logo_rt_bot_line.gif" WIDTH=326 HEIGHT=11></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="155" height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="281" height="14" colspan="4" valign="top"><IMG SRC="/images/logo_beige.gif" WIDTH=281 HEIGHT=14></td>
    <td width="326" height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" colspan="4" valign="top"><IMG SRC="/images/logo_spacer.gif" WIDTH=281 HEIGHT=2></td>
    <td width="326" height="2" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="19"></td>
    <td width="155" height="19" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="19" colspan="4" valign="top"><IMG SRC="/images/cartpage_22.gif" WIDTH=281 HEIGHT=19></td>
    <td width="326" height="19" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="19"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="762" height="5" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="20"></td>
    <td width="12" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut1','','/images/cartbut_1_over.gif',1)"><img src="/images/cartbut_1.gif" alt="homepage" name="cartbut1" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/orders.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut2','','/images/cartbut_2_over.gif',1)"><img src="/images/cartbut_2.gif" alt="my orders" name="cartbut2" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" colspan="2" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut3','','/images/cartbut_3_over.gif',1)"><img src="/images/cartbut_3.gif" alt="new account" name="cartbut3" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="logout.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut4','','/images/cartbut_7_over.gif',1)"><img src="/images/cartbut_7.gif" alt="Log-Out" name="cartbut4" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/order/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut5','','/images/cartbut_5_over.gif',1)"><img src="/images/cartbut_5.gif" alt="my account page" name="cartbut5" width="141" height="20" border="0"></a></td>
    <td width="13" height="20" bgcolor="#FFFFFF"></td>
    <td width="10" height="20"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="622"></td>
    <td width="762" height="622" colspan="13" align="center" valign="top" bgcolor="#FFFFFF"> 
      <form action="/en/order/pickup.asp" method="post" name="SPpickup"><table width="760" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
    <tr> 
      <td colspan="7"><img src="/images/topbar2.gif" width="760" height="9"></td>
    </tr>
    <tr> 
      <td colspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="25" height="1"><font color="#FFFFFF" size="4" face="Arial, Helvetica, sans-serif">&#8226; 
        Cart Checkout - <font size="3"><strong>Step 2 (2 of 3) Billing And Shipping 
        Addresses </strong></font></font></td>
    </tr>
    <tr> 
      <td colspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td width="1" rowspan="4" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="1"></td>
	  <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
      <td height="10" colspan="3"><img src="/images/transparent.gif" width="1" height="10"></td>
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
	  <td width="1" rowspan="4" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="1"></td
    ></tr>
    <tr> 
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
      <td valign="top">
<table width="364" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="6"><img src="/images/halfbar.gif" width="364" height="9"></td>
              </tr>
              <tr> 
                <td width="1" rowspan="33" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="1"></td>
                <td colspan="5" align="center" bgcolor="#CC1C0F"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>Billing 
                  Address</strong></font></td>
              </tr>
              <tr> 
                <td height="5" colspan="5" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="5"></td>
              </tr>
              <tr> 
                <td height="10" colspan="5"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td colspan="5"><img src="/images/transparent.gif" width="15" height="1"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Personal 
                  Information:</strong></font></td>
              </tr>
              <tr> 
                <td height="7" colspan="5"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td width="10" rowspan="7"><img src="/images/transparent.gif" width="10" height="1"></td>
                <td colspan="3" align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Name</font></td>
                <td width="10"><img src="/images/transparent.gif" width="5" height="8"></td>
              </tr>
              <tr> 
                <td colspan="3" align="left"><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("FirstName").Value &" "&SPcartuser.Fields.Item("Initial").Value&" "&SPcartuser.Fields.Item("LastName").Value%></font></td>
                <td width="10">&nbsp;</td>
              </tr>
              <tr> 
                <td height="7" colspan="4"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td colspan="3" align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Company</font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="3"><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("Company").Value%></font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td height="7" colspan="4"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td height="1" colspan="3" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="1"></td>
                <td width="10" height="1"><img src="/images/transparent.gif" width="10" height="1"></td>
              </tr>
              <tr> 
                <td height="10" colspan="5"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td colspan="5"><img src="/images/transparent.gif" width="15" height="1"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Billing 
                  Address:</strong></font></td>
              </tr>
              <tr> 
                <td height="7" colspan="5"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td rowspan="10">&nbsp;</td>
                <td colspan="3" align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Street 
                  Address</font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="3"><font size="3" face="Arial, Helvetica, sans-serif"><%if SPcartuser.Fields.Item("Apt").Value <> "" then%>Suite # <%=SPcartuser.Fields.Item("Apt").Value%><br><%end if%><%=SPcartuser.Fields.Item("Address1").Value & "<br>" & SPcartuser.Fields.Item("Address2").Value%></font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td height="7" colspan="4"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">City</font></td>
                <td width="5">&nbsp;</td>
                <td width="140" align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Province 
                  / State</font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("City").Value%></font></td>
                <td>&nbsp;</td>
                <td><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("Province").Value%></font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td height="7" colspan="4"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Postal 
                  Code / Zip</font></td>
                <td>&nbsp;</td>
                <td align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Country</font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("Postal").Value%></font></td>
                <td>&nbsp;</td>
                <td><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("Country").Value%></font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td height="7" colspan="4"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td colspan="3" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="1"></td>
                <td><img src="/images/transparent.gif" width="1" height="1"></td>
              </tr>
              <tr> 
                <td height="10" colspan="5"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td colspan="5"><img src="/images/transparent.gif" width="15" height="1"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Contact 
                  Information:</strong></font></td>
              </tr>
              <tr> 
                <td height="7" colspan="5"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Phone 
                  Number</font></td>
                <td>&nbsp;</td>
                <td align="left"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Extention</font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td><font size="3" face="Arial, Helvetica, sans-serif">(<%=SPcartuser.Fields.Item("PhoneAC").Value%>) <%=SPcartuser.Fields.Item("Phone3").Value%> - <%=SPcartuser.Fields.Item("Phone4").Value%></font></td>
                <td>&nbsp;</td>
                <td><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("Ext").Value%></font></td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Email 
                  Address</font></td>
                <td rowspan="2">&nbsp;</td>
                <td>&nbsp;</td>
                <td rowspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
                <td><font size="3" face="Arial, Helvetica, sans-serif"><%=SPcartuser.Fields.Item("Email").Value%></font></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
      <td valign="top">
<table width="364" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="5"><img src="/images/halfbar.gif" width="364" height="9"></td>
              </tr>
              <tr> 
                <td width="1" rowspan="17" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="1"></td>
                <td colspan="4" align="center" bgcolor="#CC1C0F"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>Store Location Pickup</strong></font></td>
              </tr>
              <tr> 
                <td height="5" colspan="4" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="5"></td>
              </tr>
              <tr> 
                <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td colspan="4"><img src="/images/transparent.gif" width="15" height="1"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Location 
                  1:</strong></font></td>
              </tr>
              <tr> 
                <td height="7" colspan="4"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td width="11" rowspan="4"><img src="/images/transparent.gif" width="10" height="1"></td>
                <td align="center"> <img src="/images/transparent.gif" width="8" height="8">
                  <input name="Location" type="radio" value="Oshawa" checked></td>
                  <td width="322" align="left"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Sequins 
                    Plus Oshawa</strong></font><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><br>
                    377 Wilson Road South<br>
                    Oshawa, Ontario, Canada<br>
                    L1H 6C6, (905) 433-2319</font> </td>
                <td><img src="/images/transparent.gif" width="10" height="1"></td>
              </tr>
              <tr> 
                <td height="7" colspan="3"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td height="1" colspan="2" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="1"></td>
                <td width="10" height="1"><img src="/images/transparent.gif" width="10" height="1"></td>
              </tr>
              <tr> 
                <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td colspan="4"><p><img src="/images/transparent.gif" width="15" height="1"></p>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p></td>
              </tr>
              <tr> 
                <td height="7" colspan="4"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td width="11" rowspan="4"><img src="/images/transparent.gif" width="10" height="1"></td>
                <td align="left"> <img src="/images/transparent.gif" width="5" height="1"></td>
                  <td align="left"><p>&nbsp;</p>
                  </td>
                <td><img src="/images/transparent.gif" width="10" height="1"></td>
              </tr>
              <tr> 
                <td height="7" colspan="3"><img src="/images/transparent.gif" width="1" height="7"></td>
              </tr>
              <tr> 
                <td height="1" colspan="2" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="1"></td>
                <td width="10" height="1"><img src="/images/transparent.gif" width="10" height="1"></td>
              </tr>
              <tr> 
                <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
              </tr>
              <tr>
                <td colspan="5"><img src="/images/transparent.gif" width="1" height="237"><input name="Submit" type="hidden" value="no"></td>
              </tr>
            </table></td>
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td colspan="3"><img src="/images/transparent.gif" width="90" height="1"><a href="#"><img src="/images/cancel-order.gif" alt="Cancel this order and continue shopping" width="112" height="18" border="0" align="absmiddle"></a><img src="/images/transparent.gif" width="212" height="1">
            <input name="Continue" type="image" value="Submit" src="/images/continue-checkout-red.gif" alt="Continue Checkout" align="middle" width="174" height="25" border="0" onClick="return submitpickup()">
            <img src="/images/transparent.gif" width="90" height="1"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td colspan="3"><img src="/images/transparent.gif" width="1" height="20"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="7" bgcolor="#CC1C0F"><img src="/images/bottombar2.gif" width="760" height="9"></td>
    </tr>
  </table></form>
</td>
    <td width="10" height="622"></td>
  </tr>
  <tr>
    <td width="8" height="1"></td>
    <td width="12" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="2" height="1" bgcolor="#FFFFFF"></td>
    <td width="6" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="8" height="1" bgcolor="#FFFFFF"></td>
    <td width="126" height="1" bgcolor="#FFFFFF"></td>
    <td width="15" height="1" bgcolor="#FFFFFF"></td>
    <td width="8" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="8" height="1" bgcolor="#FFFFFF"></td>
    <td width="141" height="1" bgcolor="#FFFFFF"></td>
    <td width="13" height="1" bgcolor="#FFFFFF"></td>
    <td width="10" height="1"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="13" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td width="762" height="49" colspan="13" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="13" valign="top"><IMG SRC="/images/page-bottom.gif" WIDTH=762 HEIGHT=7></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="15"></td>
  </tr>
  <tr>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="12" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="12" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="2" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="2" height="1"></td>
    <td width="6" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="6" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="126" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="126" height="1"></td>
    <td width="15" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="15" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="13" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="13" height="1"></td>
    <td width="10" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="10" height="1"></td>
  </tr>
</table>
</BODY>
</HTML>