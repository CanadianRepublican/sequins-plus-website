<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" --><%
OrderID = request("oid")
OrderName = request("oname")
  set SPreciept = Server.CreateObject("ADODB.Recordset")
  SPreciept.ActiveConnection = SPdbString
  SPreciept.Source = "SELECT * FROM spOrders WHERE id=" & OrderID
  SPreciept.CursorType = 0
  SPreciept.CursorLocation = 2
  SPreciept.LockType = 3
  SPreciept.Open
  Buyer = SPreciept.Fields.Item("SPuser").Value
  set SPuser = Server.CreateObject("ADODB.Recordset")
  SPuser.ActiveConnection = SPdbString
  SPuser.Source = "SELECT * FROM spCartUser WHERE ID_BUYER=" & Session("MM_UserID")
  SPuser.CursorType = 0
  SPuser.CursorLocation = 2
  SPuser.LockType = 3
  SPuser.Open
   set SPrdetails = Server.CreateObject("ADODB.Recordset")
  SPrdetails.ActiveConnection = SPdbString
  SPrdetails.Source = "SELECT * FROM spOrderDetails WHERE OrderID=" & OrderID
  SPrdetails.CursorType = 0
  SPrdetails.CursorLocation = 2
  SPrdetails.LockType = 3
  SPrdetails.Open %>
<html>
<head>
<title>Sequins Plus Online - Receipt for Order# <%=SPreciept.Fields.Item("OrderNumber").Value%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="671" border="0" align="center" cellpadding="0" cellspacing="0" background="/images/form_bg.gif">
  <tr> 
    <td width="210"><img src="/images/form-tile.gif" width="210" height="109"></td>
    <td width="281"><img src="/images/form_logo.gif" width="281" height="109"></td>
    <td width="704"><img src="/images/form-tile.gif" width="181" height="109"></td>
  </tr>
  <tr> 
    <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td colspan="7"><table width="671" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="185" align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
            Order Name :</strong></font></td>
          <td width="100" align="left" valign="bottom" nowrap><img src="/images/transparent.gif" width="5" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPreciept.Fields.Item("Name").Value%></font></td>
          <td align="right" valign="bottom" nowrap><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
            Order Date & Time :</strong></font></td>
          <td width="256" align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPreciept.Fields.Item("DateTxT").Value%></font></td>
        </tr>
        <tr> 
          <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
            Order Number :</strong></font></td>
          <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPreciept.Fields.Item("OrderNumber").Value%></font></td>
          <td width="84" align="right" valign="bottom" nowrap><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Order 
            Key :</strong></font></td>
          <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPreciept.Fields.Item("Key").Value%></font></td>
        </tr>
        <tr> 
          <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td align="right" valign="bottom">&nbsp;</td>
          <td align="left" valign="bottom">&nbsp;</td>
          <td align="right" valign="bottom"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
            Order Status :</strong></font></td>
          <td align="left" valign="bottom"><img src="/images/transparent.gif" width="5" height="1"><strong><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><%=SPreciept.Fields.Item("Status").Value%></font></strong><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></font></td>
        </tr>
        <tr> 
          <td height="10" colspan="4"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td colspan="4" align="center" valign="bottom"> 
            <table width="550" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
                <td height="20" align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Personal 
                  Information:</strong></font></td>
                <td height="20" align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong> 
                  <%if SPreciept.Fields.Item("PickupAtStore").Value <> "" then%>
                  Pickup at <%=SPreciept.Fields.Item("PickupAtStore").Value%> Store 
                  <%else%>
                  Shipping Information: 
                  <%end if%>
                  </strong></font></td>
              </tr>
              <tr valign="top"> 
                <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPuser.Fields.Item("FirstName").Value&" "%> 
                  <%if SPuser.Fields.Item("Initial").Value <> "" then%>
                  <%=SPuser.Fields.Item("Initial").Value&" "%> 
                  <%end if%>
                  <%=SPuser.Fields.Item("LastName").Value%><br>
                  <%if SPuser.Fields.Item("Company").Value <> "" then%><%=SPuser.Fields.Item("Company").Value%><br><%end if%>
				  <%if SPuser.Fields.Item("Apt").Value <> "" then%><%=SPuser.Fields.Item("Apt").Value%> - <%end if%><%=SPuser.Fields.Item("Address1").Value%><br>
                  <%if SPuser.Fields.Item("Address2").Value <> "" then%>
                  <%=SPuser.Fields.Item("Address2").Value%><br>
                  <%end if%>
                  <%=SPuser.Fields.Item("City").Value%>, <%=SPuser.Fields.Item("Province").Value&" "%> <%=SPuser.Fields.Item("Country").Value%><br>
                  <%=SPuser.Fields.Item("Postal").Value%><br>
                  (<%=SPuser.Fields.Item("PhoneAC").Value%>) <%=SPuser.Fields.Item("Phone3").Value%> - <%=SPuser.Fields.Item("Phone4").Value%><%if SPuser.Fields.Item("Ext").Value <> "" then%> ext. <%=SPuser.Fields.Item("Ext").Value%><%end if%></font></td>
                <td align="center"> 
                  <%if SPreciept.Fields.Item("PickupAtStore").Value <> "" then
			if SPreciept.Fields.Item("PickupAtStore").Value = "Whitby" then%>
                  <font color="#000000" size="2" face="Arial, Helvetica, sans-serif"> 
                  <strong>Sequins Plus Whitby</strong></font><br>
                  <font color="#616161" size="2" face="Arial, Helvetica, sans-serif">856 Brock Street North<br>
                  Whitby, Ontario<br>
                  Canada. L1H 6C6<br>
                  (905) 665-3006</font> 
                  <%else%>
                  <font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Sequins 
                  Plus Oshawa</strong></font><br>
                  <font color="#616161" size="2" face="Arial, Helvetica, sans-serif">377 Wilson Road South<br>
                  Oshawa, Ontario<br>
                  Canada. L1H 6C6 <br>
                  (905 433-2319)</font> 
                  <%end if
					else%>
                  <font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPreciept.Fields.Item("ShippingFirstName").Value&" "%> 
                  <%if SPreciept.Fields.Item("ShippingInitial").Value <> "" then%>
                  <%=SPreciept.Fields.Item("ShippingInitial").Value&" "%> 
                  <%end if%>
                  <%=SPreciept.Fields.Item("ShippingLastName").Value%><br>
                  <%if SPreciept.Fields.Item("ShippingCompanyName").Value <> "" then%><%=SPreciept.Fields.Item("ShippingCompanyName").Value%><br><%end if%>
                  <%if SPreciept.Fields.Item("ShippingApt").Value <> "" then%><%=SPreciept.Fields.Item("ShippingApt").Value%> - <%end if%><%=SPreciept.Fields.Item("ShippingAddress1").Value%><br>
                  <%if SPreciept.Fields.Item("ShippingAddress2").Value <> "" then%>
                  <%=SPreciept.Fields.Item("ShippingAddress2").Value%><br>
                  <%end if%>
                  <%=SPreciept.Fields.Item("ShippingCity").Value%>, <%=SPreciept.Fields.Item("ShippingState").Value&" "%> <%=SPreciept.Fields.Item("ShippingCountry").Value%><br>
                  <%=SPreciept.Fields.Item("ShippingPostal").Value%><br>
                  (<%=SPreciept.Fields.Item("ShippingPhoneAC").Value%>) <%=SPreciept.Fields.Item("ShippingPhone3").Value%> - <%=SPreciept.Fields.Item("ShippingPhone4").Value%><%if SPreciept.Fields.Item("ShippingExt").Value <> "" then%> ext. <%=SPreciept.Fields.Item("ShippingExt").Value%> <%end if %>
                  <%end if%>
                  </font></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="2" colspan="7"><img src="/images/black.gif" width="671" height="2"></td>
  </tr>
  <tr>
    <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="10" colspan="7"><table width="671" border="0" cellpadding="0" cellspacing="1">
        <tr> 
          <td colspan="2" align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Product 
            Description</strong></font></td>
          <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Size 
            Type</strong></font></td>
          <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Size</strong></font></td>
          <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Colour</strong></font></td>
          <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Price</strong></font></td>
          <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Qty</strong></font></td>
          <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Total</strong></font></td>
        </tr>
        <%While NOT (SPrdetails.EOF OR SPrdetails.EOF)%>
        <tr> 
          <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPrdetails.Fields.Item("ItemName").Value%></font></td>
          <td align="left" nowrap><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
            #: <%=SPrdetails.Fields.Item("ItemProductCode").Value%></font></td>
          <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPrdetails.Fields.Item("ItemSizeType").Value%></font></td>
          <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPrdetails.Fields.Item("ItemSize").Value%></font></td>
          <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><%=SPrdetails.Fields.Item("ItemColour").Value%></font></td>
          <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000"><%= "Call Now"%></font></font></td>
          <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><%=SPrdetails.Fields.Item("ItemQuantity").Value%></font></td>
          <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F"><%= "Call Now"%></font></strong></font></td>
        </tr>
        <%SPrdetails.movenext()
		wend%>
        <td colspan="8"><img src="/images/transparent.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td height="10"></td>
          <td height="10"></td>
          <td height="10"></td>
          <td height="10"></td>
          <td height="10" colspan="2" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Subtotal 
            :</strong></font></td>
          <% ' ---- tax calc ----

Dim vTax
Dim vTaxTotal
Dim vTaxRate

vTax = 0.00
vTaxRate = 0.15

vTaxTotal = (1.0 + vTaxRate) * (SPreciept.Fields.Item("SubTotal").Value)
vTax = vTaxTotal - SPreciept.Fields.Item("SubTotal").Value
if SPreciept.Fields.Item("ShippingAmmount").Value <> "0" then
vTaxTotal = vTaxTotal + SPreciept.Fields.Item("ShippingAmmount").Value
end if
%><td height="10" colspan="2" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><%= "Call Now"%></strong></font></td>
        </tr>
        <tr> 
          <td rowspan="2"></td>
          <td rowspan="2"></td>
          <td rowspan="2"></td>
          <td rowspan="2"></td>
          <td height="10" colspan="2" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Taxes 
            :</strong></font></td>
          <td height="10" colspan="2" align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><%= vTax%></strong></font></td>
        </tr>
        <tr> 
          <%if SPreciept.Fields.Item("ShippingAmmount").Value <> "0" then%><td height="10" colspan="2" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Shipping 
            :</strong></font></td>
          <td height="10" colspan="2" align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><%= "Call Now"%></strong></font></td><%end if%>
        </tr>
        <tr> 
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td height="10" colspan="2" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Total 
            :</strong></font></td>
          <td height="10" colspan="2" align="right"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong><%= vTaxTotal%></strong></font></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr> 
    <td height="2" colspan="7"><img src="/images/black.gif" width="671" height="2"></td>
  </tr>
  <tr>
    <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
  <tr align="center"> 
    <td height="73" colspan="7"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Sequins 
      Plus Dance Supplies &#8226; 377 Wilson Road South, Oshawa, ON L1H 6C6 &#8226; 
      (905) 433 - 2319</font></td>
  </tr>
  <tr>
    <td height="10" colspan="7"><img src="/images/transparent.gif" width="1" height="10"></td>
  </tr>
</table><%
SPreciept.Close()
SPrdetails.Close()
SPuser.Close()
%><script language="JavaScript" type="text/JavaScript">
if (window.self) window.print();
</script>
</body>
</html>