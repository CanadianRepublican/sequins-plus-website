<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
Response.Expires=-1000
Response.CacheControl="no-cache"
%>
<!--#include virtual="inc/cart.asp" -->
<%
' *** Add item to UC Shopping cart
UC_editAction = CStr(Request("URL"))
If (Request.QueryString <> "") Then
  UC_editAction = UC_editAction & "?" & Request.QueryString
End If
UC_recordId = CStr(Request.Form("UC_recordId"))
If (Request.Form("UC_recordId").Count = 1) Then
  UC_uniqueCol=""
  UC_redirectPage = "/en/order/cart.asp"
  UC_BindingTypes=Array("FORM","FORM","FORM","FORM","FORM","FORM","FORM","FORM","FORM","FORM","FORM","FORM","NONE")
  UC_BindingValues=Array("Special","SizeType","SizeTypeID","Size","SizeID","Colour","ColourID","ProductCode","ProductID","Quantity","Name","Price","")
  UCCart1.AddItem UC_rs,UC_BindingTypes,UC_BindingValues,"increment"
  ' redirect with URL parameters
  If (UC_redirectPage <> "") Then
    If (InStr(1, UC_redirectPage, "?", vbTextCompare) = 0 And Request.QueryString <> "") Then
      UC_redirectPage = UC_redirectPage & "?" & Request.QueryString
    End If
    Call Response.Redirect(UC_redirectPage)
  End If
End If
%>