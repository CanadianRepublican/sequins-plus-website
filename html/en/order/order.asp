<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%><!--#include virtual="DB/spDB.asp" -->
<!--#include virtual="inc/cart.asp" -->
<!--#include virtual="inc/date.asp" --><%
MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If%><%
Function WishListLength(UserID)
Set WLlength = Server.CreateObject("ADODB.Recordset")
WLlength.ActiveConnection = SPdbString
WLlength.Source = "SELECT * FROM spWishlist Where UserID = "&UserID
WLlength.CursorType = 0
WLlength.CursorLocation = 2
WLlength.LockType = 3
WLlength.Open()
wishlength = 0
while NOT (WLlength.BOF OR WLlength.EOF)
wishlength = wishlength + 1
WLlength.MoveNext
wend
WLlength.Close()
WishListLength = wishlength
End Function
' *** Restrict Access To Page: Grant or deny access to this page
MM_authorizedUsers=""
MM_authFailedURL="/en/order/login.asp"
MM_grantAccess=false
If Session("MM_SPshopping") <> "" Then
  If (true Or CStr(Session("MM_UserAuthorization"))="") Or _
         (InStr(1,MM_authorizedUsers,Session("MM_UserAuthorization"))>=1) Then
    MM_grantAccess = true
  End If
End If
If Not MM_grantAccess Then
  MM_qsChar = "?"
  If (InStr(1,MM_authFailedURL,"?") >= 1) Then MM_qsChar = "&"
  MM_referrer = Request.ServerVariables("URL")
  if (Len(Request.QueryString()) > 0) Then MM_referrer = MM_referrer & "?" & Request.QueryString()
  MM_authFailedURL = MM_authFailedURL & MM_qsChar & "accessdenied=" & Server.URLEncode(MM_referrer)
  Response.Redirect(MM_authFailedURL)
End If
%><HTML>
<HEAD>
<TITLE>Sequins Plus Online - Shopping Cart Checkout - Step One</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<!--#include virtual="inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR=#DFDFDF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="MM_preloadImages('/images/cartbut_1_over.gif','/images/cartbut_2_over.gif','/images/cartbut_3_over.gif','/images/cartbut_4_over.gif','/images/cartbut_5_over.gif','/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="4"></td>
    <td width="281" height="28" colspan="4" valign="top"><IMG SRC="/images/logo_top.gif" WIDTH=281 HEIGHT=28></td>
    <td width="336" height="28" colspan="7"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="3" valign="top"><IMG SRC="/images/logo_lft_top_line.gif" WIDTH=155 HEIGHT=9></td>
    <td width="281" height="9" colspan="4" valign="top"><IMG SRC="/images/logo_topline.gif" WIDTH=281 HEIGHT=9></td>
    <td width="326" height="9" colspan="6" valign="top"><IMG SRC="/images/logo_rt_top_line.gif" WIDTH=326 HEIGHT=9></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
    <td width="155" height="26" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" colspan="4" valign="top"><IMG SRC="/images/logo_title.gif" WIDTH=281 HEIGHT=26></td>
    <td width="326" height="26" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td width="155" height="11" colspan="3" valign="top"><IMG SRC="/images/logo_lft_bot_line.gif" WIDTH=155 HEIGHT=11></td>
    <td width="281" height="11" colspan="4" valign="top"><IMG SRC="/images/logo_bot-line.gif" WIDTH=281 HEIGHT=11></td>
    <td width="326" height="11" colspan="6" valign="top"><IMG SRC="/images/logo_rt_bot_line.gif" WIDTH=326 HEIGHT=11></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="155" height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="281" height="14" colspan="4" valign="top"><IMG SRC="/images/logo_beige.gif" WIDTH=281 HEIGHT=14></td>
    <td width="326" height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" colspan="4" valign="top"><IMG SRC="/images/logo_spacer.gif" WIDTH=281 HEIGHT=2></td>
    <td width="326" height="2" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="19"></td>
    <td width="155" height="19" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="19" colspan="4" valign="top"><IMG SRC="/images/cartpage_22.gif" WIDTH=281 HEIGHT=19></td>
    <td width="326" height="19" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="19"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="762" height="5" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="20"></td>
    <td width="12" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut1','','/images/cartbut_1_over.gif',1)"><img src="/images/cartbut_1.gif" alt="homepage" name="cartbut1" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut2','','/images/cartbut_2_over.gif',1)"><img src="/images/cartbut_2.gif" alt="Order Status" name="cartbut2" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" colspan="2" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut3','','/images/cartbut_3_over.gif',1)"><img src="/images/cartbut_3.gif" alt="Create A New Account" name="cartbut3" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut4','','/images/cartbut_4_over.gif',1)"><img src="/images/cartbut_4.gif" alt="Login" name="cartbut4" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cartbut5','','/images/cartbut_5_over.gif',1)"><img src="/images/cartbut_5.gif" alt="My Account Details" name="cartbut5" width="141" height="20" border="0"></a></td>
    <td width="13" height="20" bgcolor="#FFFFFF"></td>
    <td width="10" height="20"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="173"></td>
    <td width="762" height="173" colspan="13" align="center" valign="top" bgcolor="#FFFFFF"> 
      <table width="700" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
        <tr> 
      <td colspan="7"><img src="/images/topbar.gif" width="700" height="9"></td>
    </tr>
    <tr> 
      <td colspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="25" height="1"><font color="#FFFFFF" size="4" face="Arial, Helvetica, sans-serif">&#8226; 
        Cart Checkout - <font size="3"><strong>Step 1 (1 of 3) Order Confirmation</strong></font></font></td>
    </tr>
    <tr> 
      <td colspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td width="1" rowspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
      <td colspan="5"><img src="/images/transparent.gif" width="1" height="10"></td>
      <td width="1" rowspan="7" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td width="10"><img src="/images/transparent.gif" width="10" height="8"></td>
      <td colspan="3"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">You 
        have choosen to order the items listed below :</font></td>
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
    </tr>
    <tr> 
      <td height="5"><img src="/images/transparent.gif" width="1" height="5"></td>
          <td height="5" colspan="3"><img src="/images/transparent.gif" width="1" height="5"></td>
      <td height="5"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td width="17" valign="top"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">(i)<br>
          (ii)<br><br><br>
          (iii)</font></td>
      <td width="10"><img src="/images/transparent.gif" width="10" height="1"></td>
      <td width="651"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"> 
          Please note that all prices are in Canadian dollars.<br>
          Shipping is an approximate value. After you place your order a Sequins 
          Plus representative will contact you to verify payment and arrange shipping 
          method. If you would like, you can pick up your order at one of our 
          store locations.<br>
          Verify that your order is correct and then click the 'Continue Checkout' 
          button.</font></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="20" colspan="5"><img src="/images/transparent.gif" width="1" height="20"></td>
    </tr>
    <tr> 
      <td height="2"><img src="/images/transparent.gif" width="1" height="2"></td>
      <td height="2" colspan="3" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="2"></td>
      <td height="2"><img src="/images/transparent.gif" width="1" height="2"></td>
    </tr>
    <tr> 
      <td height="5" colspan="5"><img src="/images/transparent.gif" width="1" height="5"></td>
    </tr>
  </table>
    </td>
    <td width="10" height="173"></td>
  </tr>
  <tr>
    <td width="8" height="463"></td>
    <td width="762" height="463" colspan="13" align="center" valign="top" bgcolor="#FFFFFF"> 
      <table width="700" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
    <tr> 
      <td rowspan="6" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
      <td><img src="/images/transparent.gif" width="10" height="1"></td>
      <td><table width="678" border="0" cellspacing="1" cellpadding="0">
            <tr> 
              <td colspan="2" align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Product 
                Description</strong></font></td>
              <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Size 
                Type</strong></font></td>
              <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Size</strong></font></td>
              <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Colour</strong></font></td>
              <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Price</strong></font></td>
              <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Qty</strong></font></td>
              <td align="center"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Total</strong></font></td>
            </tr>
            <tr> 
              <td colspan="8"><img src="/images/transparent.gif" width="1" height="10"></td>
            </tr>
            <tr> 
              <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Capezio 
                Maroon Silk Body Suit</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
                #: 520453</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Girls</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">23a</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Blue</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$169.99</font></font></td>
              <td align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">1</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">$1,669.99</font></strong></font></td>
            </tr>
            <tr> 
              <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Capezio 
                Maroon Silk Body Suit</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
                #: 520453</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Girls</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">23a</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Blue</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$169.99</font></font></td>
              <td align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">1</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">$1,669.99</font></strong></font></td>
            </tr>
            <tr> 
              <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Capezio 
                Maroon Silk Body Suit</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
                #: 520453</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Girls</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">23a</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Blue</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$169.99</font></font></td>
              <td align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">1</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">$1,669.99</font></strong></font></td>
            </tr>
            <tr> 
              <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Capezio 
                Maroon Silk Body Suit</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
                #: 520453</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Girls</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">23a</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Blue</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$169.99</font></font></td>
              <td align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">1</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">$1,669.99</font></strong></font></td>
            </tr>
            <tr> 
              <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Capezio 
                Maroon Silk Body Suit</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
                #: 520453</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Girls</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">23a</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Blue</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$169.99</font></font></td>
              <td align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">1</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">$1,669.99</font></strong></font></td>
            </tr>
            <tr> 
              <td align="left"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Capezio 
                Maroon Silk Body Suit</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Catalog 
                #: 520453</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Girls</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">23a</font></td>
              <td align="center"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Blue</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000">$169.99</font></font></td>
              <td align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">1</font></td>
              <td align="right"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">$1,669.99</font></strong></font></td>
            </tr>
            <tr> 
              <td colspan="8"><img src="/images/transparent.gif" width="1" height="10"></td>
            </tr>
            <tr> 
              <td colspan="6" align="right" bgcolor="#ececec"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Subtotal 
                :</strong></font></td>
              <td colspan="2" align="right" bgcolor="#ececec"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong>$5,204.95</strong></font></td>
            </tr>
            <tr> 
              <td colspan="6" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">**</font> 
                Shipping :</strong></font></td>
                <td colspan="2" align="right"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>$35.00</strong></font></td>
            </tr>
            <tr> 
              <td colspan="6" align="right" bgcolor="#ececec"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>Taxes 
                :</strong></font></td>
              <td colspan="2" align="right" bgcolor="#ececec"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong>$204.65</strong></font></td>
            </tr>
            <tr> 
              <td height="5" colspan="8"><img src="/images/transparent.gif" width="1" height="5"></td>
            </tr>
            <tr> 
              <td height="2" colspan="8" bgcolor="#000000"><img src="/images/transparent.gif" width="1" height="2"></td>
            </tr>
            <tr> 
              <td height="5" colspan="8"><img src="/images/transparent.gif" width="1" height="5"></td>
            </tr>
            <tr> 
              <td colspan="6" align="right"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>Total 
                :</strong></font></td>
              <td colspan="2" align="right"><font color="#CC1C0F" size="3" face="Arial, Helvetica, sans-serif"><strong>$5,409.99</strong></font></td>
            </tr>
          </table></td>
      <td><img src="/images/transparent.gif" width="10" height="1"></td>
      <td rowspan="6" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="1"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td height="20"><img src="/images/transparent.gif" width="1" height="20"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><table width="678" border="0" cellpadding="5" cellspacing="1" bgcolor="#CECECE">
            <tr bgcolor="#ececec"> 
              <td><b><font color="#000000" size="2" face="Arial, Helvetica, sans-serif"><strong><font color="#CC1C0F">**</font> 
                SHIPPING <font color="#CC1C0F">**</font></strong></font></b><img src="/images/transparent.gif" width="10" height="1">
                <input name="" type="checkbox" value="">
                <img src="/images/transparent.gif" width="5" height="1"><font color="#000000" size="2" face="Arial, Helvetica, sans-serif">Check 
                this box if you would like to pick up your order at one of our 
                store locations.</font></td>
			<tr bgcolor="#ececec">
              <td align="center">
<textarea name="textfield" cols="60" rows="5">Type your message or special request here.</textarea>
            </table></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><img src="/images/transparent.gif" width="1" height="20"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><img src="/images/transparent.gif" width="90" height="1"><a href="/en/order/cart.asp"><img src="/images/cancel-order.gif" alt="Cancel this order and continue shopping" width="112" height="18" border="0" align="absmiddle"></a><img src="/images/transparent.gif" width="212" height="1"><a href="/en/order/order2.asp"><img src="/images/continue-checkout-red.gif" alt="Continue Checkout" width="174" height="25" border="0" align="absmiddle"></a><img src="/images/transparent.gif" width="90" height="1"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td><img src="/images/transparent.gif" width="1" height="20"></td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="5" bgcolor="#CC1C0F"><img src="/images/bottombar.gif" width="700" height="9"></td>
    </tr>
  </table>
    </td>
    <td width="10" height="463"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="13" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td width="762" height="49" colspan="13" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif">Home 
      | About Us | Products | Services | Contact | Sitemap | Disclaimer</font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="13" valign="top"><IMG SRC="/images/page-bottom.gif" WIDTH=762 HEIGHT=7></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="15"></td>
  </tr>
  <tr>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="12" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="12" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="2" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="2" height="1"></td>
    <td width="6" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="6" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="126" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="126" height="1"></td>
    <td width="15" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="15" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="8" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="8" height="1"></td>
    <td width="141" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="141" height="1"></td>
    <td width="13" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="13" height="1"></td>
    <td width="10" height="1" valign="top"><img src="/images/transparent.gif" alt="" width="10" height="1"></td>
  </tr>
</table>
</BODY>
</HTML>