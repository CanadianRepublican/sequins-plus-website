<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<!--#include virtual="sequinsplus/inc/cart.asp" -->
<!--#include virtual="sequinsplus/inc/date.asp" -->
<% ' Redirect if logged on already
	If Len(Session("MM_UserID"))<>0  AND CStr(Request.QueryString("accessdenied"))="" Then
		Response.Redirect("/en/order/cart.asp")
	End If
%>
<% ' Redirect if new user
        If Request.Form("rB") = CStr("New") Then
		RedirURL = "/en/order/new.asp"
		If Request.QueryString("accessdenied") <> "" Then RedirURL = RedirURL  + "?" + Request.QueryString
                Response.Redirect (RedirURL)
        End If
%>
<%
' *** Validate request to log in to this site.
MM_LoginAction = Request.ServerVariables("URL")
If Request.QueryString<>"" Then MM_LoginAction = MM_LoginAction + "?" + Request.QueryString
MM_valUsername=CStr(Request.Form("UserName"))
If MM_valUsername <> "" Then
  MM_fldUserAuthorization=""
  MM_redirectLoginSuccess="/en/order/cart.asp"

  MM_redirectLoginFailed="/en/order/login.asp"
  If Request.QueryString <> "" Then MM_redirectLoginFailed = MM_redirectLoginFailed + "?" + Request.QueryString
  If Request.QueryString("authfail")="" Then
	  If (InStr(1,MM_redirectLoginFailed,"?") >= 1) Then 
		MM_redirectLoginFailed = MM_redirectLoginFailed + "&" + "authfail=1"
	  Else 
		MM_redirectLoginFailed = MM_redirectLoginFailed + "?" + "authfail=1"
	  End If
  End If

  MM_flag="ADODB.Recordset"
  set MM_rsUser = Server.CreateObject(MM_flag)
  MM_rsUser.ActiveConnection = SPdbString
  MM_rsUser.Source = "SELECT * "
  If MM_fldUserAuthorization <> "" Then MM_rsUser.Source = MM_rsUser.Source & "," & MM_fldUserAuthorization
  MM_rsUser.Source = MM_rsUser.Source & " FROM spCartUser WHERE Login='" & MM_valUsername &"' AND Password='" & CStr(Request.Form("Password")) & "'"
  MM_rsUser.CursorType = 0
  MM_rsUser.CursorLocation = 2
  MM_rsUser.LockType = 3
  MM_rsUser.Open
  If Not MM_rsUser.EOF Or Not MM_rsUser.BOF Then 
    ' username and password match - this is a valid user
    Session("MM_SPshopping") = MM_valUsername
    Session("MM_UserID") = CStr(MM_rsUser.Fields.Item("ID_BUYER").Value)

    If (MM_fldUserAuthorization <> "") Then
      Session("MM_UserAuthorization") = CStr(MM_rsUser.Fields.Item(MM_fldUserAuthorization).Value)
    Else
      Session("MM_UserAuthorization") = ""
    End If
    if CStr(Request.QueryString("accessdenied")) <> "" And true Then
      MM_redirectLoginSuccess = Request.QueryString("accessdenied")
    End If
    MM_rsUser.Close
    Response.Redirect(MM_redirectLoginSuccess)
  End If
  MM_rsUser.Close
  Response.Redirect(MM_redirectLoginFailed)
End If
%><%
Set RSquickcat = Server.CreateObject("ADODB.Recordset")
RSquickcat.ActiveConnection = SPdbString
RSquickcat.Source = "SELECT spItemCatagory.id, spItemCatagory.Name FROM spItemCatagory ORDER BY spItemCatagory.Name ASC"
RSquickcat.CursorType = 0
RSquickcat.CursorLocation = 2
RSquickcat.LockType = 3
RSquickcat.Open()
%>
<script runat=SERVER language=VBSCRIPT>					
function DoDateTime(str, nNamedFormat, nLCID)				
	dim strRet								
	dim nOldLCID								
										
	strRet = str								
	If (nLCID > -1) Then							
		oldLCID = Session.LCID						
	End If									
										
	On Error Resume Next							
										
	If (nLCID > -1) Then							
		Session.LCID = nLCID						
	End If									
										
	If ((nLCID < 0) Or (Session.LCID = nLCID)) Then				
		strRet = FormatDateTime(str, nNamedFormat)			
	End If									
										
	If (nLCID > -1) Then							
		Session.LCID = oldLCID						
	End If									
										
	DoDateTime = strRet							
End Function									
</script>
<HTML>
<HEAD>
<TITLE>Sequins Plus Online - Shopping Cart Login</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<!--#include virtual="sequinsplus/inc/ClientFunctions.asp" -->
<link href="/main.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR=#DFDFDF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="MM_preloadImages('/images/cartbut_1_over.gif','/images/cartbut_2_over.gif','/images/cartbut_3_over.gif','/images/cartbut_4_over.gif','/images/cartbut_5_over.gif','/images/top_home_over.gif','/images/top_cart_over.gif','/images/top_help_over.gif')">
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="163" height="28" colspan="4"></td>
    <td width="281" height="28" colspan="4" valign="top"><IMG SRC="/images/logo_top.gif" WIDTH=281 HEIGHT=28></td>
    <td width="336" height="28" colspan="7" valign="top"></td>
  </tr>
  <tr>
    <td width="8" height="9"></td>
    <td width="155" height="9" colspan="3" valign="top"><IMG SRC="/images/logo_lft_top_line.gif" WIDTH=155 HEIGHT=9></td>
    <td width="281" height="9" colspan="4" valign="top"><IMG SRC="/images/logo_topline.gif" WIDTH=281 HEIGHT=9></td>
    <td width="326" height="9" colspan="6" valign="top"><IMG SRC="/images/logo_rt_top_line.gif" WIDTH=326 HEIGHT=9></td>
    <td width="10" height="9"></td>
  </tr>
  <tr>
    <td width="8" height="26"></td>
    <td width="155" height="26" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF" class="uhoDate"><font size="-2" face="Arial, Helvetica, sans-serif"><%=FormatDate("l") & ", " & FormatDate("F ")& FormatDate("j")& FormatDate("S ")& FormatDate("Y")%></font></td>
    <td width="281" height="26" colspan="4" valign="top"><IMG SRC="/images/logo_title.gif" WIDTH=281 HEIGHT=26></td>
    <td width="326" height="26" colspan="6" bgcolor="#FFFFFF"><img src="/images/transparent.gif" width="190" height="26"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','/images/top_home_over.gif',1)"><img src="/images/top_home.gif" alt="Homepage" name="home" width="35" height="26" border="0"></a><a href="<%If MM_grantAccess = true then 
	spcarturl = "/en/order/default.asp" 
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() = 0 then
	spcarturl = "/en/order/login.asp"
	end if
	if MM_grantAccess = false AND UCCart1.GetItemCount() >= 1 then
	spcarturl = "/en/order/cart.asp"
	end if
	%><%=spcarturl%>" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('cart','','/images/top_cart_over.gif',1)"><img src="/images/top_cart.gif" alt="Shopping Cart" name="cart" width="35" height="26" border="0"></a><a href="javascript:Help()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help','','/images/top_help_over.gif',1)"><img src="/images/top_help.gif" alt="Help?" name="help" width="35" height="26" border="0"></a></td>
    <td width="10" height="26"></td>
  </tr>
  <tr>
    <td width="8" height="11"></td>
    <td width="155" height="11" colspan="3" valign="top"><IMG SRC="/images/logo_lft_bot_line.gif" WIDTH=155 HEIGHT=11></td>
    <td width="281" height="11" colspan="4" valign="top"><IMG SRC="/images/logo_bot-line.gif" WIDTH=281 HEIGHT=11></td>
    <td width="326" height="11" colspan="6" valign="top"><IMG SRC="/images/logo_rt_bot_line.gif" WIDTH=326 HEIGHT=11></td>
    <td width="10" height="11"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="155" height="14" colspan="3" bgcolor="#DFC6A3"></td>
    <td width="281" height="14" colspan="4" valign="top"><IMG SRC="/images/logo_beige.gif" WIDTH=281 HEIGHT=14></td>
    <td width="326" height="14" colspan="6" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="2"></td>
    <td width="155" height="2" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="2" colspan="4" valign="top"><IMG SRC="/images/logo_spacer.gif" WIDTH=281 HEIGHT=2></td>
    <td width="326" height="2" colspan="6" bgcolor="#FFFFFF"></td>
    <td width="10" height="2"></td>
  </tr>
  <tr>
    <td width="8" height="19"></td>
    <td width="155" height="19" colspan="3" bgcolor="#FFFFFF"></td>
    <td width="281" height="19" colspan="4" valign="top"><IMG SRC="/images/cartpage_22.gif" WIDTH=281 HEIGHT=19></td>
    <td width="326" height="19" colspan="6" valign="top" bgcolor="#FFFFFF"><img src="/images/red_arrow.gif" width="17" height="17" align="absmiddle"><img src="/images/transparent.gif" width="5" height="1"><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif"><strong>Shopping 
  Cart Items:</strong></font> <%=UCCart1.GetItemCount()%> <strong><font color="#CC1C0F" size="2" face="Arial, Helvetica, sans-serif">Subtotal:</font></strong> 
      $<%=FormatNumber(UCCart1.GetColumnTotal("Total"), 2, -2, -2, -1)%></td>
    <td width="10" height="19"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="762" height="5" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="20"></td>
    <td width="12" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="/en/default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('homepage','','/images/cartbut_1_over.gif',1)"><img src="/images/cartbut_1.gif" alt="Homepage" name="homepage" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" colspan="2" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('check','','/images/cartbut_2_over.gif',1)"><img src="/images/cartbut_2.gif" alt="Check Your Order Status" name="check" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" colspan="2" valign="top"><a href="/en/order/new.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('create','','/images/cartbut_3_over.gif',1)"><img src="/images/cartbut_3.gif" alt="Create A New Account" name="create" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('signin','','/images/cartbut_4_over.gif',1)"><img src="/images/cartbut_4.gif" alt="Sign In" name="signin" width="141" height="20" border="0"></a></td>
    <td width="8" height="20" bgcolor="#FFFFFF"></td>
    <td width="141" height="20" valign="top"><a href="default.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('account','','/images/cartbut_5_over.gif',1)"><img src="/images/cartbut_5.gif" alt="Account Services" name="account" width="141" height="20" border="0"></a></td>
    <td width="13" height="20" bgcolor="#FFFFFF"></td>
    <td width="10" height="20"></td>
  </tr>
  <tr>
    <td width="8" height="6"></td>
    <td width="762" height="6" colspan="13" bgcolor="#FFFFFF"></td>
    <td width="10" height="6"></td>
  </tr>
  <tr>
    <td width="8" height="375"></td>
    <td width="153" height="380" colspan="2" rowspan="2" valign="top" bgcolor="#FFFFFF"> 
  <table width="153" border="0" cellpadding="0" cellspacing="0" background="/images/list_border.gif" mm_noconvert="TRUE">
    <tr> 
          <td valign="bottom"><img src="/images/nav_top.gif" width="153" height="14"></td>
    </tr>
    <tr> 
      <td height="2" valign="bottom"><img src="/images/transparent.gif" width="1" height="2"></td>
    </tr>
    <tr> 
      <td align="center"><img src="/images/dancer.gif" width="55" height="350"></td>
    </tr>
    <tr> 
      <td height="14" valign="top"><img src="/images/list_bottom.gif" width="153" height="14"></td>
    </tr>
  </table>
</td>
    <td width="2" height="375" bgcolor="#FFFFFF"></td>
    <td width="6" height="375" bgcolor="#FFFFFF"></td>
    <td width="601" height="375" colspan="9" valign="top" bgcolor="#FFFFFF"> 
  <table width="601" border="0" cellpadding="0" cellspacing="0" mm_noconvert="TRUE">
        <tr>
      <td height="10" colspan="2" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
      <td bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="10" height="1"><font color="#FFFFFF" size="5" face="Arial, Helvetica, sans-serif">&#8226; 
        Sign In</font></td>
      <form name="jumpmenu" onSubmit="return jump(document.jumpmenu.menu.options[document.jumpmenu.menu.options.selectedIndex].value)"> <td align="right" valign="top" bgcolor="#CC1C0F"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font>
            <select align="middle" name="menu" >
                <option value="none" selected>More Categories</option><% While (NOT RSquickcat.EOF) %>
          <option value="<%="/en/products/default.asp?Cat="&(RSquickcat.Fields.Item("id").Value)%>"  ><%=(RSquickcat.Fields.Item("Name").Value)%></option>
          <% 
		  RSquickcat.MoveNext()
		  Wend
		  RSquickcat.close()
		  %>
		  </select>
              <input name="button" type="image" src="/images/btn_go.jpg" alt="Search" align="middle" width="27" height="27" hspace="0" vspace="0" border="0">
      </td></form>
    </tr>
    <tr> 
      <td height="10" colspan="2" bgcolor="#CC1C0F"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <form name="form1" method="post" action="<%=MM_LoginAction%>"> <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="1" height="10"></td>
    </tr>
    <tr> 
          <td colspan="2"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Welcome 
            to SequinsPlus.com. If you are ready to order please specify your 
            'username' and correct 'password', otherwise please check &quot;New 
            Account. I am a guest.&quot; then click &quot;Sign In&quot; button 
            to continue.</font></td>
    </tr>
    <tr> 
      <td height="20" colspan="2"><img src="/images/transparent.gif" width="1" height="20">
            </td>
    </tr>
        <tr valign="bottom"> 
          <td height="20" colspan="2"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>I 
            want to sign in with my:</strong></font></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" colspan="2" td align="center" style="padding-top: 5; padding-bottom: 5; padding-left: 5; padding-right:5;"> 
        <table width="590" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="20"> <input name="rb" type="radio" value="Existing" checked> 
            </td>
            <td><img src="/images/transparent.gif" width="5" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">Existing 
              account for this site.</font></td>
          </tr>
          <tr> 
            <td width="20"> <input type="radio" name="rb" value="New"></td>
            <td><img src="/images/transparent.gif" width="5" height="1"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">New 
              Account. I am a new guest.</font></td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td height="20" colspan="2"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>Username 
        : </strong></font> <input name="UserName" type="text" id="UserName"></td>
    </tr>
    <tr> 
      <td height="10" colspan="2"><img src="/images/transparent.gif" width="10" height="10"></td>
    </tr>
    <tr> 
      <td height="20" colspan="2"><font color="#000000" size="3" face="Arial, Helvetica, sans-serif"><strong>Password 
        : </strong></font> <input name="Password" type="password" id="Password"> <img src="/images/transparent.gif" width="10" height="10"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif">(leave 
        blank if creating a new account)</font></td>
    </tr>
    <tr> 
      <td height="20" colspan="2">&nbsp;</td>
    </tr>
    <tr> 
          <td height="20" colspan="2">
              <input type="image" border="0" name="imageField" src="/images/sign_in.gif" width="76" height="18" ></td>
    </tr>
    <tr> 
      <td height="20" colspan="2">&nbsp;</td>
    </tr>
    <tr> 
            <td height="20" colspan="2"><a href="/en/order/forgot.asp"><font color="#616161" size="2" face="Arial, Helvetica, sans-serif"><u>Forgot 
              your password? Click Here</u></font></a></td>
    </tr>
    <tr> 
      <td height="20" colspan="2">&nbsp;</td>
    </tr>
    <tr> 
      <td height="20" colspan="2">&nbsp;</td>
    </tr><input type="hidden" name="accessdenied" value="<%=Request.QueryString("accessdenied")%>"></form>
  </table>
</td>
    <td width="10" height="375"></td>
  </tr>
  <tr>
    <td width="8" height="5"></td>
    <td width="2" height="5" bgcolor="#FFFFFF"></td>
    <td width="6" height="5" bgcolor="#FFFFFF"></td>
    <td width="141" height="5" bgcolor="#FFFFFF"></td>
    <td width="8" height="5" bgcolor="#FFFFFF"></td>
    <td width="126" height="5" bgcolor="#FFFFFF"></td>
    <td width="15" height="5" bgcolor="#FFFFFF"></td>
    <td width="8" height="5" bgcolor="#FFFFFF"></td>
    <td width="141" height="5" bgcolor="#FFFFFF"></td>
    <td width="8" height="5" bgcolor="#FFFFFF"></td>
    <td width="141" height="5" bgcolor="#FFFFFF"></td>
    <td width="13" height="5" bgcolor="#FFFFFF"></td>
    <td width="10" height="5"></td>
  </tr>
  <tr>
    <td width="8" height="14"></td>
    <td width="762" height="14" colspan="13" bgcolor="#DFC6A3"></td>
    <td width="10" height="14"></td>
  </tr>
  <tr>
    <td width="8" height="49"></td>
    <td width="762" height="49" colspan="13" align="center" valign="middle" bgcolor="#FFFFFF"><font size="2" face="Arial, Helvetica, sans-serif"><a href="/">Home</a> 
      | <a href="/en/about/default.asp">About Us</a> | <a href="/en/products/default.asp">Products</a> 
      | <a href="/en/services/default.asp">Services</a> | <a href="/en/contact/default.asp">Contact</a> 
      | <a href="javascript:SiteMap()">Sitemap</a> | <a href="javascript:Disclaimer()">Disclaimer</a></font></td>
    <td width="10" height="49"></td>
  </tr>
  <tr>
    <td width="8" height="7"></td>
    <td width="762" height="7" colspan="13" valign="top"><IMG SRC="/images/page-bottom.gif" WIDTH=762 HEIGHT=7></td>
    <td width="10" height="7"></td>
  </tr>
  <tr>
    <td width="780" height="10" colspan="15"></td>
  </tr>
</table>
</BODY>
</HTML>