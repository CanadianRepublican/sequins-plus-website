<%
'#################### START CLASS DEFINITION ########################
Class DESQLSearch
	'private variables
	Private hTmpCount, bQuickSearch, bSearchName, bSearchDescriptions, bSearchProductID, bSearchManu, bSearchSize, bSearchCatagory, bSearchSubCat, bSearchMinPrice, bSearchMaxPrice, bSearchXact
	'public variables become properties of the class
	Public DBConnectionString
	Public DBConnectionAccount
	Public DBConnectionAcctPassword
	Public bNoFieldsToSearch
	Public SearchType
	Public ManuValue
	Public CatValue
	Public CatagoryValue
	Public SubCatValue
	Public MinPriceValue
	Public MaxPriceValue
	Public SizeValue
	Public Property Get Version
		Version = "2.0"
	End Property
	'public methods
	Public Sub QuickSearch
		bQuickSearch = true
	End Sub
	Public Sub SearchName
		bSearchName = true
	End Sub
	Public Sub SearchMinPrice
		bSearchMinPrice = true
	End Sub
	Public Sub SearchMaxPrice
		bSearchMaxPrice = true
	End Sub
	Public Sub SearchXact
		bSearchXact = true
	End Sub
	Public Sub SearchProductID
		bSearchProductID = true
	End Sub
	Public Sub SearchManu
		bSearchManu = true
	End Sub
	Public Sub SearchSize
		bSearchSize = true
	End Sub
	Public Sub SearchSubCat
		bSearchSubCat = true
	End Sub
	Public Sub SearchCatagory
		bSearchCatagory = true
	End Sub
	Public Sub SearchDescriptions
		bSearchDescriptions = true
	End Sub
	Public Function Search(ByVal sKeyword)
		strSQL = sqlString( MakeKeywordArray( sKeyword ) )
		Search = strSQL
	End Function
	Public Function Count
		Count = hTmpCount
	End Function
	'private class routines
	Private Sub Class_Initialize()
		'class_initialize event is best used to set
		'up default values for class scoped variables
	bSearchName = false
	bSearchDescriptions = false
	bSearchMinPrice = false
	bSearchMaxPrice = false
	bSearchXact = false
	bSearchProductID = false
	bSearchManu = false
	bSearchSize = false
	bSearchSubCat = false
	bSearchCatagory = false
	bQuickSearch = false	
	End Sub
	Private Function EscapeApostrophe(ByVal toEscape)
		'make "my dog's fleas" become "my dog''s fleas",
		'escaping the apostrophe character because
		'it's meaning is special in TSQL...
		EscapeApostrophe = replace(toEscape, "'", "''")
	End Function
	Private Function sqlString(ByVal keywords)
		 ' this function creates an sql string based on various criteria
		Dim i
		'the base string
		sqlString = "SELECT spItem.ProductCode, spItem.desc, spItem.Manu, spItem.Name, spItem.Price, spItem.id, spItem.Cat, spItem.BrowsePic, spItem.SmallPic, spItem.SubCat, spItem.Feat, spItem.SubSubCat, spItem.Quan, spItemCatagory.Name as CatName,  spItemSubCatagory.Name as SubName, spItemManufacturer.Name as ManuName, spItemQuantity.Name as QuanTxt" 
		if bSearchSize then sqlString = sqlString & ", spItemSize.id as SizeID, spItemSize.SizeType, spItemSizes.ItemID, spItemSizes.itemSize, spItemSizeType.id as STypeID, spItemSizeType.name as STypeName"
		sqlString = sqlString & " FROM spItemManufacturer, spItem, spItemCatagory, spItemSubCatagory, spItemQuantity " 
		if bSearchSize then sqlString = sqlString & ", spItemSizeType, spItemSize, spItemSizes "
		'check for any keywords entered...
		if IsArray(keywords) then
			'reset properties
			if bSearchProductID then
				bSearchName = false
				bSearchDescriptions = false
	        	bSearchMinPrice = false
	        	bSearchMaxPrice = false
	        	bSearchManu = false
	        	bSearchSize = false
          		bSearchXact = false
				SearchType = "OR"
			end if
			if keywords(0) = "NNothingNN" then
			bSearchName = false
				bSearchDescriptions = false
				end if
			 ' don't forget the where if we're still here
			sqlString = sqlString & "WHERE (spItem.Cat = spItemCatagory.id AND spItem.SubCat = spItemSubCatagory.id AND spItem.Quan = spItemQuantity.id AND spItem.Manu = spItemManufacturer.id "
			 ' make the criteria part of the statement based on the 
			 ' array of keywords and the chosen search fields
			if bSearchSize then sqlString = sqlString & "AND spItemSizes.ItemID = spItem.id AND spItemSize.SizeType = spItemSizeType.id AND spItemSize.id = spItemSizes.itemSize AND spItemSize.SizeType = " & SizeValue & " "
			if bSearchManu then sqlString = sqlString & "AND spItem.Manu = " & ManuValue & " "
			
			if bSearchCatagory then	sqlString = sqlString & "AND spItem.Cat = " & CatagoryValue & " "
			
			if bSearchSubCat then sqlString = sqlString & "AND spItem.SubCat = " & SubCatValue & " "
			
			if bSearchMinPrice then	sqlString = sqlString & "AND spItem.Price >= " & MinPriceValue & " "
			
			if bSearchMaxPrice then	sqlString = sqlString & "AND spItem.Price <= " & MaxPriceValue & " "
			
			sqlString = sqlString & ") AND ("
			if bQuickSearch then
			bSearchProductID = true
			bSearchName = true
			bSearchDescriptions = true
			if keywords(0) = "NNothingNN" then
			bSearchProductID = false
			bSearchName = false
				bSearchDescriptions = false
				end if
			end if
			if bSearchProductID then 
				for i = 0 to UBOUND(keywords)
					sqlString = sqlString & "spItem.ProductCode LIKE '%" & EscapeApostrophe(keywords(i)) & "%' " & SearchType & " "
				next
			
			if NOT (bQuickSearch) then sqlString = sqlString & ")"
			
		end if
			for i = 0 to UBOUND(keywords)
				if bSearchName then sqlString = sqlString & _
					"spItem.Name LIKE '%" & EscapeApostrophe(keywords(i)) & _
					"%' " & SearchType & " "
			next
			
			for i = 0 to UBOUND(keywords)
				if bSearchName then sqlString = sqlString & _
					"spItemManufacturer.Name LIKE '%" & EscapeApostrophe(keywords(i)) & _
					"%' " & "OR" & " "
			next
			if bSearchName AND bSearchDescriptions then 
				'need to clean the sql string and remove
				'the last AND or OR to avoid an error...
				sqlString = Left(sqlString, Len(sqlString)-4)
				sqlString = TRIM(sqlString)
				sqlString = sqlString & " OR "
			end if
			for i = 0 to UBOUND(keywords)
				if bSearchDescriptions then sqlString = sqlString & _
					"spItem.desc LIKE '%" & EscapeApostrophe(keywords(i)) & _
					"%' " & SearchType & " "
			next
			sqlString = Left(sqlString, Len(sqlString)-4)

			if SearchType = "OR" then sqlString = sqlString & " "
			if bSearchName OR bSearchDescriptions OR bSearchProductID then sqlString = sqlString & ") "
		
		 ' let's make the order by : most recent examples on top
		 if keywords(0) = "NNothingNN" then
			sqlString = Left(sqlString, Len(sqlString)-2)
				end if
		sqlString = sqlString & "ORDER BY spItemCatagory.Name ASC"
		end if
	End Function
	Private Function MakeKeywordArray(ByVal sWordVarTmp)
		'makes an array of any entered keywords
		Dim sDelim, x
		sDelim = ","
		if bSearchXact then
		if TRIM(sWordVarTmp) = "NNothingNN" then 
		redim x(0)
		x(0) = sWordVarTmp
		MakeKeywordArray = x
		Exit Function
		end if
		redim x(0)
		x(0) = sWordVarTmp
		end if
		if NOT (bSearchXact) then
		if TRIM(sWordVarTmp) = "" then Exit Function
		if not instr(sWordVarTmp, sDelim) then sDelim = " "
		x = Split(sWordVarTmp, sDelim)
		end if
		MakeKeywordArray = x
		End Function
End Class
'#################### END CLASS DEFINITION ########################
%>