<%

	Private Function InternetTime()

		Dim lLngTime
		Dim lLngBeats
		Dim lLngBeatsRound

		lLngTime = (Hour(Time()) * 3600 * 1000) + (Minute(Time()) * 60 * 1000) + (Second(Time()) * 1000 + 3600000)
		lLngBeats = lLngTime / 86400
		lLngBeatsRound = Round(lLngBeats)
		InternetTime = "@" & lLngBeatsRound

		If lLngBeatsRound > 1000 Then InternetTime = "@0" & lLngBeatsRound
		If lLngBeatsRound > 100 Then InternetTime = "@0" & lLngBeatsRound

	End Function ' InternetTime()

	Private Function LeadingZero(ByRef pStrValue)

		If Len(pStrValue) < 2 Then pStrValue = "0" & pStrValue

		LeadingZero = pStrValue

	End Function ' LeadingZero(ByRef pStrValue)

	Private Function IsLeap()

		Dim lLngYear

		lLngYear = Year(Now())
		IsLeap = 0

		If (lLngYear Mod 4 = 0) And (lLngYear Mod 100 <> 0) Or (lLngYear Mod 400 = 0) Then IsLeap = 1

	End Function ' IsLeap()


	Private Function DaysInMonth()

		Dim lLngMonth

		lLngMonth = Month(Now())

	    Select Case lLngMonth
		    Case 9, 4, 6, 11 : DaysInMonth = 30
		    Case 2 : If CBool(IsLeap()) Then DaysInMonth = 29 Else DaysInMonth = 28
		    Case Else : DaysInMonth = 31
	    End Select

	End Function ' DaysInMonth()

	Private Function FormatHour()

		Dim lDtmNow

		lDtmNow = FormatDateTime(Now(),3)
		FormatHour = Left(lDtmNow,InStr(lDtmNow,":") - 1)

	End Function 

	Private Function OrdinalSuffix()

		Dim lLngDay

		lLngDay = Day(Now())
		OrdinalSuffix = "th"

		If lLngDay = 1 Then OrdinalSuffix = "st"
		If lLngDay = 2 Then OrdinalSuffix = "nd"
		If lLngDay = 3 Then OrdinalSuffix = "rd"

	End Function ' OrdinalSuffix()

	Public Function FormatDate(ByRef pStrDate)

		Dim lObjRegExp
		Dim lObjRegMatches
		Dim lObjMatch
		Dim lDtmNow
		Dim lLngSecond
		Dim lLngMinute
		Dim lLngHour
		Dim lLngDay
		Dim lLngWeekDay
		Dim lLngMonth
		Dim lLngYear
		lDtmNow = Now()
		lLngSecond = Second(lDtmNow)
		lLngMinute = Minute(lDtmNow)
		lLngHour = Hour(lDtmNow)
		lLngDay = Day(lDtmNow)
		lLngWeekDay = WeekDay(lDtmNow)
		lLngMonth = Month(lDtmNow)
		lLngYear = Year(lDtmNow)
		Set lObjRegExp = New RegExp
		lObjRegExp.Pattern = "([a-z][a-z]*[a-z])*[a-z]"
		lObjRegExp.Global = True
		lObjRegExp.IgnoreCase = True
		Set lObjRegMatches = lObjRegExp.Execute(pStrDate)
		For Each lObjMatch In lObjRegMatches
			Select Case lObjMatch.Value
				Case "a" : pStrDate = Replace(pStrDate,"a",LCase(Right(lDtmNow,2)))
				Case "A" : pStrDate = Replace(pStrDate,"A",UCase(Right(lDtmNow,2)))
				Case "B" : pStrDate = Replace(pStrDate,"B",InternetTime())
				Case "d" : pStrDate = Replace(pStrDate,"d",LeadingZero(lLngDay))
				Case "D" : pStrDate = Replace(pStrDate,"D",Left(WeekDayName(lLngWeekDay),3))
				Case "F" : pStrDate = Replace(pStrDate,"F",MonthName(lLngMonth))
				Case "g" : pStrDate = Replace(pStrDate,"g",FormatHour())
				Case "G" : pStrDate = Replace(pStrDate,"G",lLngHour)
				Case "h" : pStrDate = Replace(pStrDate,"h",LeadingZero(FormatHour()))
				Case "H" : pStrDate = Replace(pStrDate,"H",LeadingZero(lLngHour))
				Case "i" : pStrDate = Replace(pStrDate,"i",LeadingZero(lLngMinute))
				Case "j" : pStrDate = Replace(pStrDate,"j",lLngDay)
				Case "l" : pStrDate = Replace(pStrDate,"l",WeekDayName(lLngWeekDay))
				Case "L" : pStrDate = Replace(pStrDate,"L",IsLeap())
				Case "m" : pStrDate = Replace(pStrDate,"m",LeadingZero(lLngMonth))
				Case "M" : pStrDate = Replace(pStrDate,"M",Left(MonthName(lLngMonth),3))
				Case "n" : pStrDate = Replace(pStrDate,"n",lLngMonth)
				Case "r" : pStrDate = Replace(pStrDate,"r",Left(WeekDayName(lLngWeekDay),3) & ", " & _
					lLngDay & " " & Left(MonthName(lLngMonth),3) & " " & lLngYear & " " & FormatDateTime(Time(),4))
				Case "s" : pStrDate = Replace(pStrDate,"s",LeadingZero(lLngSecond))
				Case "S" : pStrDate = Replace(pStrDate,"S",OrdinalSuffix())
				Case "t" : pStrDate = Replace(pStrDate,"t",DaysInMonth())
				Case "U" : pStrDate = Replace(pStrDate,"U",DateDiff("s",DateSerial(1970, 1, 1),lDtmNow))
				Case "w" : pStrDate = Replace(pStrDate,"w",lLngWeekDay - 1)
				Case "W" : pStrDate = Replace(pStrDate,"W",DatePart("ww", Date(), 2, vbFirstFourDays))
				Case "Y" : pStrDate = Replace(pStrDate,"Y",lLngYear)
				Case "y" : pStrDate = Replace(pStrDate,"y",Right(lLngYear,2))
				Case "z" : pStrDate = Replace(pStrDate,"z",DatePart("d", Date(), 2))
				Case Else : pStrDate = pStrDate & ""
			End Select
		Next

		Set lObjRegExp = Nothing
		FormatDate = pStrDate

	End Function ' FormatDate(ByRef pStrDate)
%>