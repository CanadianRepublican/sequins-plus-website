<META NAME="DC.Title" CONTENT="<%=MetaTitle%>">
<META NAME="keywords" CONTENT="<%=MetaKey%>">
<META NAME="description" CONTENT="<%=MetaDesc%>">
<META NAME="abstract" CONTENT="<%=MetaDesc%>">
<META HTTP-EQUIV="Content-Language" CONTENT="EN">
<META NAME="author" CONTENT="Digital Evolution (Rahim Khoja and Brent Nickelchok)">
<META HTTP-EQUIV="Expires" CONTENT="30 days">
<META NAME="revisit-after" CONTENT="1 days">
<META NAME="rating" CONTENT="General">
<META NAME="copyright" CONTENT="Sequins Plus">
<META NAME="robots" CONTENT="FOLLOW,INDEX">
<META NAME="Pragma" CONTENT="no-cache">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK REV="made" href="mailto:webmaster@sequinsplus.com">
