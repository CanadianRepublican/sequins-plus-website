<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
Response.Expires=-1000
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual="sequinsplus/DB/spDB.asp" -->
<html>
<head>
<link rel="stylesheet" type="text/css" href="/portal.css" />
<script>function SetScrollPosition(){ scrollTo(0,0);}</script><script language='javascript'>window.onscroll = function(){document.getElementById('_scroll_pos').value  = document.body.scrollLeft+'!' + document.body.scrollTop;}</script><script language='javascript'>window.onload=SetScrollPosition;</script>
<title>Sequins Plus Online - Admin Console - Login Page</title><meta NAME="Author" CONTENT="Rahim Khoja & Brent Nickelchok" />
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="Bodybg"> <table width="100%" align="center" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
<tr><td colspan="3">

<table width="100%" height="102" border="0" cellpadding="0" cellspacing="0" class="HeadBg">
	<tr>
		<td height="20" align="right" valign="middle" background="/images/banner/stripebg.gif" class="HeadBgTop">
			<span id="ucBanner_WelcomeMessage"><font color="#333333"></font></span>
			<a href='/SiteAdmin/default.asp' class="SiteLink">
				<span id="ucBanner_PortalName"><font color="#333333">Sequins Plus Admin Console</font></span></a>
			
&nbsp;&nbsp;	  </td>
	</tr>

	<tr>
		<td background="/images/banner/topbg.gif" height="79"><img src='/images/banner/topleft.jpg' width="353" height="79" border="0"><img src='/images/banner/topmid.gif' width="423" height="79" border="0"></td>
	</tr>
	<tr>
	  <td background="/images/banner/bottombar.gif" height="8"><img src="/images/banner/bottombar.gif" width="4" height="8" border="0"></td>
  </tr>
</table>
</td></tr><tr><td colspan="3" height="10px"></td></tr><tr valign="top"><td>&nbsp;&nbsp;</td><td width="160">
<table Width="175" border="0" cellpadding="0" cellspacing="0" class="HeadBg">
	<tr>

		<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width='8'><img src='/images/menuvert/menutl.gif' width='8' height='20'></td>
					<td background='/images/menuvert/menutm.gif' align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td width='8'><div align='right'><img src='/images/banner/menutr.gif' width='8' height='20'></div>
					</td>
				</tr>
			</table>
		</td>

	</tr>
	<tr>
		<td align='left' colspan='2' class='HeadBgBottomVert'>
			<table id="ucTabs_tabs" class="OtherTabsBgVert" cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td class="TabBgVert" nowrap="nowrap" height="22">
					<table width='100%' height='22' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td width='4' background='/images/menuvert/menuitembgs.gif'><img src='/images/menuvert/menuiteml.gif' width='4' height='22'></td>

							
                    <td nowrap background='/images/menuvert/menuitembgs.gif'  style="padding-left:10px"> 
                      <img id="Image2" src="/images/banner/arrow.gif" border="0" /> 
                      <span class="SelectedTab"> <a href="/SiteAdmin/default.asp">Home</a> 
                      </span></td>
							<td width="12" background='/images/menuvert/menuitembgs.gif'><div align="right"><img src='/images/menuvert/menuitemr.gif' width="4" height="22" border="0"></div>
							</td>
						</tr>
					</table>

				</td>
	</tr>
</table>
		</td>
	</tr>
	<tr>
		<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="8"><img src='/images/menuvert/menubl.gif' width="8" height="22"></td>

					<td background='/images/menuvert/menubm.gif'>&nbsp;</td>
					<td width="8"><div align="right"><img src='/images/menuvert/menubr.gif' width="8" height="22"></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
  <td width="100%">
  <table width="100%" cellspacing="0" cellpadding="0" border="0"> 
    <tr valign="top">
		<td id="LeftMargin">&nbsp;&nbsp;</td>

		<td id="LeftPane">
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
	<tr>
		<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr background='/images/container/tabletm.gif'>
					<td width='19'><img src='/images/container/tabletl2.gif' width='32' height='32'></td>

					<td valign='middle' nowrap background='/images/container/tabletm.gif'>
						&nbsp;
						<span id="_ctl5_ModuleTitle" class="Head">Account Login</span>
					</td>
					<td width='19'><div align='right'><img src='/images/container/tabletr.gif' width='19' height='32'></div>
					</td>
				</tr>
			</table>

		</td>
	</tr>
	<tr>
		<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr background='/images/container/tablemm.gif' valign="top">
					<td width='8' background='/images/container/tableml.gif'><img src='/images/container/tableml.gif' width='8' height='8'></td>
					<td background='/images/container/tablemm.gif'><br>
						<table width='100%' >
							<tr  valign="top">

								<td id="_ctl5_ContentPane">							
								

<table>
<tr>
<td>
			<span style='visibility:hidden;display:none;color:Red' class='NormalRed'  id='_ctl5__ctl0_RequiredFieldValidator1'><span id='_ctl5__ctl0_RequiredFieldValidator1_Txt'></span></span>
			<span style='visibility:hidden;display:none;color:Red' class='NormalRed'  id='_ctl5__ctl0_RequiredFieldValidator2'><span id='_ctl5__ctl0_RequiredFieldValidator2_Txt'></span></span>
</td>
</tr>
	<tr>
		<td>
			<span id="_ctl5__ctl0_lblUsername" class="Normal">Username:</span></td>

	</tr>
	<tr>
	  <td nowrap>
			<input name="_ctl5:_ctl0:email" type="text" size="16" id="_ctl5__ctl0_email" class="NormalTextBox" />
			&nbsp;</td>
	</tr>
	<tr>
		<td><SPAN class="Normal">
				<span id="_ctl5__ctl0_lblPassword">Password:</span></SPAN></td>

	</tr>
	<tr>
		<td nowrap>
			<input name="_ctl5:_ctl0:password" type="password" size="16" id="_ctl5__ctl0_password" class="NormalTextBox" />
			&nbsp;</td>
	</tr>
	<tr>
		<td nowrap>
			<span class="Normal"><input id="_ctl5__ctl0_RememberCheckbox" type="checkbox" name="_ctl5:_ctl0:RememberCheckbox" /><label for="_ctl5__ctl0_RememberCheckbox">Remember Login</label></span></td>

	</tr>
	<tr>
		<td nowrap>
			<input name="_ctl5:_ctl0:SigninBtn" type="image" id="_ctl5__ctl0_SigninBtn" src="/images/signin.gif" alt="" width="64" height="18" border="0" /></td>
	</tr>
	<tr>
		<td nowrap></td>
	</tr>
	<tr>

		<td nowrap>
			<a id="_ctl5__ctl0_Hyperlink2" href="/Admin/ForgotPW.aspx"><img src="/images/forgotpassword.gif" alt="" border="0" /></a></td>
	</tr>
	<tr>
		<td colspan="2">
			<span id="_ctl5__ctl0_Message" class="NormalRed"></span></td>
	</tr>
</table>
</td>

							</tr>
						</table>
					</td>
					<td width='8' background='/images/container/tablemr.gif'><img src='/images/container/tablemr.gif' width='8' height='8'></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>

		<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr background='/images/container/tablebm.gif'>
					<td width='8' height='8'><img src='/images/container/tablebl.gif' width='8' height='8'></td>
					<td height='8' background='/images/container/tablebm.gif'><img src='/images/container/tablebm.gif' width='8' height='8'></td>
					<td width='8' height='8'><div align='right'><img src='/images/container/tablebr.gif' width='8' height='8'></div>
					</td>
				</tr>
			</table>
		</td>

	</tr>
</table>

</td>

		<td>&nbsp;&nbsp;</td>
		<td id="ContentPane" width="100%">
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
	<tr>
		<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr background='/images/container/tabletm.gif'>

					<td width='19'><img src='/images/container/tabletl2.gif' width='32' height='32'></td>
					<td valign='middle' nowrap background='/images/container/tabletm.gif'>
						&nbsp;
						<span id="_ctl6_ModuleTitle" class="Head">Welcome</span>
					</td>
					<td width='19'><div align='right'><img src='/images/container/tabletr.gif' width='19' height='32'></div>
					</td>
				</tr>

			</table>
		</td>
	</tr>
	<tr>
		<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr background='/images/container/tablemm.gif' valign="top">
					<td width='8' background='/images/container/tableml.gif'><img src='/images/container/tableml.gif' width='8' height='8'></td>
					<td background='/images/container/tablemm.gif'><br>
						<table width='100%' >

							<tr  valign="top">
								<td id="_ctl6_ContentPane">							
								<table id="_ctl6__ctl0_t1" cellspacing="0" cellpadding="0">
	<tr valign="top">
		              <td id="_ctl6__ctl0_HtmlHolder" class="normal"> </head> <body bgcolor="#FFFFFF" text="#000000" link="#000066" vlink="#0000FF" alink="#0099FF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> 
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tr> 
                            <td height="185" colspan="3" valign="top"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><font color="#000000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><font size="3"><font color="#003333">Welcome 
                              to the Sequins Plus Admin Console!</font></font></strong><font size="1"><br>
                              <br>
                              <strong>New Users </strong><br>
                              </font><font color="#003333" size="2" face="Arial, Helvetica, sans-serif"><font color="#000000" size="1">To 
                              enter this site for the first time enter your Username 
                              and your Password.<br>
                              <br>
                              </font></font></font></font><font color="#000000" size="1" face="Arial, Helvetica, sans-serif"> 
                              <strong><font face="Verdana, Arial, Helvetica, sans-serif">Access 
                              Problems?</font></strong><br>
                              Please contact The Webmaster. <br>
                              &#8226; </font><span class="SelectedTab">webmaster@sequinsplus.com</span><br>
                              </td>
                          </tr>
                          <tr> 
                            <td width="240" height="1" valign="top"><img src="/transparent.gif" alt="" width="240" height="1"></td>
                            <td width="20" height="1" valign="top"><img src="/transparent.gif" alt="" width="20" height="1"></td>
                            <td width="265" height="1" valign="top"><img src="/transparent.gif" alt="" width="265" height="1"></td>
                          </tr>
                        </table>
</body>
</html></td>
	</tr>
</table>

</td>

							</tr>
						</table>
					</td>

					<td width='8' background='/images/container/tablemr.gif'><img src='/images/container/tablemr.gif' width='8' height='8'></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr background='/images/container/tablebm.gif'>
					<td width='8' height='8'><img src='/images/container/tablebl.gif' width='8' height='8'></td>

					<td height='8' background='/images/container/tablebm.gif'><img src='/images/container/tablebm.gif' width='8' height='8'></td>
					<td width='8' height='8'><div align='right'><img src='/images/container/tablebr.gif' width='8' height='8'></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</td>

		<td>&nbsp;&nbsp;</td>
		
		
	</tr>
</table>
</td></tr><tr><td colspan="3" height="10px"></td></tr><tr><td colspan="3">
<br clear="all">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">  
   <tr>  
     <td height="63" background='/images/footer/footbar.gif'><img src='/images/footer/footbar.gif' width="4" height="63" border="0"></td>  
   </tr>  
</table></td></tr></table>
<input type=hidden name='_scroll_pos' id='_scroll_pos' value='0!0'>
<script language='JavaScript' src='/xlms/client/aspnet_client/system_web/1_1_4322/WebUIValidation.js'></script>
<script language="javascript" type="text/javascript">
<!--
	var gVAMActions =  new Array({ActnFnc:VAM_DoValidate,InitFnc:VAM_InitValAction,Enabled:true,Cond: {EvalFnc:VAM_EvalReqTextCond,InitFnc:VAM_InitOneFldCond,Enabled:true,HUEvts:true,IDToEval:'_ctl5__ctl0_email',CaseIns:false},CanRun:VAM_CanRunVal,VT:'VAL',ErrMsg:'* Please enter a username<br>',FmttrFnc:VAM_TextFmttr,ErrFldID:'_ctl5__ctl0_RequiredFieldValidator1',Dspl:2,Blnk:true,BlnkCss:'VAMBlinkText'}, {ActnFnc:VAM_DoValidate,InitFnc:VAM_InitValAction,Enabled:true,Cond: {EvalFnc:VAM_EvalReqTextCond,InitFnc:VAM_InitOneFldCond,Enabled:true,HUEvts:true,IDToEval:'_ctl5__ctl0_password',CaseIns:false},CanRun:VAM_CanRunVal,VT:'VAL',ErrMsg:'* Please enter a password',FmttrFnc:VAM_TextFmttr,ErrFldID:'_ctl5__ctl0_RequiredFieldValidator2',Dspl:2,Blnk:true,BlnkCss:'VAMBlinkText'});
		// -->
</script>
<script type='text/javascript'>
<!--
VAM_InitActions();
VAM_UpdateOnClick('_ctl5__ctl0_SigninBtn','','');
VAM_GetById('VAM_JSE').value='1';
// -->
</script>
</body>
</html>