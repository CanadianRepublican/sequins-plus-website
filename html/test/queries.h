/*
	this file contains the class declarations and prototypes for queries
*/
#include "resource.h"

class	CAddQuery	:	public	CRecordset
{
public:
CAddQuery(CDatabase* pdb);

		//Field/Param Data
	//{{AFX_FIELD(CAddQuery, CRecordset)
	CString		m_strName;
	CString		m_strHomePage;
	CString		m_strBrowser;
	CString		m_strEmail;
	CString		m_strComments;
	//}}AFX_FIELD

	//	ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddQuery)
public:
	virtual	CString		GetDefaultConnect();
	virtual	CString		GetDefaultSQL();
	virtual	void		DoFieldExchange(CFieldExchange* pFX);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CAddQuery)
	//}}AFX_MSG

};

class	CViewQuery	:	public	CRecordset
{
public:
	CViewQuery(CDatabase* pdb);

		//Field/Param Data
	//{{AFX_FIELD(CViewQuery, CRecordset)
	CString		m_strName;
	CString		m_strHomePage;
	CString		m_strBrowser;
	CString		m_strEmail;
	CString		m_strComments;
	//}}AFX_FIELD

	//	ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CViewQuery)
public:
	virtual	CString		GetDefaultConnect();
	virtual	CString		GetDefaultSQL();
	virtual	void		DoFieldExchange(CFieldExchange* pFX);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CViewQuery)
	//}}AFX_MSG

};

class	CDetailsQuery	:	public	CRecordset
{
public:
CDetailsQuery(CDatabase* pdb);

		//Field/Param Data
	//{{AFX_FIELD(CDetailsQuery, CRecordset)
	CString		m_strName;
	CString		m_strHomePage;
	CString		m_strBrowser;
	CString		m_strEmail;
	CString		m_strComments;
	//}}AFX_FIELD

	//	ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDetailsQuery)
public:
	virtual	CString		GetDefaultConnect();
	virtual	CString		GetDefaultSQL();
	virtual	void		DoFieldExchange(CFieldExchange* pFX);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDetailsQuery)
	//}}AFX_MSG

};
