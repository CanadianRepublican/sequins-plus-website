/* Microsoft SQL Server - Scripting			*/
/* Server: STATION04			*/
/* Database: master			*/
/* Creation Date 8/27/96 9:39:50 PM 			*/
/****** Object:  Table dbo.GuestBook    Script Date: 8/27/96 9:39:52 PM ******/
if exists (select * from sysobjects where id = object_id('dbo.GuestBook') and sysstat & 0xf = 3)
	drop table dbo.GuestBook
GO

CREATE TABLE GuestBook
(
	Name char (50) NOT NULL ,
	Browser char (255) NULL ,
	HomePage char (50) NULL ,
	Email char (50) NULL ,
	Comments char (255) NULL ,
	CONSTRAINT PK___1__12 PRIMARY KEY  CLUSTERED 
	(
		Name
	)
)
GO

