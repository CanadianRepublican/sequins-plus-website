	/*
		Check to see if the user entered a name,
		if they didn't, inform them they need to enter one
	*/
	if(*pszName == NULL)
	{
		LoadLongResource(strHTML, IDS_HTML_NAMEREQUIRED);

		strOutput.Format(strHTML, szBASEHREF, szSCRIPTPATH,
						pszBrowser, pszBrowser, pszHomePage,
						pszComments);
	}
	/*
		If the user did enter a name, try to add it
		to the database
	*/
	else
	{
		db.Open(_T("GuestBook"), FALSE, FALSE, szConnectString, FALSE);

		CAddQuery AddQuery(&db, pszName);

		/*
			Check to see if the name is already on file,
			if it is, inform the user
		*/
		/*if(AddQuery.Open())
		{

			LoadLongResource(strHTML,IDS_HTML_ERROR);

			strError.Format("%s is already in our Guestbook", pszName);

			strOutput.Format(strHTML, szBASEHREF, strError);
		}*/

		/*
			If the name is not on file, prepare the database
			for a new record, set the values and save the record
		*/
		if(AddQuery.Open())
		{
			try
			{
				AddQuery.Open(NULL,"GuestBook");

				AddQuery.AddNew();

				AddQuery.m_strName		=	pszName;
				AddQuery.m_strBrowser	=	pszBrowser;
				AddQuery.m_strHomePage	=	pszHomePage;
				AddQuery.m_strEmail		=	pszEmail;
				AddQuery.m_strComments	=	pszComments;

				AddQuery.Update();

				AddQuery.Close();
				db.Close();

				WritePageTitle(pCtxt, IDS_ADDSUCCESS);

				strMsg.Format("Thank you for signing our guestbook, %s"
					, pszName);

				LoadLongResource(strHTML, IDS_HTML_DETAILS);

				strOutput.Format(strHTML, szBASEHREF, strMsg, pszName, 
					pszBrowser,	pszHomePage, pszEmail, pszComments,
					szBASEHREF);
			}

			/*
				Catch any errors that may occur
			*/
			catch(CDBException* pEX)
			{
				TCHAR szError[1024];

				*pCtxt << (LPSTR) pEX;

				if(pEX->GetErrorMessage(szError, sizeof(szError)))
				{
	
					LoadLongResource(strHTML, IDS_HTML_ERROR);

					strOutput.Format(strHTML, szBASEHREF, szError);
	
				}
				else
				{
					LoadLongResource(strHTML, IDS_HTML_ERROR);

					strError = "Query failed for unknown reason.";

					strOutput.Format(strHTML,szBASEHREF, strError);
				}
			}
		}
	}

	*pCtxt	<<	strOutput;
