/**********************************************************************\
	DBSAMPLE.H
\**********************************************************************/

static const TCHAR	szExpires[]			= _T("Expires: Thu, 01 Dec 1994 16:00:00 GMT\r\n");
static const TCHAR	szNoCache[]			= _T("Pragma: no-cache\r\n");
static const TCHAR	szConnectString[]	= _T("ODBC;UID=sa;PWD=;");
static const TCHAR	szHTMLType[]		= _T("HTML");
static const TCHAR	szBrowser[]			= _T("USER_AGENT");
static const TCHAR	szBASEHREF[]		= _T("http://www.midnite.net/");
static const TCHAR	szSCRIPTPATH[]		= _T("/scripts/dbsample.dll");


///////////////////////////////////////////////////////////////////////

#include "resource.h"

class CDBSample : public CHttpServer
{
private:
	CString	GetUserAgent(CHttpServerContext* pCtxt);
	void	SendNoCache(CHttpServerContext* pCtxt);
	void	WritePageTitle(CHttpServerContext* pCtxt, UINT nID);
	BOOL	LoadLongResource(CString& str, UINT nID);

public:
	CDBSample();
	~CDBSample();

	void	HowItWorks(CHttpServerContext* pCtxt);
	void	View(CHttpServerContext* pCtxt);
	void	Details(CHttpServerContext* pCtxt, LPCTSTR pszName);
	void	Add(CHttpServerContext* pCtxt, LPCTSTR pszName,
					LPCTSTR pszBrowser, LPCTSTR pszHomePage,
					LPCTSTR pszEmail, LPCTSTR pszComments);
	void	AddSetup(CHttpServerContext* pCtxt);
	void	Default(CHttpServerContext* pCtxt);

// Overrides
	// ClassWizard generated virtual function overrides
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//{{AFX_VIRTUAL(CDBSample)
	public:
	virtual BOOL GetExtensionVersion(HSE_VERSION_INFO* pVer);
	//}}AFX_VIRTUAL

	DECLARE_PARSE_MAP()

	//{{AFX_MSG(CDBSample)
	//}}AFX_MSG
};


