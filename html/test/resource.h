//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DBSample.rc
//
#define IDS_SERVER                      102
#define IDS_MAIN                        103
#define IDS_HTML_MAIN                   103
#define IDS_HOWITWORKS                  104
#define IDS_HTML_HOWITWORKS             104
#define IDS_ADDSETUP                    105
#define IDS_HTML_ADDSETUP               105
#define IDS_ADDSUCCESS                  106
#define IDS_ADDFAIL                     107
#define IDS_HTML_NAMEREQUIRED           107
#define IDS_DEFAULT                     108
#define IDS_HTML_VIEWBODY               109
#define IDS_DETAILS                     109
#define IDS_HTML_VIEWHEADER             110
#define IDS_NAMEREQUIRED                110
#define IDS_HTML_VIEWFOOTER             111
#define IDS_HTML_ERROR                  112
#define IDS_HTML_DETAILS                113

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        114
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         103
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
