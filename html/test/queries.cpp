/**********************************************************************\
  QUERIES.CPP
  ---------------------------------------------------------------

  This file contains all of the queries related to this app

  The fields in the database are:
	Name		char		50		NOT	NULL	User's name
	HomePage	char		50		NULL		User's Home Page
	Browser		char		255		NULL		User's browser
	Email		char		50		NULL		User's E-mail address
	Comments	char		255		NULL		User's comments

\**********************************************************************/

#include "stdafx.h"
#include "queries.h"

CAddQuery::CAddQuery(CDatabase* pdb)
: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CAddQuery)
	m_strName		=	_T("");
	m_strBrowser	=	_T("");
	m_strHomePage	=	_T("");
	m_strEmail		=	_T("");
	m_strComments	=	_T("");
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dynaset;
}

CString	CAddQuery::GetDefaultConnect()
{
	return _T("ODBC;DSN=GuestBook");
}

CString	CAddQuery::GetDefaultSQL()
{
	CString	strQuery;

	strQuery.Format("GuestBook");

	return	strQuery;
}

void	CAddQuery::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CAddQuery)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Text(pFX, _T("[Name]"), m_strName);
	RFX_Text(pFX, _T("[Browser]"), m_strBrowser);
	RFX_Text(pFX, _T("[HomePage]"), m_strHomePage);
	RFX_Text(pFX, _T("[EMail]"), m_strEmail);
	RFX_Text(pFX, _T("[Comments]"), m_strComments);
	//}}AFX_FIELD_MAP
}

#if 0
BEGIN_MESSAGE_MAP(CAddQuery, CRecordset)
	//{{AFX_MSG_MAP(CAddQuery)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif // 0

CViewQuery::CViewQuery(CDatabase* pdb)
: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CViewQuery)
	m_strName		=	_T("");
	m_strBrowser	=	_T("");
	m_strHomePage	=	_T("");
	m_strEmail		=	_T("");
	m_strComments	=	_T("");
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dynaset;
}

CString	CViewQuery::GetDefaultConnect()
{
	return _T("ODBC;DSN=GuestBook");
}

CString	CViewQuery::GetDefaultSQL()
{
	CString	strQuery;

	strQuery= _T("GuestBook");

	return	strQuery;
}

void	CViewQuery::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CViewQuery)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Text(pFX, _T("[Name]"), m_strName);
	RFX_Text(pFX, _T("[HomePage]"), m_strHomePage);
	RFX_Text(pFX, _T("[Browser]"), m_strBrowser);
	RFX_Text(pFX, _T("[EMail]"), m_strEmail);
	RFX_Text(pFX, _T("[Comments]"), m_strComments);
	//}}AFX_FIELD_MAP
}

#if 0
BEGIN_MESSAGE_MAP(CViewQuery, CRecordset)
	//{{AFX_MSG_MAP(CViewQuery)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif // 0

CDetailsQuery::CDetailsQuery(CDatabase* pdb)
: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CDetailsQuery)
	m_strName		=	_T("");
	m_strBrowser	=	_T("");
	m_strHomePage	=	_T("");
	m_strEmail		=	_T("");
	m_strComments	=	_T("");
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dynaset;
}

CString	CDetailsQuery::GetDefaultConnect()
{
	return _T("ODBC;DSN=GuestBook");
}

CString	CDetailsQuery::GetDefaultSQL()
{
	CString	strQuery;

	strQuery = _T("GuestBook");

	return	strQuery;
}

void	CDetailsQuery::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CDetailsQuery)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Text(pFX, _T("[Name]"), m_strName);
	RFX_Text(pFX, _T("[Browser]"), m_strBrowser);
	RFX_Text(pFX, _T("[HomePage]"), m_strHomePage);
	RFX_Text(pFX, _T("[EMail]"), m_strEmail);
	RFX_Text(pFX, _T("[Comments]"), m_strComments);
	//}}AFX_FIELD_MAP
}

#if 0
BEGIN_MESSAGE_MAP(CDetailsQuery, CRecordset)
	//{{AFX_MSG_MAP(CDetailsQuery)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif // 0
