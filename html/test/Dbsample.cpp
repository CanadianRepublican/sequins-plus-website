/**********************************************************************\

  ISAPI Database Sample
  --------------------------------------------------------------------
  Created using Visual C++ 4.2

  This ISAPI application demonstrates how to connect a database to
  the World Wide Web by using MFC's CDatabase and CRecordset. 
  This source code is only one implementation and is meant as an
  example; not the only nor the best way. This source code is
  PUBLIC DOMAIN and may be modified, copied and distributed with the
  stipulations that original copyright notices remain in place, and
  Midnite Communications is not held liable for problems resulting from
  the direct or indirect use of this code or executables created from
  this code. This application is not supported by Midnite Communications,
  but if serious problems are found, please inform baddison@midnite.net.

  We hope this source helps more people learn how to build ISAPI
  applications. If there are specific questions regarding this code,
  or ISAPI in general, we would be willing to help. We support
  Stephen Genusa and his efforts in promoting ISAPI freely and openly.

  Bobby Addison, Jr.
  baddison@midnite.net
  http://www.midnite.net/

\**********************************************************************/

#include "stdafx.h"
#include "DBSample.h"
#include "queries.h"


CWinApp theApp;

///////////////////////////////////////////////////////////////////////
// command-parsing map

BEGIN_PARSE_MAP(CDBSample, CHttpServer)
	ON_PARSE_COMMAND(Details, CDBSample, ITS_PSTR)
	ON_PARSE_COMMAND_PARAMS("Name")
	ON_PARSE_COMMAND(View, CDBSample, ITS_EMPTY)
	ON_PARSE_COMMAND(HowItWorks, CDBSample, ITS_EMPTY)
	ON_PARSE_COMMAND(AddSetup, CDBSample, ITS_EMPTY)
	ON_PARSE_COMMAND(Add, CDBSample, ITS_PSTR ITS_PSTR ITS_PSTR ITS_PSTR ITS_PSTR)
	ON_PARSE_COMMAND_PARAMS("Name Browser HomePage Email Comments")
	ON_PARSE_COMMAND(Default, CDBSample, ITS_EMPTY)
	DEFAULT_PARSE_COMMAND(Default, CDBSample)
END_PARSE_MAP(CDBSample)


///////////////////////////////////////////////////////////////////////
// The one and only CDBSample object

CDBSample theExtension;


///////////////////////////////////////////////////////////////////////
// CDBSample implementation

CDBSample::CDBSample()
{
}

CDBSample::~CDBSample()
{
}

BOOL CDBSample::GetExtensionVersion(HSE_VERSION_INFO* pVer)
{
	// Call default implementation for initialization
	CHttpServer::GetExtensionVersion(pVer);

	// Load description string
	TCHAR sz[HSE_MAX_EXT_DLL_NAME_LEN+1];
	ISAPIVERIFY(::LoadString(AfxGetResourceHandle(),
			IDS_SERVER, sz, HSE_MAX_EXT_DLL_NAME_LEN));
	_tcscpy(pVer->lpszExtensionDesc, sz);
	return TRUE;
}

/**********************************************\
	Public Function-Default()
\**********************************************/
void CDBSample::Default(CHttpServerContext* pCtxt)
{
	CString	strHTML;
	CString strOutput;
	CString strBrowser;
	CString strReferer;
	
	StartContent(pCtxt);
	WritePageTitle(pCtxt, IDS_MAIN);

	LoadLongResource(strHTML, IDS_HTML_MAIN);

	strOutput.Format(strHTML, szBASEHREF, szSCRIPTPATH,
								szSCRIPTPATH, szSCRIPTPATH, 
								szSCRIPTPATH);

	*pCtxt << strOutput;

	EndContent(pCtxt);
}

/**********************************************\
	Public Function-HowItWorks()
\**********************************************/
void CDBSample::HowItWorks(CHttpServerContext* pCtxt)
{
	CString strHTML;
	CString strOutput;

	StartContent(pCtxt);
	WritePageTitle(pCtxt, IDS_HOWITWORKS);

	LoadLongResource(strHTML, IDS_HTML_HOWITWORKS);

	strOutput.Format(strHTML);

	*pCtxt << strOutput;

	EndContent(pCtxt);
}

/**********************************************\
	Public Function-AddSetup()
\**********************************************/
void CDBSample::AddSetup(CHttpServerContext* pCtxt)
{
	CString	strHTML;
	CString	strOutput;
	CString	strBrowser;

	strBrowser = GetUserAgent(pCtxt);

	SendNoCache(pCtxt);
	StartContent(pCtxt);
	WritePageTitle(pCtxt, IDS_ADDSETUP);

	LoadLongResource(strHTML, IDS_HTML_ADDSETUP);

	strOutput.Format(strHTML, szBASEHREF, szSCRIPTPATH,
							strBrowser, strBrowser);

	*pCtxt << strOutput;

	EndContent(pCtxt);

}

/**********************************************\
	Public Function-Add()
\**********************************************/
void CDBSample::Add(CHttpServerContext* pCtxt, LPCTSTR pszName,
					LPCTSTR pszBrowser, LPCTSTR pszHomePage,
					LPCTSTR pszEmail, LPCTSTR pszComments)
{
	CDatabase	db;
	CString		strHTML;	//	buffer for HTML resources
	CString		strOutput;	//	string to send to user
	CString		strError;	//	string for error messages
	CString		strMsg;
	CString		strQuery;

	StartContent(pCtxt);
	SendNoCache(pCtxt);

	strQuery.Format("Name = '%s'", pszName);

	db.Open(_T("GuestBook"), FALSE, FALSE, szConnectString, FALSE);

	CAddQuery	AddQuery(&db);

	/*
		Check to see if the user entered a name,
		if they didn't, inform them they need to enter one
	*/
	if(*pszName == NULL)
	{
		WritePageTitle(pCtxt, IDS_NAMEREQUIRED);

		LoadLongResource(strHTML, IDS_HTML_NAMEREQUIRED);

		strOutput.Format(strHTML, szBASEHREF, szSCRIPTPATH,
						pszBrowser, pszBrowser, pszHomePage,
						pszEmail, pszComments);
	}
	else
	{

		AddQuery.m_strFilter	=	strQuery;

		AddQuery.Open();

		try
		{
			/*
				OK, they entered a name, but is it on file already?
			*/
			if(AddQuery.IsBOF())
			{
				/*
					nope, it's a new name
				*/
				AddQuery.AddNew();
				AddQuery.m_strName		=	pszName;
				AddQuery.m_strBrowser	=	pszBrowser;
				AddQuery.m_strHomePage	=	pszHomePage;
				AddQuery.m_strEmail		=	pszEmail;
				AddQuery.m_strComments	=	pszComments;
				AddQuery.Update();
								
				WritePageTitle(pCtxt, IDS_ADDSUCCESS);

				strMsg.Format("Thank you for signing our guestbook, %s"
					, pszName);

				LoadLongResource(strHTML, IDS_HTML_DETAILS);

				strOutput.Format(strHTML, szBASEHREF, strMsg, pszName, 
					pszBrowser,	pszHomePage, pszEmail, pszComments,
					szSCRIPTPATH, szSCRIPTPATH, szSCRIPTPATH);
			}
			else
			{
				/*
					sorry, name alread there
				*/
				LoadLongResource(strHTML,IDS_HTML_ERROR);

				strError.Format("%s is already in our Guestbook", pszName);

				strOutput.Format(strHTML, szBASEHREF, strError);

			}
		}
		catch(CDBException* pEX)
		{
			TCHAR szError[1024];

			if(pEX->GetErrorMessage(szError, sizeof(szError)))
			{

					LoadLongResource(strHTML, IDS_HTML_ERROR);

					strOutput.Format(strHTML, szBASEHREF, szError);
	
			}
			else
			{
					LoadLongResource(strHTML, IDS_HTML_ERROR);

					strError = "Query failed for unknown reason.";

					strOutput.Format(strHTML,szBASEHREF, strError);
			}
		}
	}

	*pCtxt	<<	strOutput;

	AddQuery.Close();
	db.Close();

	EndContent(pCtxt);
}

void CDBSample::View(CHttpServerContext* pCtxt)
{
	CString	strHTMLHeader;
	CString strHTMLBody;
	CString strHTMLFooter;

	CString	strHeaderOutput;
	CString strBodyOutput;
	CString strFooterOutput;

	LPCTSTR	pszName;
	LPCTSTR	pszHomePage;
	LPCTSTR	pszEmail;

	CDatabase db;

	db.Open(_T("GuestBook"), FALSE, FALSE, szConnectString, FALSE);

	CViewQuery	ViewQuery(&db);

	StartContent(pCtxt);
	WritePageTitle(pCtxt, IDS_MAIN);

	LoadLongResource(strHTMLHeader, IDS_HTML_VIEWHEADER);
	LoadLongResource(strHTMLBody, IDS_HTML_VIEWBODY);
	LoadLongResource(strHTMLFooter, IDS_HTML_VIEWFOOTER);

	strHeaderOutput.Format(strHTMLHeader, szBASEHREF);
	strFooterOutput.Format(strHTMLFooter);

	*pCtxt << strHeaderOutput;

	if(ViewQuery.Open())
	{
		while(!ViewQuery.IsEOF())
		{

			pszName		=	ViewQuery.m_strName;
			pszEmail	=	ViewQuery.m_strEmail;
			pszHomePage	=	ViewQuery.m_strHomePage;

			strBodyOutput.Format(strHTMLBody, szSCRIPTPATH, 
				pszName, pszName, pszEmail, pszHomePage);

			*pCtxt << strBodyOutput;

			ViewQuery.MoveNext();
		}

		ViewQuery.Close();

		*pCtxt << strFooterOutput;
	}
	else
	{
		*pCtxt	<<	"Could not open query";
	}


	EndContent(pCtxt);
}

void CDBSample::Details(CHttpServerContext* pCtxt, LPCTSTR pszName)
{
	CDatabase	db;
	CString		strOutput;
	CString		strError;
	CString		strHTML;
	CString		strMsg;
	CString		strQuery;
	LPCTSTR		pszEmail;
	LPCTSTR		pszHomePage;
	LPCTSTR		pszBrowser;
	LPCTSTR		pszComments;

	SendNoCache(pCtxt);
	StartContent(pCtxt);
	WritePageTitle(pCtxt, IDS_DETAILS);

	strQuery.Format("Name = '%s'", pszName);

	db.Open(_T("GuestBook"), FALSE, FALSE, szConnectString, FALSE);

	CDetailsQuery	DetailsQuery(&db);

	DetailsQuery.m_strFilter = strQuery;

	DetailsQuery.Open();

	try
	{
		if(DetailsQuery.IsBOF())
		{
			LoadLongResource(strHTML, IDS_HTML_ERROR);
			strError.Format("%s was not found.",pszName);
			strOutput.Format(strHTML, szBASEHREF, strError);
		}
		else
		{
			pszEmail	=	DetailsQuery.m_strEmail;
			pszHomePage	=	DetailsQuery.m_strHomePage;
			pszBrowser	=	DetailsQuery.m_strBrowser;
			pszComments	=	DetailsQuery.m_strComments;

			strMsg.Format("The details for %s are:", pszName);

			LoadLongResource(strHTML, IDS_HTML_DETAILS);

			strOutput.Format(strHTML, szBASEHREF, strMsg, pszName, 
				pszBrowser,	pszHomePage, pszEmail, pszComments,
				szSCRIPTPATH, szSCRIPTPATH, szSCRIPTPATH);
		}
	}
	catch(CDBException* pEX)
	{
		TCHAR szError[1024];

		if(pEX->GetErrorMessage(szError, sizeof(szError)))
		{
			LoadLongResource(strHTML, IDS_HTML_ERROR);
			strOutput.Format(strHTML, szError);
		}
		else
		{
			LoadLongResource(strHTML, IDS_HTML_ERROR);
			strError = "An unknown error has occurred.";
			strOutput.Format(strHTML, strError);
		}
	}

	DetailsQuery.Close();
	db.Close();

	*pCtxt	<<	strOutput;

	EndContent(pCtxt);
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CDBSample, CHttpServer)
	//{{AFX_MSG_MAP(CDBSample)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

BOOL CDBSample::LoadLongResource(CString& str, UINT nID)
{
	HRSRC hRes;
	HINSTANCE hInst = AfxGetResourceHandle();
	BOOL bResult = FALSE;
	
	hRes = FindResource(hInst, MAKEINTRESOURCE(nID), szHTMLType);
	if (hRes == NULL)
		ISAPITRACE1("Error: Resource %d could not be found\r\n", nID);
	else
	{
		DWORD dwSize = SizeofResource(hInst, hRes);
		if (dwSize == 0)
		{
			str.Empty();
			bResult = TRUE;
		}
		else
		{
			LPTSTR pszStorage = str.GetBufferSetLength(dwSize);

			HGLOBAL hGlob = LoadResource(hInst, hRes);
			if (hGlob != NULL)
			{
				LPVOID lpData = LockResource(hGlob);

				if (lpData != NULL)
				{
					memcpy(pszStorage, lpData, dwSize);
					bResult = TRUE;
				}

				FreeResource(hGlob);
			}
		}
	}

#ifdef _DEBUG
	if (!bResult)
		str.Format(_T("<b>Could not find string %d</b>"), nID);
#endif

	return bResult;
}

void CDBSample::WritePageTitle(CHttpServerContext* pCtxt, UINT nID)
{
	CString strTitle;

	strTitle.LoadString(nID);

	*pCtxt << "<title>";
	*pCtxt << strTitle;
	*pCtxt << "</title>";
}

//	find out the browser the user is using
CString CDBSample::GetUserAgent(CHttpServerContext* pCtxt)
{
	LPTSTR	pstrBrowser = "BROWSER";
	DWORD	dwBrowser;
	
	if(pCtxt->GetServerVariable("HTTP_USER_AGENT", pstrBrowser, &dwBrowser))
	{
		return pstrBrowser;
	}
	else
	{
		return "UNKNOWN BROWSER";
	}
}

void CDBSample::SendNoCache(CHttpServerContext* pCtxt)
{
	AddHeader(pCtxt, szExpires);
	AddHeader(pCtxt, szNoCache);

}