<CFSETTING ENABLECFOUTPUTONLY="Yes">
<!--- CF_FILEEXIST
Usage: <CF_FILEEXIST file="picture.gif" fileok="picture.gif" fileerr="no_picture.gif">
Description: Check if the file "file" exists, if it exists it returns
             the text in "fileok", if it doesn't exists then it returns
             the text in "fileerr"

             If "fileok" is omitted it defaults to ""
             If "fileerr" is omitted it defaults to ""

Example: <IMG SRC="<CF_CF_FILEEXIST file="c:\picture.gif" fileok="picture.gif" fileerr="no_picture_found.gif">">
Example: <CF_FILEEXIST file='c:\file.zip' fileok='<A HREF="file.zip">file.zip</A>' fileerr='<I>file.zip</I>'>

Author: Stefan Karlsson, stefan@spray.se
Date: 20-Apr-2001

--->
<CFIF isDefined('Attributes.file')>
  <CFIF isDefined('Attributes.fileok')>
    <CFSET fileok = '#Attributes.fileok#'>
  <CFELSE>
    <CFSET fileok = ''>
  </CFIF>
  <CFIF isDefined('Attributes.fileerr')>
    <CFSET fileerr = '#Attributes.fileerr#'>
  <CFELSE>
    <CFSET fileerr = ''>
  </CFIF>
  <CFIF FileExists('#Attributes.file#') is 'YES'>
    <CFOUTPUT>#fileok#</CFOUTPUT>
  <CFELSE>
    <CFOUTPUT>#fileerr#</CFOUTPUT>
  </CFIF>
</CFIF>
<CFSETTING ENABLECFOUTPUTONLY="No">